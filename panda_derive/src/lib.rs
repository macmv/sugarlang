#![doc = include_str!("lib.md")]

use proc_macro::TokenStream;
use proc_macro2::{Ident, Span, TokenStream as TokenStream2};
use proc_macro_error::{abort, emit_error};
use quote::{quote, quote_spanned, ToTokens};

use syn::{
  parse_macro_input, punctuated::Punctuated, spanned::Spanned, AttributeArgs, FnArg,
  GenericArgument, ImplItem, ItemImpl, Lit, Meta, NestedMeta, Pat, Path, PathArguments, ReturnType,
  Type,
};

/// Defines a panda type. See the module level docs for more.
///
/// Arguments:
/// - `path`: This will change the path where this type is defined. By default,
///   this is just the type's name. If you were declaring a group of static
///   functions, you might want to change the type to something like `my_lib`.
///   Then, in panda, you would simple use `my_lib::my_func()`.
/// - `crate`: This is only meant for use within the Panda crate. By default,
///   all types are referenced through the `panda` crate. However, in the panda
///   crate itself, they must be referenced through the `crate` type. This
///   argument is a string, which will change the prefix for all referenced
///   types.
#[proc_macro_error::proc_macro_error]
#[proc_macro_attribute]
pub fn define_ty(args: TokenStream, input: TokenStream) -> TokenStream {
  let args = parse_macro_input!(args as AttributeArgs);
  let mut crte = quote!(panda::runtime);
  let mut path = None;
  let mut map_key = false;
  let mut block = parse_macro_input!(input as ItemImpl);
  let ty = match *block.self_ty {
    Type::Path(ref path) => path,
    _ => abort!(block.self_ty, "cannot use `define_ty` on a non-literal type"),
  };
  let ty_ident = ty.path.get_ident();
  for v in args {
    match v {
      NestedMeta::Meta(m) => match m {
        Meta::NameValue(v) => match &v.path.segments[0].ident {
          n if n == "crate" => match v.lit {
            Lit::Str(l) => {
              let value = Ident::new(&l.value(), l.span());
              crte = quote!(#value);
            }
            l => abort!(l, "expected str"),
          },
          n if n == "prefix" => match v.lit {
            Lit::Str(l) => {
              let val = l.value().parse().unwrap();
              match path {
                None => {
                  path = Some({
                    let mut path = parse_macro_input!(val as Path);
                    path.segments.push(syn::PathSegment {
                      arguments: syn::PathArguments::None,
                      ident:     ty_ident.unwrap().clone(),
                    });
                    path
                  })
                }
                Some(after) => {
                  path = Some({
                    let mut path = parse_macro_input!(val as Path);
                    path.segments.extend(after.segments);
                    path
                  })
                }
              }
            }
            l => abort!(l, "expected str"),
          },
          n if n == "path" => match v.lit {
            Lit::Str(l) => {
              let val = l.value().parse().unwrap();
              if l.value() == "" {
                path = Some(Path { leading_colon: None, segments: Punctuated::new() });
              } else {
                path = Some(parse_macro_input!(val as Path));
              }
            }
            l => abort!(l, "expected str"),
          },
          n if n == "map_key" => match v.lit {
            Lit::Bool(l) => map_key = l.value(),
            l => abort!(l, "expected bool"),
          },
          name => abort!(name, "unknown arg {}", name),
        },
        m => abort!(m, "unknown arg {:?}", m),
      },
      _ => abort!(v, "unknown arg {:?}", v),
    }
  }

  let docs = block.attrs.iter().filter_map(|a| {
    if a.path.segments.first().unwrap().ident == Ident::new("doc", Span::call_site()) {
      Some(a.tokens.to_string())
    } else {
      None
    }
  });
  let ty_path = if let Some(path) = path {
    quote!(#crte::parse::path!(#path))
  } else {
    quote!(#crte::parse::path!(#ty_ident))
  };

  let mut funcs = vec![];
  let mut field_getter = None;
  let mut field_getters = vec![];
  let mut field_values = vec![];
  for f in &mut block.items {
    if let ImplItem::Method(method) = f {
      let docs = method
        .attrs
        .iter()
        .filter_map(|a| {
          if a.path.segments.first().unwrap().ident == Ident::new("doc", Span::call_site()) {
            Some(a.tokens.to_string())
          } else {
            None
          }
        })
        .collect::<Vec<_>>();
      let mut all_fields_index = None;
      for (i, attr) in method.attrs.iter().enumerate() {
        if attr.path.get_ident().map(|i| i.to_string()) == Some("all_fields".to_string()) {
          let name = &method.sig.ident;
          field_getter = Some(quote!(
            self.#name(name)
          ));
          all_fields_index = Some(i);
        }
      }
      if let Some(i) = all_fields_index {
        method.attrs.remove(i);
        continue;
      }
      let mut field_index = None;
      for (i, attr) in method.attrs.iter().enumerate() {
        if attr.path.get_ident().map(|i| i.to_string()) == Some("field".to_string()) {
          let name = &method.sig.ident;
          let name_str = name.to_string();
          field_getters.push(quote!(
            #name_str => #crte::Var::from(self.#name()),
          ));
          field_values.push((
            name_str,
            quote!(#crte::docs::MarkdownSection::from_lines(&[#(#docs),*])),
            match &method.sig.output {
              ReturnType::Type(_, ty) => ty,
              _ => abort!(method.sig, "field must have return type"),
            },
          ));
          field_index = Some(i);
        }
      }
      if let Some(i) = field_index {
        method.attrs.remove(i);
        continue;
      }
      let name = &method.sig.ident;
      let mut is_member = false;
      let mut is_member_mut = false;
      if let Some(FnArg::Receiver(rec)) = method.sig.inputs.first() {
        is_member = true;
        if rec.mutability.is_some() {
          is_member_mut = true;
        }
      }
      let doc_names: Vec<_> = method
        .sig
        .inputs
        .iter()
        .map(|arg| match arg {
          FnArg::Receiver(_) => "self".to_string(),
          FnArg::Typed(ty) => match ty.pat.as_ref() {
            Pat::Ident(ident) => ident.ident.to_string(),
            _ => abort!(Span::call_site(), "arg pattern must be ident"),
          },
        })
        .collect();
      let doc_args: Vec<_> = method
        .sig
        .inputs
        .iter()
        .map(|arg| match arg {
          FnArg::Receiver(_) => quote!(None),
          FnArg::Typed(ty) => {
            let out = convert_ty(&crte, &ty.ty);
            quote!(Some(#crte::parse::VarType::#out))
          }
        })
        .collect();
      let mut needs_try = false;
      let ret = match &method.sig.output {
        ReturnType::Default => quote!(#crte::parse::VarType::None),
        ReturnType::Type(_, ty) => {
          let out = convert_ty(&crte, ty);
          if let Type::Path(path) = ty.as_ref() {
            if path.path.segments.len() == 1
              && path.path.segments.first().unwrap().ident == "Result"
            {
              needs_try = true;
            }
          }
          quote!(#crte::parse::VarType::#out)
        }
      };

      // We now create the main translation between the args array and the function
      // arguments. We might need an &mut self, so we may need to move all the
      // arguments out of the `args` array. Because of this, we define all of our
      // variables in reverse, using `args.pop()`. We then pass these to the caller in
      // the correct order.

      let mut i = 0_usize;
      let mut arg_init = vec![];
      let mut arg_val = vec![];
      for a in method.sig.inputs.iter() {
        match a {
          FnArg::Receiver(_) => {}
          FnArg::Typed(sig) => {
            let ty = &sig.ty;
            check_arg_type_inner(ty);
            let arg_name = Ident::new(&format!("arg_{}", i), ty.span());
            arg_init.push(match &**ty {
              t if is_copy(t) || is_ref(t) => quote_spanned!(ty.span() => let #arg_name = args.pop().unwrap();),
              Type::Reference(t) => {
                let ty = &t.elem;
                let arg_val_name = Ident::new(&format!("arg_val_{}", i), ty.span());
                quote_spanned!(
                  ty.span() =>
                  let #arg_val_name = args.pop().unwrap();
                  let #arg_name = <#ty as #crte::TryFromSpanRef>::try_from_span_ref(&#arg_val_name, pos)?;
                )
              }
              _ => {
                emit_error!(
                  ty, "cannot convert to a Panda type";
                  note = "try using a reference: `&{ty}`"
                );
                continue
              }
            });
            arg_val.push(match &**ty {
              t if is_copy(t) => {
                quote_spanned!(ty.span() => <#ty>::try_from_span(#arg_name, pos)?)
              }
              t if is_ref(t) => {
                quote_spanned!(ty.span() => <#ty>::try_from_span(&#arg_name, pos)?)
              }
              Type::Reference(t) => {
                let ty = &t.elem;
                quote_spanned!(ty.span() => &*#arg_name)
              }
              _ => continue,
            });

            /*
            if is_member_mut {
              // We can only borrow the environment as mutable once, so arguments must be
              // cloned if we need an &mut self. TODO: Possibly use refcells?
              arg_init.push(quote!(let #arg_name = args.pop().unwrap().into_var(env);));
              arg_val.push(quote!(<#ty>::try_from_span(&#arg_name, pos)?));
            } else {
              arg_init.push(quote!(let #arg_name = &args[#i];));
              arg_val.push(quote!(<#ty>::try_from_span(#arg_name, pos)?));
            }
            */
            i += 1;
          }
        }
      }

      let try_val = if needs_try { quote!(?) } else { quote!() };

      let call_val = if is_member {
        let slf = if is_member_mut {
          quote_spanned!(ty.span() => &mut *<#ty as #crte::TryFromSpanMut>::try_from_span_mut(&slf, pos)?)
        } else {
          quote_spanned!(ty.span() => &*<#ty as #crte::TryFromSpanRef>::try_from_span_ref(&slf, pos)?)
        };
        quote!(Ok(#ty::#name(#slf, #(#arg_val),*)#try_val.try_into_span(pos)?))
      } else {
        quote!(Ok(#ty::#name(#(#arg_val),*)#try_val.try_into_span(pos)?))
      };
      // We pop each argument, in order to avoid clones. This means we need to pop in
      // reverse.
      arg_init.reverse();
      let exec = quote!(|env, slf, mut args, pos| {
        use #crte::{TryFromSpan, TryIntoSpan};
        #(#arg_init)*
        #call_val
      });

      let name_str = method.sig.ident.to_string();
      funcs.push(quote!(#crte::BuiltinFunc {
        name: #name_str.into(),
        docs: #crte::docs::MarkdownSection::from_lines(&[#(#docs),*]),
        args: vec![#((#doc_names, #doc_args)),*],
        args_must_match: true,
        member: #is_member,
        ret: #ret,
        exec: std::sync::Arc::new(#exec),
      }));
    }
  }
  let as_map_key_block;
  let as_map_key;
  if map_key {
    as_map_key = quote!(Some(#crte::MapKeyBuiltin::new(#ty_path, Box::new(self.clone()))));
    as_map_key_block = quote! {
      impl #crte::MapKeyBuiltinImpl for #ty {
        fn to_builtin(
          &self,
        ) -> ::std::rc::Rc<::std::cell::RefCell<dyn #crte::BuiltinImpl + Send + Sync>> {
          ::std::rc::Rc::new(::std::cell::RefCell::new(self.clone()))
        }
        fn hash(&self, state: &mut #crte::DynHasher) {
          <Self as ::std::hash::Hash>::hash(self, state);
        }
        fn box_clone(&self) -> Box<dyn #crte::MapKeyBuiltinImpl + Send + Sync> {
          Box::new(self.clone())
        }
        fn as_any(&self) -> &dyn ::std::any::Any {
          self
        }
        fn eq(&self, other: &dyn #crte::MapKeyBuiltinImpl) -> bool {
          <Self as ::std::cmp::Eq>::assert_receiver_is_total_eq(self);
          if let Some(other) = other.as_any().downcast_ref::<Self>() {
            <Self as ::std::cmp::PartialEq>::eq(self, other)
          } else {
            false
          }
        }
      }
    };
  } else {
    as_map_key = quote!(None);
    as_map_key_block = quote!();
  }
  let dynamic_fields = field_getter.is_some();
  let field_getter = match field_getter {
    Some(v) => v,
    None => {
      if field_getters.is_empty() {
        quote!(None)
      } else {
        quote!(Some(match name.as_ref() {
          #( #field_getters )*
          _ => return None,
        }))
      }
    }
  };
  let fields_def = if field_getters.is_empty() {
    quote!(vec![])
  } else {
    let name = field_values.iter().map(|(name, _, _)| name);
    let doc = field_values.iter().map(|(_, doc, _)| doc);
    let ty = field_values.iter().map(|(_, _, ty)| ty);
    quote!({
      let mut fields = vec![];
      #(
        fields.push((#name.into(), #doc, <#ty as #crte::PandaType>::var_type()));
      )*
      fields
    })
  };
  let out = quote! {
    // We want the block the user declared
    #block

    // And then we add some more code after that
    impl #crte::BuiltinImpl for #ty {
      fn path() -> #crte::parse::Path {
        #ty_path
      }
      #[allow(clippy::needless_question_mark)]
      fn funcs() -> Vec<std::sync::Arc<dyn #crte::FuncCallable + Send + Sync>> {
        vec![#(std::sync::Arc::new(#funcs)),*]
      }
      fn strct() -> #crte::BuiltinStruct {
        #crte::BuiltinStruct {
          docs: &[#(#docs),*],
          name: "".into(),
          fields: #fields_def,
          dynamic_fields: #dynamic_fields,
        }
      }
      fn field(&self, name: &#crte::parse::token::Ident) -> Option<#crte::Var> {
        #field_getter
      }
      fn box_clone(&self) -> Box<dyn #crte::BuiltinImpl + Send + Sync>  {
        Box::new(self.clone())
      }
      fn rc_clone(&self) -> ::std::rc::Rc<::std::cell::RefCell<dyn #crte::BuiltinImpl + Send + Sync>> {
        ::std::rc::Rc::new(::std::cell::RefCell::new(self.clone()))
      }
      fn as_any(&self) -> &dyn std::any::Any { self }
      fn as_any_mut(&mut self) -> &mut dyn std::any::Any { self }
      fn as_map_key(&self) -> Option<#crte::MapKeyBuiltin> { #as_map_key }
    }

    #as_map_key_block

    impl<'a> #crte::TryFromSpanRef<'a> for #ty {
      type Error = #crte::RuntimeError;

      fn try_from_span_ref(
        var: &'a #crte::Var,
        pos: #crte::parse::token::Span,
      ) -> ::std::result::Result<::std::cell::Ref<'a, #ty>, #crte::RuntimeError> {
        let strct = var.strct(pos)?;
        let builtin = strct.as_builtin(pos)?;
        if !builtin.as_any().is::<#ty>() {
          return Err(#crte::RuntimeError::custom(
            &format!("Expected builtin type {}, got {}", #ty_path, strct.path()),
            pos,
          ));
        }
        Ok(::std::cell::Ref::map(builtin, |v| v.as_any().downcast_ref::<#ty>().unwrap()))
      }
    }
    impl<'a> #crte::TryFromSpanMut<'a> for #ty {
      type Error = #crte::RuntimeError;

      fn try_from_span_mut(
        var: &'a #crte::Var,
        pos: #crte::parse::token::Span,
      ) -> ::std::result::Result<::std::cell::RefMut<'a, #ty>, #crte::RuntimeError> {
        let strct = var.strct(pos)?;
        let mut builtin = strct.as_builtin_mut(pos)?;
        if !builtin.as_any_mut().is::<#ty>() {
          return Err(#crte::RuntimeError::custom(
            &format!("Expected builtin type {}, got {}", #ty_path, strct.path()),
            pos,
          ));
        }
        Ok(::std::cell::RefMut::map(builtin, |v| v.as_any_mut().downcast_mut::<#ty>().unwrap()))
      }
    }

    impl From<#ty> for #crte::Var {
      fn from(data: #ty) -> #crte::Var {
        #crte::Var::new_builtin(#ty_path.into(), ::std::rc::Rc::new(::std::cell::RefCell::new(data)))
      }
    }
    impl #crte::PandaType for #ty {
      fn var_type() -> #crte::parse::VarType { #crte::parse::VarType::Builtin(#ty_path) }
    }
  };
  // Will print the result of this proc macro
  // let mut p =
  //   std::process::Command::new("rustfmt").stdin(std::process::Stdio::piped()).
  // spawn().unwrap(); std::io::Write::write_all(p.stdin.as_mut().unwrap(),
  // out.to_string().as_bytes()).unwrap(); p.wait_with_output().unwrap();
  TokenStream::from(out)
}

fn check_arg_type_inner(ty: &Type) {
  match ty {
    Type::Path(ref path) => {
      let seg = path.path.segments.iter().next().unwrap();
      match seg.ident.to_string().as_str() {
        "u8" | "u16" | "u32" | "u64" | "u128" | "i8" | "i16" | "i32" | "i64" | "i128" | "f32"
        | "f64" | "bool" | "Closure" => {}
        "Vec" => match seg.arguments {
          PathArguments::AngleBracketed(ref args) => match args.args.iter().next().unwrap() {
            GenericArgument::Type(t) => check_arg_type_inner(t),
            _ => emit_error!(
              ty, "cannot convert to a Panda type";
              note = "expected a concrete type parameter for Vec<T>";
            ),
          },
          _ => emit_error!(
            ty, "cannot convert to a Panda type";
            note = "expected a type parameter for Vec<T>";
          ),
        },
        "Box" | "Rc" | "Arc" | "Cell" | "RefCell" => match seg.arguments {
          PathArguments::AngleBracketed(ref args) => {
            emit_error!(
              ty, "cannot convert to a Panda type";
              note = "smart pointers should not be used";
              help = "try using `{}`", args.args.to_token_stream();
            )
          }
          _ => emit_error!(
            ty, "cannot convert to a Panda type";
            note = "smart pointers should not be used",
          ),
        },
        _ => {}
      }
    }
    Type::Reference(ref path) => match *path.elem {
      Type::Path(ref path) => {
        match path.path.segments.iter().next().unwrap().ident.to_string().as_str() {
          "str" | "String" => {}
          _ => {}
        }
      }
      Type::Slice(ref arr) => emit_error!(
        ty, "cannot convert to a Panda type";
        note = "arrays by reference don't make sense, because the proc macro must convert the array at runtime";
        help = "try using `Vec<{}>`", arr.elem.to_token_stream();
      ),
      _ => {}
    },
    Type::Array(arr) => {
      emit_error!(
        ty, "cannot convert to a Panda type";
        note = "fixed length arrays are not implemented yet";
        help = "try using `Vec<{}>`", arr.elem.to_token_stream();
      )
    }
    // Anything else is probably OK. We rely on normal compiler errors here,
    // which are much less readable than the values above.
    _ => {}
  }
}

fn convert_ty(crte: &TokenStream2, ty: &Type) -> TokenStream2 {
  match ty {
    Type::Array(_) => quote!(Array),
    Type::BareFn(_) => quote!(None),
    Type::Group(_) => quote!(None),
    Type::ImplTrait(_) => quote!(None),
    Type::Infer(_) => quote!(None),
    Type::Macro(_) => quote!(None),
    Type::Never(_) => quote!(None),
    Type::Paren(_) => quote!(None),
    Type::Path(path) => {
      if path.path.segments.len() == 1 {
        match path.path.segments.first().unwrap().ident.to_string().as_str() {
          "bool" => quote!(Bool),
          "i8" => quote!(Int),
          "i16" => quote!(Int),
          "i32" => quote!(Int),
          "i64" => quote!(Int),
          "i128" => quote!(Int),
          "u8" => quote!(Int),
          "u16" => quote!(Int),
          "u32" => quote!(Int),
          "u64" => quote!(Int),
          "u128" => quote!(Int),
          "f32" => quote!(Float),
          "f64" => quote!(Float),
          "Vec" => quote!(Array),
          "Var" => quote!(None),
          "Callback" => quote!(Callback),
          "Closure" => quote!(Closure),
          "Result" => quote!(None),
          "Option" => quote!(None),
          "Variadic" => quote!(None),
          "String" => quote!(String),
          _ => {
            let i = &path.path.segments.first().unwrap().ident;
            quote!(Builtin(#i::path()))
          }
        }
      } else {
        let segments = path.path.segments.iter();
        quote!(Struct(#crte::parse::Path::new(vec![#(#segments.to_string()),*])))
      }
    }
    Type::Ptr(_) => quote!(None),
    Type::Reference(r) => match r.elem.as_ref() {
      Type::Path(path) => {
        if path.path.segments.len() == 1 {
          match path.path.segments.first().unwrap().ident.to_string().as_str() {
            "str" => quote!(String),
            "Var" => quote!(None),
            _ => {
              let i = &path.path.segments.first().unwrap().ident;
              quote!(Builtin(#i::path()))
            }
          }
        } else {
          quote!(None)
        }
      }
      _ => quote!(None),
    },
    Type::Slice(_) => quote!(None),
    Type::TraitObject(_) => quote!(None),
    Type::Tuple(_) => quote!(None),
    Type::Verbatim(_) => quote!(None),
    _ => quote!(None),
  }
}

fn is_copy(t: &Type) -> bool {
  matches!(t,
    Type::Path(p) if matches!(
      p.path.segments.first(),
      Some(seg) if seg.ident == "bool"
        || seg.ident == "u8"
        || seg.ident == "i8"
        || seg.ident == "u16"
        || seg.ident == "i16"
        || seg.ident == "u32"
        || seg.ident == "i32"
        || seg.ident == "u64"
        || seg.ident == "i64"
        || seg.ident == "u128"
        || seg.ident == "i128"
        || seg.ident == "Callback"
        || seg.ident == "Closure"
        || seg.ident == "String"
        || seg.ident == "Vec"
        || seg.ident == "Var"
        || seg.ident == "f32"
        || seg.ident == "f64"
    )
  )
}

fn is_ref(t: &Type) -> bool {
  matches!(t,
    Type::Reference(t) if matches!(&*t.elem, Type::Path(p)
      if matches!(
        p.path.segments.first(),
        Some(seg) if seg.ident == "str" || seg.ident == "String"
      )
    )
  )
}
