The derive portion of the Panda API. This crate allows you to easily add custom Rust types into Panda,
by simply adding an attribute to an `impl` block. Example:

(This is ignored because panda is not in scope, so we can't compile this. For a tested example,
see the Panda docs).
```ignore
use panda::runtime::{RuntimeError, Var};

struct MyType {}

// Panda re-exports the panda_derive types, so you should not add `panda_derive` as
// a seperate dependency.
#[panda::define_ty]
impl MyType {
  fn new() -> Self {
    MyType {}
  }

  fn print_hello(&self, name: &str) {
    println!("Hello {}", name);
  }

  fn add_numbers(&self, first: i32, second: Option<i32>, third: Option<i32>) -> i32 {
    let mut out = first;
    if let Some(second) = second {
      out += second;
    }
    if let Some(third) = third {
      out += third;
    }
    out
  }

  // NOTE: Variadic gets converting into a Vec by the `define_ty` macro. See below for more.
  fn join_strings(&self, sep: &str, strs: Variadic<&Var>) -> Result<String, RuntimeError> {
    let mut out = String::new();
    // Remember, strs is actuall a Vec<&Var>
    let mut iter = strs.iter();
    if let Some(first) = iter.next() {
      // Convert the variadic arg into a string, and return an error if it's not a string.
      out += first.str()?;
    }
    for s in iter {
      out += sep;
      // Convert the variadic arg into a string, and return an error if it's not a string.
      out += s.str()?;
    }
    out
  }

  fn mutable(&mut self) {}
}

// Start and run a Panda interpreter.
fn main() {
  let sl = Panda::new();

  // This only works if we used `define_ty`
  sl.add_builtin_ty::<MyType>();

  // Now we can call MyType functions
  sl.exec("fn main {
    ty = MyType::new()
  }");
}
```

To use this type, you would simply call it's functions in Panda:

(This is panda source, but uses rust syntax for readability)
```ignore
fn main() {
  // By default, MyType is declared in the global scope.
  ty = MyType::new()
  // Prints `Hello world`
  ty.print_hello("world")

  // Errors at runtime with a type-error. This will error at compile time in the future.
  ty.print_hello(5)

  assert_eq(ty.add_numbers(5),       5)
  assert_eq(ty.add_numbers(4, 5),    9)
  assert_eq(ty.add_numbers(1, 2, 3), 6)

  assert_eq(ty.join_strings(":"),                "")
  assert_eq(ty.join_strings(":", "text"),        "text")
  assert_eq(ty.join_strings(":", "a", "b", "c"), "a:b:c")
}
```

The types defined on MyType are complex. Here is a list of all the functions, and all of the rules
surrounding the `define_ty` macro:

- `new() -> Self {}`
  - The `new` function is the only static function defined on `MyType`. Because of how panda treats
    static/member functions, it can only ever be called as a static function.
  - `new` returns `Self`. Anything that implements `Into<Var>` can be returned from a builtin funciton.
    The `define_ty` macro implements `Into<Var>` and `TryFromSpan<Var>` for `MyType`.

- `print_hello(&self, name: &str) {}`
  - The `print_hello` function demostrates a simple member function. Unlike Rust, you cannot call a
    member function statically in Panda. So a member function must always use the `var.<my_func>`
    syntax.
  - Arguments are anything that implements `TryFromSpan<&Var>`. Currently, argument types are checked
    at runtime, but they will be checked at compile time in the future. Once they are checked at compile
    time, this may switch to use `From<&Var>`.

- `add_numbers(first: i32, second: Option<i32>, third: Option<i32>) -> i32 {}`
  - Options are parsed differently than most other arguments. Panda supports optional
    function parameters, and this is how they are implemented in Rust.
  - You can call this function with 1, 2, or 3 arguments. Anything outside of that range is
    invalid. In this example, the amount of parameters would produce options like so:
    ```text
    add_numbers(5)        -> first: 5, second: None, third: None
    add_numbers(6, 7)     -> first: 6, second: Some(7), third: None
    add_numbers(8, 9, 10) -> first: 8, second: Some(9), third: Some(10)
    ```
  - To avoid confusion with argument placement, all optional arguments must appear after
    all normal arguments. This is partially because it was easier to implement this way, but
    mostly because having an optional argument in the middle changes where all the arguments
    are, and makes long function calls hard to work with. I am speaking from the perspective
    of dealing with 10 arguments in a java function, where the function is defined so many
    times (with slightly differing arguments) that I can't understand what is happening.

- `fn join_strings(&self, sep: &str, strs: Variadic<&Var>) -> String {}`
  - This function accepts varidic arguments. Variadics can only be used as the last parameter
    in a function. Optional arguments cannot be mixed with a variadic argument. This function
    will accept 1 or more arguments. This means the varidic argument can be empty.
  - The `Variadic` type is a keyword to the derive macro that this is a varidic argument. It
    doesn't care about a `Variadic` type in scope, as the generated code converts all `Variadic`
    arguments into `Vec`.
  - Variadic arguments cannot be typed. They must always be a `&Var` or `Var`. See the mutable
    function below for why they are somtimes references and sometimes by value.
  - Lastly, this function returls a `Result<String, RuntimeError>`. Anything that returns a `Result`
    gets a `?` after the call. So the error can be anything that implements `Into<RuntimError>`.

- `fn mutable(&mut self) {}`
  - In a panda environment, all the variables are stored in a Vec at runtime. And because
    there are no restrictions on runtime arguments, it is valid to pass in self as another
    parameter to a function call. So, for static functions, and immutable member functions, this
    is fine (because we immutably borrow the arguments Vec for each argument).
  - This mutability rule is broken when we need mutable access to `self`. In this situation,
    all the known varidiables in the Panda environment must be mutably borrowed. When this
    happens, we must clone all other arguments to the function.
  - In a majority of situations, this will not affect you. The only time it will is for `Variadic`
    arguments. When a function mutably borrows self, the type within a `Variadic` changes from
    `&Var` to `Var`.
  - The other situation where this might be a problem is passing in other builtin types. If calling
    `clone` on your builtin is expensive, then you might want to avoid mutable borrows of `self`.
    However, any time you insert a builtin into an array or map, it gets cloned. So if `clone` is
    slow, you should fix it by wrapping the large data in a `Arc<Mutex<T>>`. All builtins must be
    `Send` and `Clone`, so you cannot use a `RefCell`.
