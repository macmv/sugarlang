#![deny(clippy::all)]
#![allow(clippy::needless_doctest_main, clippy::borrowed_box)]

//! # Panda
//!
//! This is the API docs for interfacing with Panda from rust. Panda is
//! built to be a small, fast, and multithreaded scripting language. I
//! specifically made it for a minecraft server, so that writing plugins would
//! be simple on my part and on the user's part.
//!
//! To get started, run this:
//! ```
//! use panda::Panda;
//!
//! let mut sl = Panda::new();
//! // This adds stdout functionality. Without this, the language is fully sandboxed.
//! sl.env().add_std_builtins();
//!
//! sl.exec_statement("println(5 + 6)");
//! ```
//!
//! This should print out 11 to the console. If you want a cli to mess with, you
//! can build the standalone binary with `cargo build` or `cargo run` from the
//! root directory of this project. The `pand` binary will alow you to run
//! individual statements, and it works very similar to running `python` with no
//! arguments.
//!
//! If you wish to run a file, you can use `pand <path>`. See the examples
//! directory for more.
//!
//! # Custom callbacks
//!
//! The whole point of this API is to add custom functionality to Panda.
//! This is very simple:
//!
//! ```
//! use panda::{runtime::Var, Panda, path, docs::markdown};
//!
//! let mut sl = Panda::new();
//! sl.add_builtin_fn(
//!   path!(hello), // path of the function
//!   markdown!(
//!     /// This function prints a hello message to stdout.
//!   ), // docs for this function
//!   false, // if this function takes `self`
//!   |_env, _slf, args, _call_site| {
//!   println!("Hello! You passed the arguments {:?}", args);
//!   Ok(Var::None)
//! });
//! sl.exec_statement("hello(5, 6, true, \"text here\")");
//! ```
//!
//! See the docs on [`add_builtin_fn`](Panda::add_builtin_fn) for more
//! details on how this works.
//!
//! # Custom types
//!
//! I have written a proc macro for custom types. This makes adding multiple
//! functions at a time very easy, and should be prefered over the
//! `add_builtin_fn` syntax. You can see an example of this below (it's untested
//! because the proc macro crate depends on this crate):
//!
//! ```
//! use panda::{runtime::{RuntimeError, Var}, Panda};
//!
//! // This needs to be Debug, Clone and Send. The debug is used when this
//! // value is printed with the std `println` function. This is cloned any
//! // time it is stored in global data. For example, putting this struct
//! // in a global HashMap. If your data is not Clone, you will need to make
//! // a wrapper struct that is an Arc.
//! //
//! // This is Send because everything in Panda is Send. The Rust API is
//! // able to start executing any function on any thread, so all data stored
//! // within the runtime environment (all global variables, functions, etc)
//! // must be Send. And because any variable can be declared as global, they
//! // all need to be Send. This may change in the future (there might be a
//! // parameter on the `define_ty` macro that will disallow this variable to
//! // enter global scope, but that would be a lot of work, so no promises).
//! #[derive(Debug, Clone)]
//! struct MyBuiltin {
//!   data: u32,
//! }
//!
//! // Magic time
//! #[panda::define_ty]
//! impl MyBuiltin {
//!   // This will declare a new global function called `new_builtin`.
//!   pub fn new_builtin() -> MyBuiltin {
//!     MyBuiltin { data: 5 }
//!   }
//!   // Return types are converted using `Into<Var>`
//!   pub fn get(&self) -> u32 {
//!     self.data
//!   }
//!   // Anything that implements `TryFromSpan<Var>` can be passed in.
//!   //
//!   // TODO: Get &mut self working again
//!   pub fn set(&self, val: u32) {
//!     // self.data = val
//!   }
//!   // Multiple variables require the exact number of parameters to be passed in.
//!   pub fn some_values_here(&self, a: &str, b: u8, c: i128, d: &MyData) {
//!     dbg!(self, a, b, c, d);
//!   }
//!   // Variadics are implemented like so. The variable is actually a Vec<&Var>,
//!   // and is converted with the proc macro.
//!   //
//!   // In this example, a minimum of 2 arguments is required (`anything` can be an
//!   // empty vec).
//!   // TODO: Reimplement variadics
//!   pub fn many_args(something: u32, another: u32, anything: Vec<Var>) {
//!     dbg!(something, another, anything);
//!   }
//!   // Functions may also return results, which will be checked in the generated code.
//!   pub fn maybe_works(&self) -> Result<(), RuntimeError> {
//!     Ok(())
//!   }
//! }
//!
//! #[derive(Debug, Clone)]
//! struct MyData {
//!   extra: u32,
//!   fields: String,
//! }
//!
//! // This makes MyData implement TryFromSpan<Var>.
//! #[panda_derive::define_ty]
//! impl MyData {
//!   // A nonsense constructor, so that we can create this in Panda.
//!   pub fn new_data() -> MyData {
//!     MyData {
//!       extra:  10,
//!       fields: "hello world".into(),
//!     }
//!   }
//! }
//!
//! fn main() {
//!   let mut sl = Panda::new();
//!   sl.env().add_std_builtins();
//!
//!   // The macro above makes our custom types implement a couple of traits,
//!   // so that this will work.
//!   sl.add_builtin_ty::<MyBuiltin>();
//!   sl.add_builtin_ty::<MyData>();
//!
//!   // Now everything just works
//!   let src = r#"
//!     v = new_builtin()
//!     v.set(2)
//!     v.set(6)
//!     println("Saved data:", v.get())
//!
//!     v.some_values_here("hello", 10, -1234565, new_data())
//!     println(v)
//!   "#;
//!   sl.exec_statement(src);
//!   println!("------");
//!
//!   // Argument errors work as you would expect (these all error at runtime)
//!   let src = r#"
//!     v = new_builtin()
//!     v.set("hello")
//!   "#;
//!   sl.exec_statement(src);
//!   println!("------");
//!   let src = r#"
//!     v = new_builtin()
//!     v.set(2, 3)
//!   "#;
//!   sl.exec_statement(src);
//!   println!("------");
//!   // TryFromSpan<Var> is implemented on u32, and will cause a runtime error
//!   // if you pass in a number that doesn't fit into a u32.
//!   let src = r#"
//!     v = new_builtin()
//!     v.set(-10)
//!   "#;
//!   sl.exec_statement(src);
//! }
//! ```
//!
//! # Docs
//!
//! This must be enabled with the `docs` feature. If it is enabled, then the
//! `Panda` type will have an extra function called `generate_docs`. This
//! function will generate a `Docs` struct, which can then be saved to disk.
//! This will document all builtin types that have been added so far.

#[cfg(feature = "docs")]
pub mod docs;

#[cfg(test)]
mod tests;

use panda_docs::MarkdownSection;
pub use panda_parse as parse;
pub use panda_parse::{path, PdError};
pub use panda_runtime as runtime;

pub use panda_derive::define_ty;

use panda_parse::{
  syntax::Parser,
  token::{ParseError, Span, Tokenizer},
  Path as TyPath, VarType,
};
use panda_runtime::{
  tree::StatementList as RtStatementList, BuiltinImpl, LockedEnv, RtEnv, RuntimeError, SlFile, Var,
};
use std::{fs, io::Read, path::Path, time::Duration};

/// The panda intepreter. This stores a running environment, which inclues
/// all of the functions, globals, and types defined within all of the files so
/// far. Typically, you would create a single instance of this, then
/// [`parse`](Panda::parse) all of your source files, and finally
/// [`call`](Panda::call) all of the functions you like.
pub struct Panda {
  env:   RtEnv,
  color: bool,
  files: Vec<SlFile>,
}

impl Default for Panda {
  fn default() -> Self {
    Panda::new()
  }
}

impl Panda {
  /// Creates a new panda parser. This stores an environment, which is used
  /// to store and parse all defined functions, types, and global variables.
  pub fn new() -> Self {
    // TODO: Check tty for colors
    Panda { env: RtEnv::new(), color: true, files: vec![] }
  }
  /// Executes a source file. Any functions declared in the source file will be
  /// added to the current environment, under the module `main`.
  ///
  /// `exec` will simply parse the given file, and then call the function
  /// `main::main`.
  /// ```
  /// # use panda::{path, Panda};
  /// # use std::path::Path;
  /// # fn main() -> Result<(), Box<dyn std::error::Error>> {
  /// # let src = "fn main() {}";
  /// let mut sl = Panda::new();
  ///
  /// // Parses the given source, and declares all the functions in the `main`
  /// // module. The file `main.pand` will be used in error messages. Note that
  /// // this will not read from disk at all!
  /// sl.parse_file(&path!(main), Path::new("main.pand"), src.to_string())?;
  ///
  /// // Calls the `main` function, inside the `main` module.
  /// sl.call(&path!(main::main))?;
  /// # Ok(())
  /// # }
  /// ```
  ///
  /// # Example:
  /// ```
  /// # use panda::{path, Panda};
  /// let mut sl = Panda::new();
  ///
  /// dbg!(sl.exec(
  ///   r#"
  ///   fn main() {
  ///     println("Hello world!")
  ///   }
  ///   "#.to_string()
  /// ));
  /// ```
  pub fn exec(&mut self, src: String) -> Var {
    let out = self.parse_file(&path!(main), Path::new("main.pand"), src);
    match out {
      Ok(_) => match self.call(&path!(main::main)) {
        Ok(v) => v,
        Err(e) => {
          self.print_err(e);
          Var::None
        }
      },
      Err(e) => {
        self.print_err(e);
        Var::None
      }
    }
  }
  /// Executes a statement. Shorthand for
  /// [`parse_statement`](Self::parse_statement) then [`run`](Self::run).
  ///
  /// Any functions or types that have already been declared are avalible to the
  /// statement. The statement is run at the root module, so it must use
  /// absolute paths to referernce any declared functions/types.
  ///
  /// # Example:
  /// ```
  /// # use panda::{path, Panda, runtime::Var};
  /// # use std::path::Path;
  /// let mut sl = Panda::new();
  ///
  /// assert_eq!(sl.exec_statement("5 + 6"), Var::Int(11));
  ///
  /// // Define a function
  /// sl.parse_file(
  ///   &path!(main), // Used for type declarations
  ///   Path::new("main.pand"), // Used for errors
  ///   r#"
  ///   fn add_one(v) {
  ///     v + 1
  ///   }
  ///   "#.to_string(),
  /// ).unwrap();
  /// // And use it in exec_statement
  /// assert_eq!(sl.exec_statement("main::add_one(3)"), Var::Int(4));
  /// ```
  pub fn exec_statement(&self, src: &str) -> Var {
    let out = self.parse_statement(src.as_bytes());
    match out {
      Ok(stat) => match self.run(stat) {
        Ok(v) => v,
        Err(e) => {
          e.print(src, Path::new("statement"), self.use_color());
          Var::None
        }
      },
      Err(e) => {
        e.print(src, Path::new("statement"), self.use_color());
        Var::None
      }
    }
  }

  /// Returns true if errors should be printed with colors. This defaults to
  /// checking the TTY settings, but can be overriden using
  /// [`set_color`](Self::set_color).
  pub fn use_color(&self) -> bool {
    // TODO: Implement color enable/disable
    self.color
  }

  /// Sets errors to be printed with or without errors. This only affects
  /// [`exec`](Self::exec) and [`exec_statement`](Self::exec_statement) calls.
  pub fn set_color(&mut self, enabled: bool) {
    self.color = enabled
  }

  /// Adds a new file into the internal list of stored files. This index is how
  /// errors lookup which file to access.
  ///
  /// If you are reading the file from the path, prefer
  /// [`add_file`](Self::add_file). It will make sure the file is adding into
  /// the file array before checking for errors. So if reading
  /// the file produces a file not found error, the error will display
  /// correctly.
  pub fn add_file_src(&mut self, ty_path: TyPath, path: &Path, src: String) -> u16 {
    self.files.push(SlFile::new(ty_path, path.into(), src));
    self.files.len() as u16 - 1
  }

  pub fn add_file(&mut self, ty_path: TyPath, path: &Path) -> Result<u16, ParseError> {
    self.files.push(SlFile::new(ty_path, path.into(), String::new()));
    let idx = self.files.len() as u16 - 1;
    self
      .files
      .last_mut()
      .unwrap()
      .set_src(fs::read_to_string(path).map_err(|e| ParseError::io(e, idx))?);
    Ok(idx)
  }

  /// Prints an error. If `use_color` is true, then colors will be used. This
  /// will use the file in the internal files list that the error has chosen.
  pub fn print_err<E: PdError>(&self, err: E) {
    let file = &self.files[err.pos().file() as usize];
    err.print(file.src(), file.path(), self.use_color());
  }

  /// Generates an error message. If `use_color` is true, then colors will be
  /// used in the resulting string.
  pub fn gen_err<E: PdError>(&self, err: E) -> String {
    let file = &self.files[err.pos().file() as usize];
    err.gen(file.src(), file.path(), self.use_color())
  }

  /// Parses a single statement.
  pub fn parse_statement<R: Read>(&self, src: R) -> Result<RtStatementList, ParseError> {
    let s = Parser::<R>::new(Tokenizer::new(src, 0)).parse_statement()?;
    let s = self.env.compile_stat_list(s)?;
    Ok(s)
  }
  /// Parses a file. All functions, globals, and type definitions in that file
  /// will be added to the global scope.
  pub fn parse_file(
    &mut self,
    ty_path: &TyPath,
    path: &Path,
    src: String,
  ) -> Result<(), ParseError> {
    self.env.declare_module(ty_path);
    let idx = self.add_file_src(ty_path.clone(), path, src);
    self.first_pass_file(idx)?;
    self.env.second_pass()?;
    Ok(())
  }
  /// Parses a directory. All functions, globals, and type definitions in any
  /// files ending in .pand will be added to the global scope. This is done at
  /// the same time, so files can depend on each other without issues.
  pub fn parse_dir(&mut self, dir: &Path, path: &TyPath) -> Result<(), ParseError> {
    self.first_pass_dir(dir, path)?;
    self.env.second_pass()?;
    Ok(())
  }

  /// Performs the first pass for parsing an entire directory.
  pub fn first_pass_dir(&mut self, dir: &Path, ty_path: &TyPath) -> Result<(), ParseError> {
    let dir_idx = self.add_file_src(ty_path.clone(), dir, "".into());
    for res in fs::read_dir(dir).map_err(|e| ParseError::io(e, dir_idx))? {
      let path = res.map_err(|e| ParseError::io(e, dir_idx))?.path();
      if path.is_dir() {
        self.first_pass_dir(
          &path,
          &ty_path.join(&path.file_stem().unwrap().to_str().unwrap().into()),
        )?;
      } else if path.extension() == Some("pand".as_ref()) {
        let name = path.file_stem().unwrap().to_str().unwrap().into();
        let idx = self.add_file(ty_path.join(&name), &path)?;
        self.first_pass_file(idx)?;
        // self.first_pass_file(File::open(path)?, &ty_path.join(&name))?;
      }
    }
    Ok(())
  }

  /// Performs the first pass for parsing a file.
  fn first_pass_file(&mut self, index: u16) -> Result<(), ParseError> {
    let file = &self.files[index as usize];
    let file = Parser::new(Tokenizer::new(file.src().as_bytes(), index))
      .parse_file(file.ty_path().clone())?;
    self.env.first_pass_file(file);
    Ok(())
  }

  /// Returns the runtime environment.
  pub fn env(&mut self) -> &mut RtEnv {
    &mut self.env
  }
  /// Returns the runtime environment, with a reference to the list of known
  /// files. This is returned so that `env.lock(files)` can be called.
  pub fn env_files(&mut self) -> (&mut RtEnv, &[SlFile]) {
    (&mut self.env, &self.files)
  }
  /// Adds a new builtin function.
  pub fn add_builtin_fn<
    F: Fn(&mut LockedEnv, Var, Vec<Var>, Span) -> Result<Var, RuntimeError> + 'static + Send + Sync,
  >(
    &mut self,
    path: TyPath,
    docs: MarkdownSection,
    takes_self: bool,
    f: F,
  ) {
    self.env.add_builtin_fn(path, docs, takes_self, f);
  }
  /// Adds a new builtin type. See the [`define_ty`] proc macro for more.
  pub fn add_builtin_ty<T: BuiltinImpl + 'static>(&mut self) {
    self.env.add_builtin_ty::<T>();
  }
  /// Calls a function. The path must be the absolute module path of where the
  /// function was defined. If you need to pass in arguments, see
  /// [`call_args`](Self::call_args).
  pub fn call(&self, path: &TyPath) -> Result<Var, RuntimeError> {
    self.env.call(path, None, vec![], &self.files)
  }
  /// Calls a function. The path must be the absolute module path of where the
  /// function was defined.
  ///
  /// # Panics
  /// - If the function is not defined.
  pub fn call_args(&self, path: &TyPath, args: Vec<Var>) -> Result<Var, RuntimeError> {
    self.env.call(path, None, args, &self.files)
  }
  /// Checks if the function at the given path exists.
  pub fn has_func(&self, path: &TyPath) -> bool {
    self.env.has_func(path)
  }
  /// Runs a statement.
  pub fn run(&self, stat: RtStatementList) -> Result<Var, RuntimeError> {
    let mut lock = self.env.lock(&self.files);
    stat.exec(&mut lock)
  }

  /// Defines the given callback as a valid callback. This must be called before
  /// any files are parsed.
  pub fn def_callback(&mut self, name: &str, args: Vec<VarType>, docs: MarkdownSection) {
    self.env.add_callback(name.into(), args, docs);
  }
  /// Executes the callback with the given name. Panics if there is no callback
  /// with that name.
  pub fn run_callback(&self, name: &str, args: Vec<Var>) -> Result<(), RuntimeError> {
    let mut lock = self.env.lock(&self.files);
    lock.run_callback(name, args)
  }

  /// Defined a predefined variable. This can be accessed with @<name>, and has
  /// global scope.
  pub fn predefine(
    &mut self,
    name: &str,
    docs: MarkdownSection,
    get: impl Fn() -> Var + Send + Sync + 'static,
  ) {
    self.env.predefine(name.into(), docs, Box::new(get));
  }

  /// Sets the execution time limit. If any function is executed (with [`run`],
  /// [`call`], or [`call_args`]), those calls will be cancelled if they take
  /// longer than `time`.
  ///
  /// If the `time` is set to `None`, then no limits will be imposed.
  ///
  /// [`run`]: Self::run
  /// [`call`]: Self::call
  /// [`call_args`]: Self::call_args
  pub fn set_time_limit(&mut self, time: Option<Duration>) {
    self.env.time_limit = time;
  }
}
