use crate::{path, Panda};
use std::path::Path;

#[track_caller]
pub fn exec(src: &str) {
  let mut sl = Panda::new();
  let idx = sl.add_file_src(path!(main), Path::new("test"), src.into());
  match sl.first_pass_file(idx) {
    Ok(_) => {}
    Err(e) => {
      sl.print_err(e);
      panic!("failed during first pass");
    }
  }
  sl.env().second_pass().unwrap_err();
}

#[test]
fn undefined() {
  exec(
    "
    fn test() {
      undefined_function()
    }
    ",
  );
  exec(
    "
    fn test() {
      UndefinedType {}
    }
    ",
  );
  exec(
    "
    fn test() {
      undefined::module::func()
    }
    ",
  );
  exec(
    "
    fn test() {
      undefined::module::Type {}
    }
    ",
  );
}

#[test]
fn args() {
  exec(
    "
    fn test() {
      my_func()
    }
    fn my_func(arg) {}
    ",
  );
  exec(
    "
    fn test() {
      my_func(5, 6)
    }
    fn my_func(arg) {}
    ",
  );
}
