#![allow(clippy::nonstandard_macro_braces)]

use std::collections::HashMap;
use panda::Panda;

// This needs to be Debug and Clone. The debug is used when this value is
// printed with the std `println` function. The clone is used any time
// you look up the variable in the variables list (I need to fix this, but
// haven't yet). If your data isn't Clone, you will need to wrap your data
// in an Arc.
#[derive(Debug, Clone)]
struct MyBuiltin {
  data: u32,
}

#[derive(Debug, Clone)]
struct MyData {
  a: String,
  b: u32,
}

#[panda_derive::define_ty]
impl MyData {
  pub fn new_data() -> MyData {
    MyData { a: "HELLO WORLD".into(), b: 5 }
  }
}

// Magic time
#[panda_derive::define_ty]
impl MyBuiltin {
  // This will declare a new global function called `new_builtin`.
  pub fn new_builtin() -> MyBuiltin {
    MyBuiltin { data: 5 }
  }
  // Return values are always converted with `try_into`
  pub fn get(&self) -> u32 {
    self.data
  }
  // Arguments converted into number types, and will throw a runtime error
  // if they don't fit.
  pub fn set(&mut self, val: u32) {
    self.data = val
  }
  // Strings work like this
  #[allow(clippy::ptr_arg)]
  pub fn dbg(&self, a: &str, b: &String) {
    dbg!(a, b, self);
  }
  // Bools are easy as well
  pub fn truthy(&self, v: bool) {
    dbg!(v);
  }
  // Floats are converted with `as`
  pub fn precision(&self, v: f64, another: f32) {
    dbg!(v, another);
  }
  // Arrays must be by value, since they are built at runtime by the proc macro
  pub fn gaming(&self, v: Vec<u8>) {
    dbg!(v);
  }
  // Same goes for maps. The keys are more constrained than a normal variable,
  // but they work mostly the same. The main difference is that you can't use
  // floats (or anything that is not Eq + Hash).
  pub fn lots_of_data(&self, v: HashMap<u32, &str>) {
    dbg!(v);
  }
  // Passing in other types will work, so long as they have the `#[define_ty]`
  // attribute on exactly one of their impl blocks (it can be empty, it just needs
  // to be there).
  pub fn other_data(&self, v: &MyData) {
    dbg!(v);
  }
}

fn main() {
  let mut sl = Panda::new();
  sl.env().add_std_builtins();

  // This is why we needed to use the macro above
  sl.add_builtin_ty::<MyBuiltin>();
  sl.add_builtin_ty::<MyData>();

  // Now everything just works
  let src = r#"
    v = new_builtin()
    v.set(2)
    v.set(6)
    v.truthy(false)
    v.precision(2.3, 1.2)
    v.gaming([2, 3, 5])
    v.lots_of_data(map{2: "hello"})
    v.other_data(new_data())
    println()
    println(v)
    v.dbg("hello", "world")
    println("Saved data:", v.get())
  "#;
  sl.exec_statement(src);
  println!("------");

  // Argument errors work as you would expect (these all error at runtime)
  let src = r#"
    v = new_builtin()
    v.set(-5)
    v.set("hello")
  "#;
  sl.exec_statement(src);
  println!("------");
  let src = r#"
    v = new_builtin()
    v.set(2, 3)
  "#;
  sl.exec_statement(src);
  println!("------");
  let src = r#"
    v = new_builtin("hello")
    v.set(2)
  "#;
  sl.exec_statement(src);
}
