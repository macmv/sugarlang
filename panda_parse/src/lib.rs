//! The parser for Panda. This does everything involved in convering the
//! source code into an abstract syntax tree.
//!
//! Example of the tokenizer:
//! ```
//! use panda_parse::token::{Ident, Keyword, Token, Tokenizer};
//!
//! let mut tok = Tokenizer::new("0 5.0 -10.0 hello".as_bytes(), 0);
//! assert!(matches!(tok.read().unwrap(), Token::Lit(l) if l.unwrap_int() == 0));
//! assert!(matches!(tok.read().unwrap(), Token::Lit(l) if l.unwrap_float() == 5.0));
//! assert!(tok.read().unwrap().is_keyword(Keyword::Sub));
//! assert!(matches!(tok.read().unwrap(), Token::Lit(l) if l.unwrap_float() == 10.0));
//! assert!(matches!(tok.read().unwrap(), Token::Ident(i) if i.as_ref() == "hello"));
//! assert_eq!(tok.read(), Ok(Token::EOF(0)));
//! ```
//!
//! Example of the syntax analysis (see [`syntax`] for more):
//! ```
//! use panda_parse::{
//!   tree::{Expr, Op},
//!   syntax::Parse,
//!   token::{Ident, Keyword, Pos, Span, Token, Tokenizer},
//!   span
//! };
//!
//! let expr = Expr::parse(&mut Tokenizer::new("i * j".as_bytes(), 0)).unwrap();
//! let (lhs, rhs, op) = match &expr {
//!   Expr::Op(lhs, rhs, op) => (lhs, rhs, op),
//!   v => panic!("invalid expr kind {:?}", v),
//! };
//! assert_eq!(*op, Op::Mul);
//! assert_eq!(**lhs.as_ref().unwrap(), Expr::Ident(Ident::new("i", span!(1..2))));
//! assert_eq!(**rhs.as_ref().unwrap(), Expr::Ident(Ident::new("j", span!(5..6))));
//! ```
//!
//! The AST is only used in execution, so this overlaps with the
//! [`runtime`](crate::runtime) module a lot.

mod err;
mod path;
pub mod syntax;
pub mod token;
pub mod tree;

pub use err::PdError;
pub use path::Path;

pub use ansi_term;

use std::fmt;

/// A variable type. The langauge types are there own variants, and custom
/// builtins are represented with the `Builtin` variant. The `None` variant is
/// used whenever something either does not have a type, or doesn't set a
/// specific type. For example, it's used for struct fields that are declared
/// with the type `any`.
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum VarType {
  String,
  Int,
  Float,
  Bool,
  Struct(Path),
  Map,
  Array,
  Range,
  None,

  Builtin(Path),
  // TODO: Args should be part of the type
  Callback,
  Closure,
}

impl VarType {
  pub fn to_path(&self) -> Path {
    match self {
      Self::String => path!(str),
      Self::Int => path!(int),
      Self::Float => path!(float),
      Self::Bool => path!(bool),
      Self::Struct(path) => path.clone(),
      Self::Map => path!(map),
      Self::Array => path!(arr),
      Self::Range => path!(range),
      Self::None => path!(),

      Self::Builtin(path) => path.clone(),
      Self::Callback => unimplemented!("cannot get path of callback"),
      Self::Closure => panic!("closure has no path"),
    }
  }
}

impl fmt::Display for VarType {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    match self {
      Self::String => write!(f, "string"),
      Self::Int => write!(f, "int"),
      Self::Float => write!(f, "float"),
      Self::Bool => write!(f, "bool"),
      Self::Struct(name) => write!(f, "{}", name),
      Self::Map => write!(f, "map"),
      Self::Array => write!(f, "array"),
      Self::Range => write!(f, "range"),
      Self::None => write!(f, "any"),
      Self::Builtin(path) => write!(f, "builtin {}", path),
      Self::Callback => write!(f, "callback"),
      Self::Closure => write!(f, "closure"),
    }
  }
}
