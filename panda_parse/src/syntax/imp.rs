use crate::{
  syntax::Parse,
  token::{Keyword, PathLit, Result, Tokenizer},
  tree::{FuncDef, Impl},
};
use std::io::Read;

impl Parse for Impl {
  type Output = Self;

  fn parse<R: Read>(tok: &mut Tokenizer<R>) -> Result<Self> {
    // This parses an impl block. The type on the block and all the functions is
    // incomplete, as it does not include the full module path.
    tok.expect(Keyword::Impl)?;
    let mut struct_path = PathLit::parse(tok)?;

    // check for `impl Trait for Struct` syntax. if this is the case, we need to
    // swap out the struct path for the trait path.
    let t = tok.peek(0)?;
    let trait_path = if t.is_keyword(Keyword::For) {
      Some(std::mem::replace(&mut struct_path, PathLit::parse(tok)?))
    } else {
      None
    };

    tok.expect(Keyword::OpenBlock)?;
    let mut funcs = vec![];
    loop {
      if tok.peek(0)?.is_keyword(Keyword::CloseBlock) {
        break;
      }
      funcs.push(FuncDef::parse(tok)?);
    }
    tok.expect(Keyword::CloseBlock)?;

    Ok(Impl { struct_path, trait_path, funcs })
  }
}
