use crate::{
  syntax::Parse,
  token::{Keyword, ParseError, Result, Tokenizer},
  tree::{Import, ImportTree},
};
use std::io::Read;

impl Parse for Import {
  type Output = Self;

  fn parse<R: Read>(tok: &mut Tokenizer<R>) -> Result<Self> {
    tok.expect(Keyword::Use).unwrap();

    let tree = ImportTree::parse(tok)?;
    tree.check_duplicates()?;
    Ok(Import { tree })
  }
}

impl Parse for ImportTree {
  type Output = Self;

  fn parse<R: Read>(tok: &mut Tokenizer<R>) -> Result<Self> {
    let name = tok.read()?.ident("an import path")?;

    let mut children = vec![];
    if tok.peek(0)?.is_keyword(Keyword::PathSep) {
      tok.expect(Keyword::PathSep).unwrap();
    } else {
      return Ok(ImportTree { name, children });
    }

    if tok.peek(0)?.is_keyword(Keyword::OpenBlock) {
      tok.expect(Keyword::OpenBlock).unwrap();
      loop {
        children.push(ImportTree::parse(tok)?);

        if tok.peek(0)?.is_keyword(Keyword::Comma) {
          tok.expect(Keyword::Comma).unwrap();
          if tok.peek(0)?.is_keyword(Keyword::CloseBlock) {
            break;
          }
        } else if tok.peek(0)?.is_keyword(Keyword::CloseBlock) {
          break;
        }
      }
      tok.expect(Keyword::CloseBlock)?;
    } else if tok.peek(0)?.is_ident() {
      children.push(ImportTree::parse(tok)?);
    } else {
      return Err(ParseError::unexpected(tok.read()?, "an import path or `{`"));
    }

    Ok(ImportTree { name, children })
  }
}
