use crate::{
  syntax::Parse,
  token::{Keyword, Result, Tokenizer},
  tree::{Expr, If, StatementList},
};
use std::io::Read;

impl If {
  pub fn parse<R: Read>(tok: &mut Tokenizer<R>) -> Result<Self> {
    // The if token has already been parsed
    let cond = Expr::parse(tok)?;
    tok.expect(Keyword::OpenBlock)?;
    let block = StatementList::parse(tok)?;
    tok.expect(Keyword::CloseBlock)?;

    let mut else_if = vec![];
    let mut else_block = None;

    loop {
      if tok.peek_opt(0)?.map(|v| v.is_keyword(Keyword::Else)) == Some(true) {
        tok.read()?;
        if tok.peek(0)?.is_keyword(Keyword::If) {
          tok.expect(Keyword::If)?;
          let cond = Expr::parse(tok)?;
          tok.expect(Keyword::OpenBlock)?;
          let block = StatementList::parse(tok)?;
          tok.expect(Keyword::CloseBlock)?;
          else_if.push((cond, block));
          // We just finished reading an else if block. If the token after this
          // is not an else, then we break. So, we need to have end
          // correctly updated by noe.
        } else {
          tok.expect(Keyword::OpenBlock)?;
          let block = StatementList::parse(tok)?;
          tok.expect(Keyword::CloseBlock)?;
          else_block = Some(block);
          // Not if keyword, so this is the last element in the else if block.
          break;
        }
      } else {
        break;
      }
    }

    Ok(If { cond, block, else_if, else_block })
  }
}
