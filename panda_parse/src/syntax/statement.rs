use crate::{
  syntax::Parse,
  token::{Keyword, ParseError, Result, Token, Tokenizer},
  tree::{AssignOp, AssignOpKind, Expr, If, LHSKind, Loop, Op, Statement, StatementList, LHS},
};
use std::io::Read;

impl Parse for Statement {
  type Output = Self;

  fn parse<R: Read>(tok: &mut Tokenizer<R>) -> Result<Self> {
    match tok.read()? {
      t if t.is_keyword(Keyword::Let) => {
        let name = tok.read()?.ident("variable")?;
        if tok.peek_opt(0)?.map(|t| t.is_keyword(Keyword::Assign)).unwrap_or(false) {
          tok.expect(Keyword::Assign)?;
          Ok(Statement::Let(name, Some(Expr::parse(tok)?)))
        } else {
          Ok(Statement::Let(name, None))
        }
      }
      t if t.is_keyword(Keyword::If) => Ok(Statement::If(If::parse(tok)?)),
      t if t.is_keyword(Keyword::For) => Ok(Statement::Loop(Loop::parse_for(tok, t.pos())?)),
      t if t.is_keyword(Keyword::While) => Ok(Statement::Loop(Loop::parse_while(tok, t.pos())?)),
      t if t.is_keyword(Keyword::Loop) => Ok(Statement::Loop(Loop::parse_loop(tok, t.pos())?)),
      t if t.is_keyword(Keyword::Break) => Ok(Statement::Break(t.pos())),
      t if t.is_keyword(Keyword::Continue) => Ok(Statement::Continue(t.pos())),
      t => {
        tok.unread(t);
        let lhs = Expr::parse(tok)?;
        match tok.peek_opt(0)? {
          Some(t) if is_assign(t) => Ok(Statement::Assign(AssignOp::parse(tok, lhs)?)),
          _ => Ok(Statement::Expr(lhs)),
        }
      }
    }
  }
}

impl AssignOp {
  fn parse<R: Read>(tok: &mut Tokenizer<R>, lhs: Expr) -> Result<Self> {
    let lhs = Self::parse_lhs(lhs)?;
    Ok(AssignOp {
      lhs,
      op: match tok.read()? {
        t if t.is_keyword(Keyword::Assign) => AssignOpKind::Set(Expr::parse(tok)?),
        t if t.is_keyword(Keyword::Inc) => AssignOpKind::Inc,
        t if t.is_keyword(Keyword::Dec) => AssignOpKind::Dec,
        t if t.is_keyword(Keyword::AddAssign) => AssignOpKind::Add(Expr::parse(tok)?),
        t if t.is_keyword(Keyword::SubAssign) => AssignOpKind::Sub(Expr::parse(tok)?),
        t if t.is_keyword(Keyword::DivAssign) => AssignOpKind::Div(Expr::parse(tok)?),
        t if t.is_keyword(Keyword::MulAssign) => AssignOpKind::Mul(Expr::parse(tok)?),
        t if t.is_keyword(Keyword::ModAssign) => AssignOpKind::Mod(Expr::parse(tok)?),
        t if t.is_keyword(Keyword::ExpAssign) => AssignOpKind::Exp(Expr::parse(tok)?),
        _ => unreachable!(),
      },
    })
  }

  fn parse_lhs(lhs: Expr) -> Result<LHS> {
    match lhs {
      Expr::Ident(name) => Ok(LHS { prefix: None, last: LHSKind::Name(name) }),
      Expr::Op(lhs, rhs, kind) if kind == Op::Dot => {
        Ok(LHS { prefix: Some(*lhs.unwrap()), last: LHSKind::Name(rhs.unwrap().unwrap_ident()) })
      }
      _ => Err(ParseError::custom("Cannot assign to this expression".to_string(), lhs.pos())),
    }
  }
}

fn is_assign(t: &Token) -> bool {
  t.is_keyword(Keyword::Assign)
    || t.is_keyword(Keyword::Inc)
    || t.is_keyword(Keyword::Dec)
    || t.is_keyword(Keyword::AddAssign)
    || t.is_keyword(Keyword::SubAssign)
    || t.is_keyword(Keyword::DivAssign)
    || t.is_keyword(Keyword::MulAssign)
    || t.is_keyword(Keyword::ModAssign)
    || t.is_keyword(Keyword::ExpAssign)
}

impl Parse for StatementList {
  type Output = Self;

  fn parse<R: Read>(tok: &mut Tokenizer<R>) -> Result<Self> {
    let mut items = vec![];
    loop {
      match tok.peek_opt(0)? {
        Some(t) if t.is_keyword(Keyword::CloseBlock) => return Ok(StatementList { items }),
        None => return Ok(StatementList { items }),
        _ => items.push(Statement::parse(tok)?),
      }
    }
  }
}

#[cfg(test)]
mod tests {
  use super::*;
  use crate::{
    span,
    token::Ident,
    tree::{BoolLit, LHSKind, Lit, Loop},
  };

  #[test]
  fn assigns() {
    match Statement::parse(&mut Tokenizer::new("bar = 5".as_bytes(), 0)).unwrap() {
      Statement::Assign(op) => {
        assert!(op.lhs.prefix.is_none());
        assert_eq!(op.lhs.last, LHSKind::Name(Ident::new("bar", span!(1..4))));
      }
      _ => panic!(),
    }
    match Statement::parse(&mut Tokenizer::new("foo.bar = 5".as_bytes(), 0)).unwrap() {
      Statement::Assign(op) => {
        assert_eq!(op.lhs.prefix.unwrap(), Expr::Ident(Ident::new("foo", span!(1..4))));
        assert_eq!(op.lhs.last, LHSKind::Name(Ident::new("bar", span!(5..8))));
      }
      _ => panic!(),
    }
  }

  #[test]
  fn while_loops() {
    match Statement::parse(&mut Tokenizer::new("while true {}".as_bytes(), 0)).unwrap() {
      Statement::Loop(Loop::While { cond, block, .. }) => {
        assert_eq!(cond, Expr::Lit(Lit::Bool(BoolLit { pos: span!(7..11), val: true })));
        assert!(block.items.is_empty());
      }
      _ => panic!(),
    }
  }
}
