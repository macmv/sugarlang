use super::{Expr, StatementList};
use crate::{
  token::{Ident, PathLit, Span},
  VarType,
};

#[derive(Debug, Clone, PartialEq)]
pub struct FuncCall {
  /// The name of the function.
  pub name: PathLit,
  /// If some, then this function was called like `"some text".split(" ")`
  /// slf will be the string "some text".
  pub slf:  Option<Box<Expr>>,
  /// Args to the function.
  pub args: Vec<Expr>,
}

#[derive(Debug, Clone)]
pub struct FuncDef {
  /// Name of the function.
  pub name: Ident,
  /// Arguments for this function.
  pub args: Args,
  /// The actual body to execute when the function is called.
  pub body: StatementList,
}

#[derive(Debug, Clone, PartialEq)]
pub struct Args {
  pub pos:      Span,
  pub names:    Vec<(Ident, Option<VarType>)>,
  pub variadic: Option<Ident>,
}

impl Args {
  pub fn takes_self(&self) -> bool {
    matches!(self.names.first(), Some((name, _)) if name.as_ref() == "self")
  }
  pub fn check_args_rt(&self, called_on_self: bool, args_len: usize) -> Result<(), (String, Span)> {
    let mut expected_len = self.names.len();
    if self.takes_self() && called_on_self {
      expected_len -= 1;
    }
    if self.variadic.is_some() {
      // If we have variadics, then we need at least names args
      if args_len < expected_len {
        Err((
          format!(
            "expected at least {} argument{}, but was called with {} argument{}",
            self.names.len(),
            if self.names.len() == 1 { "" } else { "s" },
            args_len,
            if args_len == 1 { "" } else { "s" },
          ),
          self.pos,
        ))
      } else {
        Ok(())
      }
    } else {
      // If we don't have variadics, then we need exactly names args
      if args_len != expected_len {
        Err((
          format!(
            "expected exactly {} argument{}, but was called with {} argument{}",
            self.names.len(),
            if self.names.len() == 1 { "" } else { "s" },
            args_len,
            if args_len == 1 { "" } else { "s" },
          ),
          self.pos,
        ))
      } else {
        Ok(())
      }
    }
  }
}
