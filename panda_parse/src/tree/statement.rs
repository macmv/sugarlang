use super::{AssignOp, Expr, If, Loop};
use crate::token::{Ident, Span};

#[derive(Debug, Clone, PartialEq)]
pub enum Statement {
  Break(Span),
  Continue(Span),
  Assign(AssignOp),
  // FuncDef(FuncDef),
  If(If),
  Loop(Loop),
  Let(Ident, Option<Expr>),
  Expr(Expr),
}

#[derive(Debug, Clone, PartialEq)]
pub struct StatementList {
  pub items: Vec<Statement>,
}

impl StatementList {
  pub fn items(&self) -> &[Statement] {
    &self.items
  }
}
