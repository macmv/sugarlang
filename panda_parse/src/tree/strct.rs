use super::{Args, Expr, StatementList};
use crate::{
  token::{Group, Ident},
  VarType,
};
use std::collections::HashMap;

// A struct definition. Only appears outside of functions.
#[derive(Debug, Clone)]
pub struct Struct {
  pub name:   Ident,
  pub fields: HashMap<String, VarType>,
  pub order:  Vec<Ident>,
}

#[derive(Debug, Clone, PartialEq)]
pub struct StructLit {
  pub name:   Ident,
  pub fields: Group<StructItem>,
}

#[derive(Debug, Clone, PartialEq)]
pub struct StructItem {
  pub name: Ident,
  pub val:  Expr,
}

#[derive(Debug, Clone, PartialEq)]
pub struct Trait {
  pub name:  Ident,
  pub funcs: HashMap<String, TraitFuncDef>,
  pub order: Vec<Ident>,
}

#[derive(Debug, Clone, PartialEq)]
pub struct TraitFuncDef {
  /// Name of the function.
  pub name: Ident,
  /// Arguments for this function.
  pub args: Args,
  /// The actual body to execute when the function is called.
  pub body: Option<StatementList>,
}
