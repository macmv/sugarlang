use super::{Expr, StatementList};
use crate::token::{Ident, Span};

#[derive(Debug, Clone, PartialEq)]
pub enum Loop {
  For {
    first:       Ident,
    second:      Option<Ident>,
    iter:        Expr,
    block:       StatementList,
    keyword_pos: Span,
  },
  While {
    cond:        Expr,
    block:       StatementList,
    keyword_pos: Span,
  },
  Loop {
    block:       StatementList,
    keyword_pos: Span,
  },
}
