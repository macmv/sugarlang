use panda_parse::Path as TyPath;
use std::path::{Path, PathBuf};

#[derive(Debug)]
pub struct SlFile {
  ty_path: TyPath,
  path:    PathBuf,
  src:     String,
}

impl SlFile {
  pub fn new(ty_path: TyPath, path: PathBuf, src: String) -> Self {
    SlFile { ty_path, path, src }
  }

  #[inline(always)]
  pub fn set_src(&mut self, new_src: String) {
    self.src = new_src
  }

  pub fn ty_path(&self) -> &TyPath {
    &self.ty_path
  }
  pub fn path(&self) -> &Path {
    &self.path
  }
  pub fn src(&self) -> &str {
    &self.src
  }
}
