//! This is the final pass. This module passes over all newly added functions
//! from the second pass, and validates all the statements. Currently, this only
//! checks for function arguments, but it may do more in the future.

use super::RtEnv;
use crate::{
  tree::{AssignOpKind, Expr, LHSKind, Lit, Loop, Statement, StatementList, LHS},
  Item, ParseError, Path,
};
use panda_parse::token::Result;
use std::collections::HashSet;

impl RtEnv {
  pub(super) fn validate_functions(&self, funcs: HashSet<Path>) -> Result<()> {
    let mut err = ParseError::Empty;
    for p in funcs {
      err.try_join(self.validate_stat_list(match &self.names[&p] {
        Item::Func(callable) => match callable.imp.as_func_def() {
          Some(v) => &v.body,
          None => continue,
        },
        _ => unreachable!(),
      }));
    }
    err.into_result(())
  }

  pub fn validate_stat_list(&self, s: &StatementList) -> Result<()> {
    let mut err = ParseError::Empty;
    for s in &s.items {
      err.try_join(self.validate_stat(s));
    }
    err.into_result(())
  }
  fn validate_stat(&self, s: &Statement) -> Result<()> {
    let mut err = ParseError::Empty;
    match s {
      Statement::Break(_) | Statement::Continue(_) => {}
      Statement::Assign(op) => {
        match &op.lhs {
          LHS::Value(prefix, val) => {
            err.try_join(self.validate_expr(prefix));
            match val {
              LHSKind::Index(index) => err.try_join(self.validate_expr(index)),
              LHSKind::Name(_) => {}
            }
          }
          LHS::Var(_) => {}
        }
        match &op.op {
          AssignOpKind::Inc | AssignOpKind::Dec => {}
          AssignOpKind::Set(expr) => err.try_join(self.validate_expr(expr)),
          AssignOpKind::Add(expr) => err.try_join(self.validate_expr(expr)),
          AssignOpKind::Sub(expr) => err.try_join(self.validate_expr(expr)),
          AssignOpKind::Mul(expr) => err.try_join(self.validate_expr(expr)),
          AssignOpKind::Div(expr) => err.try_join(self.validate_expr(expr)),
          AssignOpKind::Exp(expr) => err.try_join(self.validate_expr(expr)),
          AssignOpKind::Mod(expr) => err.try_join(self.validate_expr(expr)),
        }
      }
      Statement::If(stat) => {
        err.try_join(self.validate_expr(&stat.cond));
        err.try_join(self.validate_stat_list(&stat.block));
        for (cond, block) in &stat.else_if {
          err.try_join(self.validate_expr(cond));
          err.try_join(self.validate_stat_list(block));
        }
        if let Some(block) = &stat.else_block {
          err.try_join(self.validate_stat_list(block));
        }
      }
      Statement::Loop(stat) => match stat {
        Loop::For { iter, block, .. } => {
          err.try_join(self.validate_expr(iter));
          err.try_join(self.validate_stat_list(block));
        }
        Loop::While { cond, block, .. } => {
          err.try_join(self.validate_expr(cond));
          err.try_join(self.validate_stat_list(block));
        }
        Loop::Loop { block, .. } => {
          err.try_join(self.validate_stat_list(block));
        }
      },
      Statement::Let(_, expr) => {
        if let Some(e) = expr {
          err.try_join(self.validate_expr(e))
        }
      }
      Statement::Expr(expr) => err.try_join(self.validate_expr(expr)),
    }
    err.into_result(())
  }
  fn validate_expr(&self, e: &Expr) -> Result<()> {
    let mut err = ParseError::Empty;
    match e {
      Expr::Lit(lit) => match lit {
        Lit::Arr(items) => {
          for expr in &items.val {
            err.try_join(self.validate_expr(expr));
          }
        }
        Lit::Map(items) => {
          for (k, v) in &items.val {
            err.try_join(self.validate_expr(k));
            err.try_join(self.validate_expr(v));
          }
        }
        Lit::Struct(v) => {
          let path = &v.name;
          match self.names.get(path) {
            Some(Item::Struct(callable)) => {
              err.try_join(callable.imp.validate_fields(v.name.pos(), &v.fields))
            }
            Some(_) => {
              err.join(ParseError::custom(format!("not a struct: {}", path.path()), path.pos()))
            }
            _ => {
              err.join(ParseError::custom(format!("undefined struct: {}", path.path()), path.pos()))
            }
          }
          for (_, v) in &v.fields {
            err.try_join(self.validate_expr(v));
          }
        }
        _ => {}
      },
      Expr::Paren(expr) => err.try_join(self.validate_expr(expr)),

      Expr::Op(op) => {
        err.try_join(self.validate_expr(op.lhs()));
        if let Some(rhs) = op.rhs() {
          err.try_join(self.validate_expr(rhs))
        }
      }

      Expr::Call(slf, path, args) => {
        if slf.is_none() {
          match self.names.get(path) {
            Some(Item::Func(callable)) => {
              err.try_join(callable.imp.check_args(args.pos, &args.args))
            }
            Some(_) => {
              err.join(ParseError::custom(format!("not a function: {}", path.path()), path.pos()))
            }
            _ => err
              .join(ParseError::custom(format!("undefined function: {}", path.path()), path.pos())),
          }
        }
        for a in &args.args {
          err.try_join(self.validate_expr(a));
        }
      }
      Expr::Field(value, _name) => err.try_join(self.validate_expr(value)),
      Expr::Block(_, list) => err.try_join(self.validate_stat_list(list)),
      Expr::IdRef(_) => {}
      Expr::FuncRef(_) => {}
      Expr::Predefined(_) => {}
    }
    err.into_result(())
  }
}
