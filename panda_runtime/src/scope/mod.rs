mod convert;
mod validation;

use crate::{
  tree::{FuncDef, OnEvent, StatementList as RtStatementList, Struct},
  var::VarID,
  Item, ParsedFile, Path, PathLit, RtEnv, VarGetter,
};
use panda_parse::{
  token::{Ident, ParseError, Result, Span},
  tree::StatementList,
};
use std::{
  collections::{HashMap, HashSet},
  mem,
  sync::{Arc, Mutex},
};

#[derive(Debug)]
struct LocalVar {
  id:          usize,
  pos:         Span,
  /// The variable is defined in the outer scope, or index 0 in this array. If
  /// we set the variable say one level deeper, then index 1 in this array will
  /// be set. When we leave a scope, this will be resolved (depending on the
  /// type of block), and the 0 index will be updated to match. This will never
  /// be empty.
  initialized: Vec<Initialized>,
}

#[derive(Debug, Clone, Copy, PartialEq)]
enum Initialized {
  /// Used if the variable is initialized in the current block.
  Yes,
  /// When we call `swap_else_if` or `swap_else`, we set all variables with
  /// `Yes` to `InPreviousElse`. All variables set to `InPreviousElse` get set
  /// to `No`. Only at the end of the if chain do we make sure that all
  /// variables are set to `Yes`.
  InPreviousElse,
  /// If we find any block that doesn't contain this variable, we set it to
  /// `No`.
  No,
}

#[derive(Debug)]
struct Level {
  vars:    HashMap<String, LocalVar>,
  is_else: bool,
}

/// An environment for the second pass. This is used to keep track of all
/// local variable names, imports, etc.
#[derive(Debug)]
pub struct CompileEnv<'a> {
  /// A list of variable levels. The outer most level will always be defined.
  /// As we enter things like if statements, this will grow.
  levels:      Vec<Level>,
  /// All the global variables in scope.
  globals:     &'a HashSet<String>,
  /// All the predefined values in scope.
  predefined:  &'a HashMap<String, VarGetter>,
  /// A set of local function names, used for calling functions in the current
  /// file.
  local_names: &'a HashSet<String>,
  /// The path of the current file, NOT the path of the current impl!
  path:        &'a Path,
  /// The global namespace
  names:       &'a HashMap<Path, Item>,
  /// A mapping of all import tips to their full path. For example, importing
  /// `foo::bar` will produce an item mapping `bar` -> `foo::bar`.
  imports:     &'a HashMap<String, Path>,

  /// Only contains anything if we are parsing a closure. When parsing a
  /// closure, this contains a list of all outside local variables, and if they
  /// have been used.
  outer_variables: HashMap<String, (VarID, bool)>,
  /// This is used when parsing a closure. All new varids will be offset by
  /// this amount, so that closure arguments don't conflict with variables.
  outer_offset:    usize,
}

/// Utility functions
impl CompileEnv<'_> {
  fn expand_name(&self, name: Ident) -> Result<PathLit> {
    if self.imports.contains_key(name.as_ref()) {
      return Ok(PathLit::new(self.imports[name.as_ref()].clone(), name.pos()));
    }
    let p = Path::new(vec![name.clone().into()]);
    // Handle the global name case
    if self.names.contains_key(&p) {
      return Ok(PathLit::new(p, name.pos()));
    }
    // Handle the local file name case
    if self.local_names.contains(name.as_ref()) {
      let mut p = self.path.clone();
      p.push(name.clone().into());
      return Ok(PathLit::new(p, name.pos()));
    }
    Err(ParseError::undefined("name", name))
  }
  fn validate_path(&self, path: &PathLit) -> Result<()> {
    if self.names.contains_key(path) {
      return Ok(());
    }
    if path.len() == 1 && self.local_names.contains(path.first().as_ref()) {
      return Ok(());
    }
    Err(ParseError::undefined("name", Ident::new(path.to_string(), path.pos())))
  }
  fn has_name(&self, name: &Ident) -> bool {
    // If these are both true, then we want to use the import.
    self.globals.contains(name.as_ref())
      || self.imports.contains_key(name.as_ref())
      || self.names.contains_key(&Path::new(vec![name.clone().into()]))
      || self.local_names.contains(name.as_ref())
  }
  /// Defines a local variable. Will return an error if already defined.
  ///
  /// If `initialized` is false, then any uses of this variable will be invalid,
  /// and it will need to be set first. If `initialized` is true, then this
  /// variable can be used immediately after.
  pub fn define_local(&mut self, name: Ident, initialized: bool) -> Result<VarID> {
    if self.curr_level().contains_key(name.as_ref()) {
      return Err(ParseError::custom("variable already defined".into(), name.pos()));
    }
    let id = self.next_var_id();
    let pos = name.pos();
    self.curr_level_mut().insert(
      name.into(),
      LocalVar {
        id,
        pos,
        initialized: vec![if initialized { Initialized::Yes } else { Initialized::No }],
      },
    );
    Ok(VarID::local(id, pos))
  }
  /// Returns a variable if it is present. If not, this returns an undefined
  /// variable error.
  ///
  /// If the variable is defined in the outer scope, this will mark it as being
  /// used, so that it can be captured once the closure is compiled.
  fn get_local(&mut self, name: &Ident) -> Result<VarID> {
    if let Some(var) = self.globals.get(name.as_ref()) {
      return Ok(VarID::global(var.clone(), name.pos()));
    }
    if let Some((var, used)) = self.outer_variables.get_mut(name.as_ref()) {
      *used = true;
      return Ok(VarID::local(var.local_value().unwrap() + self.outer_offset, var.pos()));
    }
    // Order matters! If we have the same name on two levels, we want the innermost
    // level.
    for level in self.levels.iter().rev() {
      if let Some(var) = level.vars().get(name.as_ref()) {
        return Ok(VarID::local(var.id, name.pos()));
      }
    }
    Err(ParseError::undefined("variable", name.clone()))
  }
  /// Marks the given variable as initialized. Will do nothing if the variable
  /// is undefined.
  fn mark_initialized(&mut self, name: &Ident) {
    let is_else = self.levels.last().unwrap().is_else();
    let len = self.levels.len();
    for (i, level) in self.levels.iter_mut().enumerate().rev() {
      if let Some(var) = level.vars_mut().get_mut(name.as_ref()) {
        // If we are in an else block, and this variable is not initialized at this
        // depth, then we know it cannot be initialized, as one of the above else if
        // blocks didn't initialize this, so we exit.
        //
        // We only check this for else blocks, as the variable is not going to be
        // initialized in the first if block. In that first block, we want to
        // treat initialization like normal.
        if is_else && var.initialized.last().unwrap() == &Initialized::No {
          break;
        }
        // len - i is the length of `initialized` in `var`. We want the last element, so
        // we subtract 1.
        var.mark_initialized(len - i - 1);
        break;
      }
    }
  }
  /// Used when we enter a new scope. This allows us to redeclare variables,
  /// and those variables will shadow variables in the upper scope.
  fn enter_scope(&mut self, is_if: bool) {
    self.levels.push(Level::new(is_if));
    for level in self.levels.iter_mut() {
      for var in level.vars_mut().values_mut() {
        var.initialized.push(*var.initialized.last().unwrap());
      }
    }
  }
  /// Used when we leave a scope. This will pop some variables out of scope.
  fn leave_scope(&mut self, can_init: bool) {
    self.levels.pop();
    for level in self.levels.iter_mut() {
      for var in level.vars_mut().values_mut() {
        let last = var.initialized.pop().unwrap();
        if can_init && last == Initialized::Yes {
          *var.initialized.last_mut().unwrap() = Initialized::Yes;
        }
      }
    }
  }

  fn enter_if(&mut self) {
    self.enter_block();
  }
  fn swap_else(&mut self) {
    self.levels.last_mut().unwrap().is_else = true;
    for level in self.levels.iter_mut() {
      for var in level.vars_mut().values_mut() {
        // If we are in a conditional, and we were already initialized in the level
        // above, then we don't want to set this back one.
        let idx = var.initialized.len().checked_sub(2);
        if let Some(idx) = idx {
          if var.initialized.get(idx).copied().unwrap_or(Initialized::Yes) != Initialized::Yes {
            *var.initialized.last_mut().unwrap() = match var.initialized.last().unwrap() {
              Initialized::Yes => Initialized::InPreviousElse,
              Initialized::InPreviousElse => Initialized::No,
              Initialized::No => Initialized::No,
            };
          }
        }
      }
    }
  }
  // Leave without an else clause, so none of the variables inside are
  // initialized.
  fn leave_if(&mut self) {
    self.leave_scope(false);
  }
  // Leave with an else clause
  fn leave_else(&mut self) {
    self.leave_block();
  }

  fn enter_block(&mut self) {
    self.enter_scope(false);
  }
  fn leave_block(&mut self) {
    self.leave_scope(true);
  }
  fn enter_for_while(&mut self) {
    self.enter_block();
  }
  fn leave_for_while(&mut self) {
    self.leave_scope(false);
  }
  fn enter_loop(&mut self) {
    self.enter_block();
  }
  fn leave_loop(&mut self) {
    self.leave_block();
  }
  /// Returns a variable if it is present and in scope.
  ///
  /// If it is not in scope, this will return `None`. If it has been defined,
  /// and is not initialized, then this will return `Some(Err(_))`.
  fn get_local_opt(&mut self, name: &Ident) -> Option<Result<VarID>> {
    if let Some(var) = self.globals.get(name.as_ref()) {
      return Some(Ok(VarID::global(var.clone(), name.pos())));
    }
    if let Some((var, used)) = self.outer_variables.get_mut(name.as_ref()) {
      *used = true;
      return Some(Ok(var.clone()));
    }
    // Order matters! If we have the same name on two levels, we want the innermost
    // level.
    for level in self.levels.iter().rev() {
      if let Some(var) = level.vars().get(name.as_ref()) {
        if var.initialized.last().unwrap() == &Initialized::Yes {
          return Some(Ok(VarID::local(var.id, name.pos())));
        } else {
          return Some(Err(ParseError::custom(
            format!("cannot use uninitialized variable `{}`", name.as_ref()),
            name.pos(),
          )));
        }
      }
    }
    None
  }

  /// Compiles a parse tree into a runtime tree.
  pub fn compile(&mut self, stat: StatementList) -> Result<RtStatementList> {
    self.compile_stat_list(stat)
  }

  fn curr_level(&self) -> &HashMap<String, LocalVar> {
    self.levels.last().unwrap().vars()
  }
  fn curr_level_mut(&mut self) -> &mut HashMap<String, LocalVar> {
    self.levels.last_mut().unwrap().vars_mut()
  }
  fn next_var_id(&self) -> usize {
    let mut id = 0;
    for level in &self.levels {
      id += level.vars().len();
    }
    id
  }
}

impl LocalVar {
  #[track_caller]
  fn mark_initialized(&mut self, depth: usize) {
    self.initialized[depth] = Initialized::Yes;
  }

  pub fn to_var_id(&self) -> VarID {
    VarID::local(self.id, self.pos)
  }
}
impl Level {
  pub fn new(is_else: bool) -> Self {
    Level { vars: HashMap::new(), is_else }
  }
  pub fn is_else(&self) -> bool {
    self.is_else
  }
  pub fn vars(&self) -> &HashMap<String, LocalVar> {
    &self.vars
  }
  pub fn vars_mut(&mut self) -> &mut HashMap<String, LocalVar> {
    &mut self.vars
  }
}

impl RtEnv {
  /// This is the second parsing pass. This converts all `parse::tree` types
  /// into `runtime::tree` types. This will also do all of the variable/function
  /// name checking.
  pub fn second_pass(&mut self) -> Result<()> {
    let mut parsed_files = vec![];
    mem::swap(&mut self.parsed_files, &mut parsed_files);
    for f in &parsed_files {
      self.declare_file(f);
    }
    let mut defined_funcs = HashSet::new();
    let mut all_globals = vec![];
    for file in parsed_files {
      let mut import_map = HashMap::new();
      let mut err = ParseError::Empty;
      for i in file.imports {
        for path in i.paths() {
          if self.names.contains_key(&path) {
            let last = path.last();
            if import_map.insert(last.clone().into(), path.into()).is_some() {
              err.join(ParseError::Redeclaration(last, "import"));
            }
          } else {
            let last = path.last();
            err.join(ParseError::Undefined(last, "import"));
          }
        }
      }
      err.into_result(())?;

      let mut local_names = HashSet::with_capacity(file.funcs.len() + file.structs.len());
      for f in &file.funcs {
        local_names.insert(f.name.clone().into());
      }
      for s in &file.structs {
        local_names.insert(s.name.clone().into());
      }

      let mut globals = HashSet::new();
      for global in file.globals {
        let mut env = self.create_compile_env(&file.path, &globals, &import_map, &local_names);
        let init = env.compile_expr(global.init.unwrap())?;

        globals.insert(global.name.to_string());

        all_globals.push((global.name, init));
      }

      for c in file.callbacks {
        let mut env = self.create_compile_env(&file.path, &globals, &import_map, &local_names);
        for arg in &c.args.names {
          env.define_local(arg.0.clone(), true).unwrap();
        }
        let body = env.compile(c.body)?;
        let when = match c.when {
          Some(e) => Some(env.compile_expr(e)?),
          None => None,
        };

        let num_vars = env.next_var_id();
        match self.callback_impls.get_mut(c.name.as_ref()) {
          Some(cbs) => cbs.impls.push(OnEvent { when, args: c.args, body, vars: num_vars }),
          None => return Err(ParseError::custom("unknown callback".into(), c.name.pos())),
        }
      }

      for i in file.impls {
        let path = &file.path;
        let impl_path = file.path.join(&i.struct_path);

        match i.trait_path {
          Some(trait_path) => {
            let trait_path_pos = trait_path.pos();
            let trait_path = file.path.join(&trait_path);

            match self.names.get_mut(&i.struct_path) {
              Some(Item::Struct(_)) => {}
              Some(_) => {
                return Err(ParseError::custom("not a struct".into(), i.struct_path.pos()))
              }
              _ => return Err(ParseError::custom("unknown struct".into(), i.struct_path.pos())),
            };
            let t = match self.names.get(&trait_path) {
              Some(Item::Trait(t)) => t,
              Some(_) => return Err(ParseError::custom("not a trait".into(), trait_path_pos)),
              _ => return Err(ParseError::custom("unknown trait".into(), trait_path_pos)),
            };

            let mut funcs = HashMap::new();
            for (key, func) in &t.funcs {
              if !i.funcs.iter().any(|func| func.name.as_str() == key) {
                if func.has_body {
                  funcs.insert(key.as_str().into(), func.item.clone());
                } else {
                  return Err(ParseError::custom(
                    format!("missing required function `{}`", key),
                    trait_path_pos,
                  ));
                }
              }
            }

            for f in i.funcs {
              let mut env = self.create_compile_env(path, &globals, &import_map, &local_names);
              for arg in &f.args.names {
                env.define_local(arg.0.clone(), true).unwrap();
              }
              let body = env.compile(f.body)?;

              let mut p = impl_path.clone();
              let pos = f.name.pos();
              p.push(f.name.clone().into());
              let func_path = PathLit::new(p.clone(), pos);

              let num_vars = env.next_var_id();
              funcs.insert(
                f.name.as_str().into(),
                crate::FuncItem {
                  imp: Arc::new(FuncDef { path: func_path, args: f.args, body, vars: num_vars }),
                },
              );
            }
            let s = match self.names.get_mut(&i.struct_path) {
              Some(Item::Struct(t)) => t,
              _ => unreachable!(),
            };
            s.trait_imps.push(crate::TraitImpl { path: trait_path, funcs });
          }
          None => {
            for f in i.funcs {
              let mut env = self.create_compile_env(path, &globals, &import_map, &local_names);
              for arg in &f.args.names {
                env.define_local(arg.0.clone(), true).unwrap();
              }
              let body = env.compile(f.body)?;

              let mut p = impl_path.clone();
              let pos = f.name.pos();
              p.push(f.name.into());
              let func_path = PathLit::new(p.clone(), pos);
              defined_funcs.insert(p.clone());
              let num_vars = env.next_var_id();
              self.names.insert(
                p,
                Item::Func(crate::FuncItem {
                  imp: Arc::new(FuncDef { path: func_path, args: f.args, body, vars: num_vars }),
                }),
              );
            }
          }
        }
      }

      /*
      for i in file.trait_impls {
        let path = &file.path;
        let struct_path = file.path.join(&i.struct_ty);
        let trait_path = i.trait_path;

        for f in i.funcs {
          let mut env = self.create_compile_env(path, &globals, &import_map, &local_names);
          for arg in &f.args.names {
            env.define_local(arg.0.clone(), true).unwrap();
          }
          let body = env.compile(f.body)?;

          let func_path = PathLit::new(p.clone(), pos);
          let num_vars = env.next_var_id();

          funcs.push(crate::FuncItem {
            imp: Arc::new(FuncDef { path: func_path, args: f.args, body, vars: num_vars }),
          });
        }

        match self.names.get_mut(&struct_path) {
          Some(Item::Struct(s)) => {
            s.trait_imps.push(crate::TraitImpl { path: trait_path, funcs: funcs })
          }
          _ => unreachable!(),
        };
      }
      */

      let path = file.path.clone();
      for f in file.funcs {
        let mut env = self.create_compile_env(&path, &globals, &import_map, &local_names);
        for arg in &f.args.names {
          env.define_local(arg.0.clone(), true).unwrap();
        }
        let body = env.compile(f.body)?;

        let mut p = path.clone();
        let pos = f.name.pos();
        p.push(f.name.into());
        let path = PathLit::new(p.clone(), pos);
        defined_funcs.insert(p.clone());
        let num_vars = env.next_var_id();
        self.names.insert(
          p,
          Item::Func(crate::FuncItem {
            imp: Arc::new(FuncDef { path, args: f.args, body, vars: num_vars }),
          }),
        );
      }

      let path = file.path;
      for s in file.structs {
        let mut p = path.clone();
        let pos = s.name.pos();
        p.push(s.name.into());
        let path = PathLit::new(p.clone(), pos);
        self.names.insert(
          p,
          Item::Struct(crate::StructItem {
            imp:        crate::StructCallable::Native(Struct {
              path,
              fields: s.fields,
              order: s.order,
            }),
            trait_imps: vec![],
          }),
        );
      }
    }
    self.validate_functions(defined_funcs)?;

    for (name, init) in all_globals {
      let mut lock = self.lock(&[]);
      self.globals.insert(name.as_ref().into(), Mutex::new(init.exec(&mut lock).unwrap().into()));
    }

    Ok(())
  }
  pub fn compile_stat_list(&self, s: StatementList) -> Result<RtStatementList> {
    self
      .create_compile_env(&Path::empty(), &HashSet::new(), &HashMap::new(), &HashSet::new())
      .compile(s)
  }

  fn declare_file(&mut self, file: &ParsedFile) {
    self.declare_module(&file.path);
    for f in &file.funcs {
      let mut p = file.path.clone();
      p.push(f.name.clone().into());
      self.declare_module(&p);
    }
    for s in &file.structs {
      let mut p = file.path.clone();
      p.push(s.name.clone().into());
      self.declare_module(&p);
    }
    for imp in &file.impls {
      let p = file.path.join(&imp.struct_path);
      for f in &imp.funcs {
        let mut p = p.clone();
        p.push(f.name.clone().into());
        self.declare_module(&p);
      }
    }
  }

  /// Creates a new parse environment. This is used during the second pass of
  /// parsing, to check variable and function names.
  pub fn create_compile_env<'a>(
    &'a self,
    path: &'a Path,
    globals: &'a HashSet<String>,
    imports: &'a HashMap<String, Path>,
    local_names: &'a HashSet<String>,
  ) -> CompileEnv<'a> {
    CompileEnv {
      levels: vec![Level::new(false)],
      outer_variables: HashMap::new(),
      globals,
      predefined: &self.predefined,
      local_names,
      path,
      names: &self.names,
      imports,
      outer_offset: 0,
    }
  }
}

impl<'a> CompileEnv<'a> {
  fn closure_env(&self, num_args: usize) -> CompileEnv {
    CompileEnv {
      levels:          vec![Level::new(false)],
      outer_variables: self
        .levels
        .iter()
        .map(|level| {
          level.vars().iter().map(|(name, var)| (name.clone(), (var.to_var_id(), false)))
        })
        .flatten()
        .collect(),
      globals:         self.globals,
      predefined:      self.predefined,
      local_names:     self.local_names,
      path:            self.path,
      names:           self.names,
      imports:         self.imports,
      outer_offset:    self.outer_offset + num_args,
    }
  }
}
