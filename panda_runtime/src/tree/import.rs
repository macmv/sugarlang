use panda_parse::{
  token::{Ident, ParseError, PathLit},
  Path,
};
use std::collections::HashSet;

#[derive(Debug, Clone)]
pub struct ImportTree {
  pub(crate) name:     Ident,
  pub(crate) children: Vec<ImportTree>,
}

#[derive(Debug, Clone)]
pub struct Import {
  pub(crate) tree: ImportTree,
}

impl ImportTree {
  /// Makes sure all the tips of the tree contain no duplicates.
  pub fn check_duplicates(&self) -> Result<(), ParseError> {
    let mut tips = HashSet::new();
    self.check_duplicates_inner(&mut tips)?;
    Ok(())
  }
  fn check_duplicates_inner(&self, tips: &mut HashSet<String>) -> Result<(), ParseError> {
    if self.children.is_empty() {
      if !tips.insert(self.name.clone().into()) {
        return Err(ParseError::redeclaration("import", self.name.clone()));
      }
    } else {
      for c in &self.children {
        c.check_duplicates_inner(tips)?;
      }
    }
    Ok(())
  }

  pub fn paths(&self) -> Vec<PathLit> {
    let mut paths = vec![];
    self.paths_inner(Path::empty(), &mut paths);
    paths
  }

  fn paths_inner(&self, mut prefix: Path, paths: &mut Vec<PathLit>) {
    prefix.push(self.name.clone().into());
    if self.children.is_empty() {
      paths.push(PathLit::new(prefix, self.name.pos()));
    } else {
      for c in &self.children {
        c.paths_inner(prefix.clone(), paths);
      }
    }
  }

  pub fn tips(&self) -> Vec<Ident> {
    let mut tips = vec![];
    self.tips_inner(&mut tips);
    tips
  }

  fn tips_inner(&self, tips: &mut Vec<Ident>) {
    if self.children.is_empty() {
      tips.push(self.name.clone());
    } else {
      for c in &self.children {
        c.tips_inner(tips);
      }
    }
  }
}

impl Import {
  pub fn convert_name(&self, name: &mut PathLit) {
    // This checks first and last, like so:
    //
    // use my::import::module;
    //
    // fn main() {
    //   module::my_type::my_func();
    // }
    //
    // It will then expand the given PathLit to be the absolute path.
    for path in self.tree.paths() {
      if name.first() == path.last() {
        // We have something like
        // path = "a::b::c"
        // name = "c::d::e"
        // and we want name to be "a::b::c::d::e"

        let mut path: Path = path.into();
        path.pop();
        name.set_path(path.join(name));
      }
    }
  }
  pub fn tips(&self) -> Vec<Ident> {
    self.tree.tips()
  }
  pub fn paths(&self) -> Vec<PathLit> {
    self.tree.paths()
  }
}

#[cfg(test)]
mod tests {
  use panda_parse::{syntax::Parser, token::Tokenizer};

  #[track_caller]
  fn parse_file(src: &str) {
    let res = Parser::new(Tokenizer::new(src.as_bytes(), 0)).parse_file(path!()).unwrap();
    assert!(res.path.is_empty());
    assert!(res.funcs.is_empty());
    assert!(res.structs.is_empty());
    assert!(res.impls.is_empty());
  }
  #[track_caller]
  fn parse_invalid_file(src: &str) {
    Parser::new(Tokenizer::new(src.as_bytes(), 0)).parse_file(path!()).unwrap_err();
  }

  #[test]
  fn imports() {
    parse_file("use hello::world");
    parse_file("use single");
    parse_file("use lots::of::{splitting, options}");
    parse_file("use large::{tree, here::and::more::{words, and, things, }}");
    parse_invalid_file("use large::{tree, things::more{words, and, things, }}");
    parse_invalid_file("use large::{tree, things::more::{words, and, things,, }}");
    parse_invalid_file("use large::{tree, ::{words, and, things}}");
    parse_invalid_file("use large::{tree, {words, and, things}}");
    parse_invalid_file("use large::{tree,, {words, and, things}}");
    parse_invalid_file("use ::{tree, {words, and, things}}");
    parse_invalid_file("use {tree, {words, and, things}}");

    parse_invalid_file("use name::{same, import::{here, same}}");
    parse_file("use same::{import, but::{not, tip, same}}");
  }
}
