use super::FuncDef;
use panda_parse::token::PathLit;

#[allow(dead_code)]
#[derive(Debug, Clone)]
pub struct Impl {
  // The type this impl block is for
  pub(crate) ty:    PathLit,
  // The functions inside the impl. The `slf` type for these functions
  // is the same as the above type.
  pub(crate) funcs: Vec<FuncDef>,
}
