use super::Expr;
use crate::VarType;
use panda_parse::token::{Ident, PathLit};
use std::collections::HashMap;

// A struct definition. Only appears outside of functions.
#[allow(dead_code)]
#[derive(Debug, Clone)]
pub struct Struct {
  // The full path of the struct.
  pub(crate) path:   PathLit,
  pub(crate) fields: HashMap<String, VarType>,
  pub(crate) order:  Vec<Ident>,
}

// A struct literal in a function. Used to instatiate a struct.
#[derive(Debug, Clone, PartialEq)]
pub struct StructLit {
  pub(crate) name:   PathLit,
  pub(crate) fields: Vec<(Ident, Expr)>,
}
