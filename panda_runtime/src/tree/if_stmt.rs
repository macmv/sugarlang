use super::{Expr, StatementList};
use crate::{LockedEnv, Result, RuntimeError, Var};

#[derive(Debug, Clone, PartialEq)]
pub struct If {
  pub(crate) cond:       Expr,
  pub(crate) block:      StatementList,
  pub(crate) else_if:    Vec<(Expr, StatementList)>,
  pub(crate) else_block: Option<StatementList>,
}

impl If {
  pub fn exec(&self, env: &mut LockedEnv) -> Result<Var> {
    match self.cond.exec(env)? {
      Var::Bool(v) => {
        if v {
          self.block.exec(env)
        } else {
          for (cond, block) in &self.else_if {
            match cond.exec(env)? {
              Var::Bool(v) => {
                if v {
                  return block.exec(env);
                }
              }
              _ => return Err(RuntimeError::custom("Not a bool", cond.pos())),
            }
          }
          // All the else if blocks returned false, so we check the final else block.
          if let Some(ref b) = self.else_block {
            b.exec(env)
          } else {
            Ok(Var::None)
          }
        }
      }
      _ => Err(RuntimeError::custom("Not a bool", self.cond.pos())),
    }
  }
}

#[cfg(test)]
mod tests {
  use crate::{tree::exec, Var};

  #[test]
  fn math() {
    assert_eq!(
      exec(
        "
    let i = 3
    if i < 5 {
      i = 10
    }
    i"
      )
      .unwrap(),
      Var::Int(10),
    );
    assert_eq!(
      exec(
        "
    let i = 4
    if i < 2 {
      i = 10
    } else if i < 5 {
      i = 20
    } else {
      i = 30
    }
    i"
      )
      .unwrap(),
      Var::Int(20)
    );
    assert_eq!(
      exec(
        "
    let i = 4
    if i < 2 {
      i = 10
    } else {
      i = 20
    }
    i"
      )
      .unwrap(),
      Var::Int(20)
    );
  }
}
