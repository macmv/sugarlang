use super::{Expr, StatementList};
use crate::{LockedEnv, Result, RuntimeError, Var, VarID};
use panda_parse::token::Span;

#[derive(Debug, Clone, PartialEq)]
pub enum Loop {
  For {
    first:       VarID,
    second:      Option<VarID>,
    iter:        Expr,
    block:       StatementList,
    keyword_pos: Span,
  },
  While {
    cond:        Expr,
    block:       StatementList,
    keyword_pos: Span,
  },
  Loop {
    block:       StatementList,
    keyword_pos: Span,
  },
}

impl Loop {
  pub fn exec(&self, env: &mut LockedEnv) -> Result<Var> {
    match self {
      Loop::For { first, second, iter, block, keyword_pos } => {
        let it = iter.exec(env)?;
        match it {
          Var::Map(map) => {
            if let Some(second) = second {
              for (k, v) in map.borrow().iter() {
                env.set_var(first, k.clone().into());
                env.set_var(second, v.clone());
                env.check_time_left(*keyword_pos)?;
                match block.exec(env) {
                  Err(RuntimeError::Continue(_)) => continue,
                  Err(RuntimeError::Break(_)) => break,
                  Err(e) => return Err(e),
                  Ok(_) => (),
                }
              }
            } else {
              return Err(RuntimeError::custom(
                "Maps require two keys when iterating",
                first.pos(),
              ));
            }
          }
          Var::Array(arr) => {
            if let Some(second) = second {
              return Err(RuntimeError::custom(
                "Arrays cannot have two keys when iterating",
                second.pos(),
              ));
            }
            for v in arr.borrow().iter() {
              env.set_var(first, v.clone());
              env.check_time_left(*keyword_pos)?;
              match block.exec(env) {
                Err(RuntimeError::Continue(_)) => continue,
                Err(RuntimeError::Break(_)) => break,
                Err(e) => return Err(e),
                Ok(_) => (),
              }
            }
          }
          Var::Range(r) => {
            if let Some(second) = second {
              return Err(RuntimeError::custom(
                "Ranges cannot have two keys when iterating",
                second.pos(),
              ));
            }
            let min = r.0.int(iter.pos())?;
            let max = r.1.int(iter.pos())?;
            for v in min..max {
              env.set_var(first, Var::Int(v));
              env.check_time_left(*keyword_pos)?;
              match block.exec(env) {
                Err(RuntimeError::Continue(_)) => continue,
                Err(RuntimeError::Break(_)) => break,
                Err(e) => return Err(e),
                Ok(_) => (),
              }
            }
          }
          _ => return Err(RuntimeError::custom("Cannot iterate over non-map {v:?}", iter.pos())),
        }
        Ok(Var::None)
      }
      Loop::While { cond, block, keyword_pos } => loop {
        match cond.exec(env)? {
          Var::Bool(v) => {
            if v {
              env.check_time_left(*keyword_pos)?;
              match block.exec(env) {
                Err(RuntimeError::Continue(_)) => continue,
                Err(RuntimeError::Break(_)) => break Ok(Var::None),
                Err(e) => return Err(e),
                Ok(_) => (),
              }
            } else {
              return Ok(Var::None);
            }
          }
          _ => return Err(RuntimeError::custom("Not a bool", cond.pos())),
        }
      },
      Loop::Loop { block, keyword_pos } => loop {
        env.check_time_left(*keyword_pos)?;
        match block.exec(env) {
          Err(RuntimeError::Continue(_)) => continue,
          Err(RuntimeError::Break(_)) => break Ok(Var::None),
          Err(e) => return Err(e),
          Ok(_) => (),
        }
      },
    }
  }
}

#[cfg(test)]
mod tests {
  use crate::{tree::exec, Var};

  #[test]
  fn simple_loops() {
    assert_eq!(
      exec(
        "
    let j = 0
    for i in 0..5 {
      j = j + i
    }
    j"
      )
      .unwrap(),
      Var::Int(1 + 2 + 3 + 4)
    );
    assert_eq!(
      exec(
        "
    let j = 0
    for i in 0..5 {
      j = i
      if i == 3 {
        break
      }
    }
    j"
      )
      .unwrap(),
      Var::Int(3)
    );
    assert_eq!(
      exec(
        "
    let j = 0
    for i in 0..5 {
      if i == 3 {
        continue
      }
      j++
    }
    j"
      )
      .unwrap(),
      Var::Int(4) // j++ was only called 4 times, because we skipped i = 3
    );
  }
}
