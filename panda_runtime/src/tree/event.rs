use crate::{LockedEnv, Result, Var, VarID};

use super::{Expr, StatementList};
use panda_parse::{token::Span, tree::Args};

#[derive(Debug, Clone)]
pub struct OnEvent {
  // Arguments for this function.
  pub(crate) args: Args,
  // The number of variables this function will use.
  pub(crate) when: Option<Expr>,
  // The actual body to execute when the function is called.
  pub(crate) body: StatementList,
  // The number of variables this function will use.
  pub(crate) vars: usize,
}

impl OnEvent {
  /// Executes the callback with the given args. If args is the wrong length, a
  /// RuntimeError will be thrown. This is only called by Rust, so variable
  /// length errors cannot be validated ahead of time.
  // self.check_args_rt(env, pos, slf.is_some(), &args)?;
  pub fn exec(&self, env: &mut LockedEnv, args: Vec<Var>, _pos: Span) -> Result<()> {
    let mut new_args = Vec::with_capacity(args.len() + 1);

    for v in args {
      new_args.push(v);
    }
    let mut inner = env.new_scope(self.vars);
    for (i, v) in new_args.into_iter().enumerate() {
      inner.set_var(&VarID::local(i, self.args.names[i].0.pos()), v);
    }
    self.body.exec(&mut inner)?;
    Ok(())
  }
}
