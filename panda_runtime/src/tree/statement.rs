use super::{AssignOp, Expr, If, Loop};
use crate::{LockedEnv, Result, RuntimeError, Var, VarID};
use panda_parse::token::Span;

#[derive(Debug, Clone, PartialEq)]
pub enum Statement {
  Break(Span),
  Continue(Span),
  Assign(AssignOp),
  // FuncDef(FuncDef),
  If(If),
  Loop(Loop),
  Let(VarID, Option<Expr>),
  Expr(Expr),
}

#[derive(Debug, Clone, PartialEq)]
pub struct StatementList {
  pub(crate) items: Vec<Statement>,
}

impl StatementList {
  pub fn exec(&self, env: &mut LockedEnv) -> Result<Var> {
    if let Some(last) = self.items.last() {
      for item in &self.items[..self.items.len() - 1] {
        item.exec(env)?;
      }
      Ok(last.exec(env)?)
    } else {
      Ok(Var::None)
    }
  }
  pub fn items(&self) -> &[Statement] {
    &self.items
  }
}

impl Statement {
  pub fn exec(&self, env: &mut LockedEnv) -> Result<Var> {
    match &self {
      Statement::Break(pos) => Err(RuntimeError::Break(*pos)),
      Statement::Continue(pos) => Err(RuntimeError::Continue(*pos)),
      Statement::Assign(op) => Ok(op.exec(env)?),
      Statement::If(op) => Ok(op.exec(env)?),
      Statement::Loop(op) => Ok(op.exec(env)?),
      Statement::Let(var, expr) => {
        if let Some(e) = expr {
          let val = e.exec(env)?;
          env.set_var(var, val);
        }
        Ok(Var::None)
      }
      Statement::Expr(expr) => expr.exec(env),
    }
  }
}
