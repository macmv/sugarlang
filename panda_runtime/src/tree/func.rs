use super::{Expr, StatementList};
use crate::{FuncCallable, LockedEnv, ParseError, Result, RuntimeError, Var, VarID};
use panda_parse::{
  token::{PathLit, Span},
  tree::Args,
};

#[derive(Debug, Clone, PartialEq)]
pub struct FuncCall {
  // The name of the function.
  pub(crate) name: PathLit,
  // If some, then this function was called like `"some text".split(" ")`
  // slf will be the string "some text".
  pub(crate) slf:  Option<Box<Expr>>,
  // Args to the function.
  pub(crate) args: Vec<Expr>,
}

#[derive(Debug, Clone)]
pub struct FuncDef {
  // The full path of the function.
  pub(crate) path: PathLit,
  // Arguments for this function.
  pub(crate) args: Args,
  // The actual body to execute when the function is called.
  pub(crate) body: StatementList,
  // The number of variables this function will use.
  pub(crate) vars: usize,
}

impl FuncCallable for FuncDef {
  fn as_func_def(&self) -> Option<&Self> {
    Some(self)
  }
  fn name(&self) -> String {
    self.path.last().into()
  }
  fn check_args(&self, pos: Span, args: &[Expr]) -> std::result::Result<(), ParseError> {
    check_args(&self.args, pos, args)
  }
  fn check_args_rt(
    &self,
    _: &mut LockedEnv,
    _: Span,
    called_on_self: bool,
    args: &[Var],
  ) -> Result<()> {
    self
      .args
      .check_args_rt(called_on_self, args.len())
      .map_err(|(msg, span)| RuntimeError::custom(msg, span))
  }
  /// Executes the function, with slf and args. If args is the wrong length, or
  /// slf is the wrong type, a RuntimeError will be thrown. Most of these
  /// should be caught at parse time, but Rust can call this function as well,
  /// and that cannot be checked.
  fn exec(&self, env: &mut LockedEnv, slf: Option<Var>, args: Vec<Var>, pos: Span) -> Result<Var> {
    self.check_args_rt(env, pos, slf.is_some(), &args)?;

    let mut new_args = Vec::with_capacity(args.len() + 1);

    let takes_self = self.args.takes_self();
    if takes_self {
      // We handle `slf` being None below
      if let Some(slf) = slf {
        let mut slf_ty = self.path.clone();
        slf_ty.pop();
        if slf.ty().to_path() == slf_ty.clone().into() {
          new_args.push(slf);
        } else {
          // Only reachable through a Rust call, but it should still be a Panda error.
          return Err(RuntimeError::custom(
            &format!("expected self to be {slf_ty:?}, got {slf}"),
            pos,
          ));
        }
      }
    }

    for v in args {
      new_args.push(v);
    }
    let mut inner = env.new_scope(self.vars);
    for (i, v) in new_args.into_iter().enumerate() {
      inner.set_var(&VarID::local(i, self.args.names[i].0.pos()), v);
    }
    self.body.exec(&mut inner)
  }
}

impl FuncCall {
  pub fn exec(&self, env: &mut LockedEnv) -> Result<Var> {
    let mut args = vec![];
    for v in &self.args {
      args.push(v.exec(env)?);
    }
    match &self.slf {
      Some(expr) => {
        let slf = expr.exec(env)?;
        env.call(&slf.ty().to_path().join(self.name.path()), self.name.pos(), Some(slf), args)
      }
      None => env.call(self.name.path(), self.name.pos(), None, args),
    }
  }
}

/// Validates arguments. Should only be used when we pass the arguments to a
/// direction call. This function on a `foo.bar()` call is invalid, as we
/// don't know the type of `foo` at compile time. If you instead use the
/// `Foo::bar(foo)` syntax, this function should be called.
fn check_args(
  args: &Args,
  pos: Span,
  list: &[super::Expr],
) -> std::result::Result<(), panda_parse::token::ParseError> {
  if args.variadic.is_some() {
    // If we have variadics, then we need at least names list
    if list.len() < args.names.len() {
      Err(ParseError::custom(
        format!(
          "expected at least {} argument{}, got {} argument{}",
          args.names.len(),
          if args.names.len() == 1 { "" } else { "s" },
          list.len(),
          if list.len() == 1 { "" } else { "s" },
        ),
        pos,
      ))
    } else {
      Ok(())
    }
  } else {
    // If we don't have variadics, then we need exactly names list
    if list.len() != args.names.len() {
      Err(ParseError::custom(
        format!(
          "expected exactly {} argument{}, got {} argument{}",
          args.names.len(),
          if args.names.len() == 1 { "" } else { "s" },
          list.len(),
          if list.len() == 1 { "" } else { "s" },
        ),
        pos,
      ))
    } else {
      Ok(())
    }
  }
}
