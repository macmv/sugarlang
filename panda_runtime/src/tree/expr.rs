use super::{Closure, StatementList, StructLit};
use crate::{LockedEnv, MapKeyVar, Result, RuntimeError, Struct, StructImpl, Var, VarID, VarSend};
use panda_parse::{
  token::{FloatLit, Ident, IntLit, PathLit, Predefined, Span, StringLit},
  tree::{BoolLit, ClosureArgs},
};
use std::{cell::RefCell, collections::HashMap, rc::Rc};

#[derive(Debug, Clone, PartialEq)]
pub enum Lit {
  Int(IntLit),
  Float(FloatLit),
  String(StringLit),
  Bool(BoolLit),
  Arr(ArrLit),
  Map(MapLit),
  Struct(StructLit),
  Var(VarSend),
  Closure(ClosureLit),
}

#[derive(Debug, Clone, PartialEq)]
pub struct ArrLit {
  pub pos: Span,
  pub val: Vec<Expr>,
}
#[derive(Debug, Clone, PartialEq)]
pub struct MapLit {
  pub pos: Span,
  pub val: Vec<(Expr, Expr)>,
}
#[derive(Debug, Clone, PartialEq)]
pub struct CallArgs {
  pub pos:  Span,
  pub args: Vec<Expr>,
}
#[derive(Debug, Clone, PartialEq)]
pub struct ClosureLit {
  pub args:       ClosureArgs,
  pub body:       Box<Expr>,
  pub to_capture: Vec<VarID>,
}

#[derive(Debug, Clone, PartialEq)]
pub enum Expr {
  Lit(Lit),
  Paren(Box<Expr>),

  Op(Box<OpKind>),

  Call(Option<Box<Expr>>, PathLit, CallArgs),
  Field(Box<Expr>, Ident),
  Block(Span, StatementList),

  IdRef(VarID),
  FuncRef(PathLit),

  Predefined(Predefined),
}

// Ignores the first argument, and returns the second. This is so that we can
// loop an expression based on a value that we don't actually want to
// substiture.
macro_rules! ignore {
  ( $ign:ty, $ret:expr ) => {
    $ret
  };
}
// Same thing, but ident
macro_rules! ignore_i {
  ( $ign:ty, $ret:ident ) => {
    $ret
  };
  ( $ign:ty, _ ) => {
    _
  };
}
// Returns the last argument. This is how we take an optional parameter, and
// return an expression if the optional is not present.
macro_rules! last {
  ( $first:expr ) => {
    $first
  };
  ( $first:expr, $second:expr ) => {
    $second
  };
}

macro_rules! op_def {
  ( $($name:ident($lhs:ty $(, $rhs:ty)?),)* ) => {
    #[derive(Debug, Clone, PartialEq)]
    pub enum OpKind {
      $(
        $name($lhs $(, $rhs)?),
      )*
    }
    impl OpKind {
      pub fn pos(&self) -> Span {
        match self {
          $(
            OpKind::$name(
              ignore_i!($lhs, l)
              $(, ignore_i!($rhs, r))?
            ) => ignore!($lhs, l).pos() $(.join(ignore!($rhs, r).pos()))?,
          )*
        }
      }
      pub fn lhs(&self) -> &Expr {
        match self {
          $(
            OpKind::$name(ignore_i!($lhs, l), ..) => l,
          )*
        }
      }
      pub fn rhs(&self) -> Option<&Expr> {
        match self {
          $(
            OpKind::$name(
              ignore_i!($lhs, _)
              $(, ignore_i!($rhs, r))?
            ) => last!(None $(, ignore!($rhs, Some(r)))?),
          )*
        }
      }
      pub fn lhs_mut(&mut self) -> &mut Expr {
        match self {
          $(
            OpKind::$name(ignore_i!($lhs, l), ..) => l,
          )*
        }
      }
      pub fn rhs_mut(&mut self) -> Option<&mut Expr> {
        match self {
          $(
            OpKind::$name(
              ignore_i!($lhs, _)
              $(, ignore_i!($rhs, r))?
            ) => last!(None $(, ignore!($rhs, Some(r)))?),
          )*
        }
      }
    }
  };
}

op_def! {
  // +
  Add(Expr, Expr),
  // -
  Sub(Expr, Expr),
  // *
  Mul(Expr, Expr),
  // /
  Div(Expr, Expr),

  // %
  Mod(Expr, Expr),
  // **
  Exp(Expr, Expr),

  // &
  BitAnd(Expr, Expr),
  // |
  BitOr(Expr, Expr),
  // ^
  BitXor(Expr, Expr),

  // <<
  Shl(Expr, Expr),
  // >>
  Shr(Expr, Expr),
  // >>>
  Shrz(Expr, Expr),

  // ||
  Or(Expr, Expr),
  // &&
  And(Expr, Expr),
  // !
  Not(Expr),

  // ==
  Eq(Expr, Expr),
  // !=
  Neq(Expr, Expr),
  // <
  Less(Expr, Expr),
  // >
  Greater(Expr, Expr),
  // <=
  LTE(Expr, Expr),
  // >=
  GTE(Expr, Expr),

  // lhs[rhs]
  Arr(Expr, Expr),
  // lhs..rhs
  Range(Expr, Expr),
}

impl Lit {
  pub fn pos(&self) -> Span {
    match self {
      Lit::Int(v) => v.pos,
      Lit::Float(v) => v.pos,
      Lit::String(v) => v.pos,
      Lit::Struct(v) => v.name.pos(),
      _ => todo!("literal {:?}", self),
    }
  }
  pub fn exec(&self, env: &mut LockedEnv) -> Result<Var> {
    Ok(match self {
      Lit::Bool(v) => Var::Bool(v.val),
      Lit::Int(v) => Var::Int(v.val),
      Lit::Float(v) => Var::Float(v.val),
      Lit::String(v) => Var::String(Rc::new(v.val.clone())),
      Lit::Var(v) => v.clone().into(),
      Lit::Closure(c) => {
        let mut captured = HashMap::new();
        for id in &c.to_capture {
          captured.insert(id.clone(), env.lookup(id).into_var().into());
        }
        Var::Closure(Rc::new(Closure { args: c.args.clone(), body: (*c.body).clone(), captured }))
      }
      Lit::Arr(v) => {
        Var::Array(Rc::new(RefCell::new(v.val.iter().map(|v| v.exec(env)).collect::<Result<_>>()?)))
      }
      Lit::Map(m) => Var::Map(Rc::new(RefCell::new(
        m.val
          .iter()
          .map(|(k, v)| {
            let key: Var = k.exec(env)?;
            Ok(match key.as_map_key() {
              Some(key) => Ok((key, v.exec(env)?)),
              None => Err((key, k.pos())),
            })
          })
          .collect::<Result<std::result::Result<HashMap<MapKeyVar, Var>, (Var, Span)>>>()?
          .map_err(|(key, pos)| {
            RuntimeError::custom(
              &format!("Invalid map key (the type {} cannot be used as a map key)", key.ty()),
              pos,
            )
          })?,
      ))),
      Lit::Struct(v) => Var::Struct(Struct {
        path: v.name.clone().into(),
        imp:  StructImpl::Native(Rc::new(RefCell::new(
          v.fields
            .iter()
            .map(|(name, v)| Ok((name.as_ref().into(), v.exec(env)?)))
            .collect::<Result<HashMap<String, Var>>>()?,
        ))),
      }),
    })
  }
}

impl Expr {
  pub fn pos(&self) -> Span {
    match self {
      Self::Lit(l) => l.pos(),
      Self::Paren(expr) => expr.pos(),
      Self::Op(kind) => kind.pos(),
      Self::Call(lhs, path, args) => {
        if let Some(l) = lhs {
          l.pos().join(args.pos)
        } else {
          path.pos().join(args.pos)
        }
      }
      Self::Block(pos, _) => *pos,
      Self::Field(val, _) => val.pos(),
      Self::IdRef(id) => id.pos(),
      Self::FuncRef(path) => path.pos(),
      Self::Predefined(p) => p.pos,
    }
  }
  pub fn exec(&self, env: &mut LockedEnv) -> Result<Var> {
    Ok(match self {
      Expr::Lit(v) => v.exec(env)?,
      Expr::Paren(expr) => expr.exec(env)?,
      Expr::Op(op) => op.exec(env)?,

      Expr::Call(value, name, args_expr) => {
        let mut args = vec![];
        for v in &args_expr.args {
          args.push(v.exec(env)?);
        }
        if let Some(slf) = &value {
          let slf = slf.exec(env)?;
          let path = slf.ty().to_path().join(name);
          if env.get_item(&path).is_none() {
            return Err(RuntimeError::Undefined {
              pos:   name.pos(),
              name:  "function",
              value: path.to_string(),
              ty:    None,
            });
          } else {
            env.call(&path, name.pos(), Some(slf), args)?
          }
        } else {
          env.call(name, name.pos(), None, args)?
        }
      }
      Self::Block(_, block) => block.exec(env)?,
      Expr::Field(expr, name) => expr.exec(env)?.field(name)?,

      Expr::IdRef(id) => env.lookup(id).into_var(),
      Expr::FuncRef(path) => Var::Callback(path.path().clone()),

      Expr::Predefined(p) => env.predefined[&p.val].get(),
    })
  }
}

macro_rules! math_op {
  ( $lhs:expr, $rhs:expr, $env:expr, $expr:tt ) => {{
    let l = $lhs.exec($env)?;
    let r = $rhs.exec($env)?;
    match l {
      Var::Int(l) => match r {
        Var::Int(r) => Ok(Var::Int(l $expr r)),
        Var::Float(_) => Err(RuntimeError::custom("cannot add a float and int", $rhs.pos())),
        v => Err(RuntimeError::custom(&format!("expected an int, found `{}`", v), $rhs.pos())),
      },
      Var::Float(l) => match r {
        Var::Float(r) => Ok(Var::Float(l $expr r)),
        Var::Int(_) => Err(RuntimeError::custom("cannot add a float and int", $rhs.pos())),
        v => Err(RuntimeError::custom(&format!("expected a float, found `{}`", v), $rhs.pos())),
      },
      v => Err(RuntimeError::custom(&format!("expected a number, found `{}`", v), $lhs.pos())),
    }
  }};
}

macro_rules! bin_op {
  ( $lhs:expr, $rhs:expr, $env:expr, $expr:tt ) => {{
    let l = $lhs.exec($env)?;
    let r = $rhs.exec($env)?;
    match l {
      Var::Bool(l) => match r {
        Var::Bool(r) => Ok(Var::Bool(l $expr r)),
        _ => Err(RuntimeError::custom("not a bool", $rhs.pos())),
      },
      _ => Err(RuntimeError::custom("not a bool", $lhs.pos())),
    }
  }};
}

impl OpKind {
  pub fn exec(&self, env: &mut LockedEnv) -> Result<Var> {
    match &self {
      OpKind::Add(lhs, rhs) => {
        let l = lhs.exec(env)?;
        let r = rhs.exec(env)?;
        match l {
          Var::Int(l) => match r {
            Var::Int(r) => Ok(Var::Int(l + r)),
            Var::Float(_) => Err(RuntimeError::custom("cannot add a float and int", rhs.pos())),
            v => Err(RuntimeError::custom(&format!("expected an int, found `{}`", v), rhs.pos())),
          },
          Var::Float(l) => match r {
            Var::Float(r) => Ok(Var::Float(l + r)),
            Var::Int(_) => Err(RuntimeError::custom("cannot add a float and int", rhs.pos())),
            v => Err(RuntimeError::custom(&format!("expected a float, found `{}`", v), rhs.pos())),
          },
          Var::String(l) => match r {
            Var::String(r) => Ok(Var::String(Rc::new(l.to_string() + r.as_ref()))),
            v => Err(RuntimeError::custom(&format!("expected a string, found `{}`", v), rhs.pos())),
          },
          v => Err(RuntimeError::custom(
            &format!("expected a number or string, found `{}`", v),
            lhs.pos(),
          )),
        }
      }
      OpKind::Sub(lhs, rhs) => math_op!(lhs, rhs, env, -),
      OpKind::Mul(lhs, rhs) => math_op!(lhs, rhs, env, *),
      OpKind::Div(lhs, rhs) => math_op!(lhs, rhs, env, /),
      OpKind::Mod(lhs, rhs) => math_op!(lhs, rhs, env, %),
      OpKind::Or(lhs, rhs) => bin_op!(lhs, rhs, env, ||),
      OpKind::And(lhs, rhs) => bin_op!(lhs, rhs, env, &&),
      OpKind::Not(val) => {
        let v = val.exec(env)?;
        match v {
          Var::Bool(v) => Ok(Var::Bool(!v)),
          _ => Err(RuntimeError::custom("not a bool", val.pos())),
        }
      }
      OpKind::Exp(lhs, rhs) => {
        let l = lhs.exec(env)?;
        let r = rhs.exec(env)?;
        match l {
          Var::Int(l) => match r {
            Var::Int(r) => {
              if r < 0 {
                Err(RuntimeError::custom("cannot raise to negative power", rhs.pos()))
              } else {
                Ok(Var::Int(l.pow(r as u32)))
              }
            }
            Var::Float(_) => Err(RuntimeError::custom("cannot raise int to float", rhs.pos())),
            v => Err(RuntimeError::custom(&format!("expected an int, found `{}`", v), rhs.pos())),
          },
          Var::Float(l) => match r {
            Var::Float(r) => Ok(Var::Float(l.powf(r))),
            Var::Int(r) => Ok(Var::Float(l.powi(r as i32))),
            v => Err(RuntimeError::custom(&format!("expected a float, found `{}`", v), rhs.pos())),
          },
          v => Err(RuntimeError::custom(&format!("expected a number, found `{}`", v), lhs.pos())),
        }
      }
      OpKind::Eq(lhs, rhs) => {
        let l = lhs.exec(env)?;
        let r = rhs.exec(env)?;
        Ok(Var::Bool(l == r))
      }
      OpKind::Neq(lhs, rhs) => {
        let l = lhs.exec(env)?;
        let r = rhs.exec(env)?;
        Ok(Var::Bool(l != r))
      }
      OpKind::Less(lhs, rhs) => {
        let l = lhs.exec(env)?;
        let r = rhs.exec(env)?;
        Ok(Var::Bool(l.less(&r, self.pos())?))
      }
      OpKind::Greater(lhs, rhs) => {
        let l = lhs.exec(env)?;
        let r = rhs.exec(env)?;
        Ok(Var::Bool(l.greater(&r, self.pos())?))
      }
      OpKind::LTE(lhs, rhs) => {
        let l = lhs.exec(env)?;
        let r = rhs.exec(env)?;
        Ok(Var::Bool(l.lte(&r, self.pos())?))
      }
      OpKind::GTE(lhs, rhs) => {
        let l = lhs.exec(env)?;
        let r = rhs.exec(env)?;
        Ok(Var::Bool(l.gte(&r, self.pos())?))
      }
      OpKind::Arr(lhs, rhs) => {
        let l = lhs.exec(env)?;
        let r = rhs.exec(env)?;
        match l {
          Var::Array(v) => v
            .borrow()
            .get(r.int(rhs.pos())? as usize)
            .cloned()
            .ok_or_else(|| RuntimeError::custom("array does not contain index", rhs.pos())),
          Var::Map(v) => {
            v.borrow()
              .get(&r.as_map_key().ok_or_else(|| {
                RuntimeError::custom(format!("invalid map key `{}`", r), rhs.pos())
              })?)
              .cloned()
              .ok_or_else(|| RuntimeError::custom("map does not contain key", rhs.pos()))
          }
          _ => {
            let ty_path = l.ty().to_path();
            let path = ty_path.join(&path!(index));
            if env.get_item(&path).is_none() {
              Err(RuntimeError::custom(
                format!("cannot index into `{}` (undefined function `{}`)", ty_path, path),
                lhs.pos(),
              ))
            } else {
              env.call(&path, lhs.pos(), Some(l), vec![r])
            }
          }
        }
      }
      OpKind::Range(lhs, rhs) => {
        let l = lhs.exec(env)?;
        let r = rhs.exec(env)?;
        Ok(Var::Range(Rc::new((l, r))))
      }
      _ => todo!("op {:?}", self),
      // MathKind::Exp => math_op!(self, env, Exp, pow),
    }
  }
}

/*
impl BinOp {
  pub fn exec(&self, env: &mut LockedEnv) -> Result<Var> {
    match &self.kind {
      BinKind::Not => match self.lhs.exec(env)?.as_var(env) {
        Var::Bool(v) => return Ok(Var::Bool(!v)),
        _ => return Err(RuntimeError::custom("Not a bool", self.lhs.pos())),
      },
      _ => {}
    }
    let l = self.lhs.exec(env)?;
    let r = self.rhs.as_ref().unwrap().exec(env)?;
    match &self.kind {
      BinKind::Or => bin_op!(self, env, l, r, ||),
      BinKind::And => bin_op!(self, env, l, r, &&),
      BinKind::Eq => Ok(Var::Bool(l.as_var(env) == r.as_var(env))),
      BinKind::Neq => Ok(Var::Bool(l.as_var(env) != r.as_var(env))),
      BinKind::Less => Ok(Var::Bool(l.as_var(env).less(&r.as_var(env), self.pos())?)),
      BinKind::Greater => Ok(Var::Bool(l.as_var(env).greater(&r.as_var(env), self.pos())?)),
      BinKind::LTE => Ok(Var::Bool(l.as_var(env).lte(&r.as_var(env), self.pos())?)),
      BinKind::GTE => Ok(Var::Bool(l.as_var(env).gte(&r.as_var(env), self.pos())?)),
      _ => unreachable!(),
    }
  }
  pub fn pos(&self) -> Span {
    self.pos
  }
}
*/

#[cfg(test)]
mod tests {
  use super::*;
  use crate::{tree::exec, Var};

  #[test]
  fn rt_math() {
    assert_eq!(exec("4 + 5").unwrap(), Var::Int(9));
    assert_eq!(exec("4 * 5").unwrap(), Var::Int(20));
    assert_eq!(exec("10 / 2").unwrap(), Var::Int(5));
    assert_eq!(exec("5 - 2").unwrap(), Var::Int(3));
    assert_eq!(exec("5 % 2").unwrap(), Var::Int(1));

    assert_eq!(exec("\"hello\" + \" world\"").unwrap(), Var::String(Rc::new("hello world".into())));

    assert_eq!(exec("5 - - 2").unwrap(), Var::Int(7));
    assert_eq!(exec("let v = 5 v-- v").unwrap(), Var::Int(4));
    assert_eq!(exec("let v = 5 v++ v").unwrap(), Var::Int(6));
    assert_eq!(exec("let v = 5 v--").unwrap(), Var::None);
    assert_eq!(exec("let v = 5 v++").unwrap(), Var::None);

    assert!(exec("let v = 10 v /= 0").is_err());
    assert!(exec("let v = 10 v **= -1").is_err());

    assert_eq!(exec("let v = 5 v += 2 v").unwrap(), Var::Int(7));
    assert_eq!(exec("let v = 5 v -= 2 v").unwrap(), Var::Int(3));
    assert_eq!(exec("let v = 5 v /= 2 v").unwrap(), Var::Int(2));
    assert_eq!(exec("let v = 5 v *= 2 v").unwrap(), Var::Int(10));
    assert_eq!(exec("let v = 5 v **= 2 v").unwrap(), Var::Int(25));

    assert_eq!(exec("let v = 5 v += 2").unwrap(), Var::None);
    assert_eq!(exec("let v = 5 v -= 2").unwrap(), Var::None);
    assert_eq!(exec("let v = 5 v /= 2").unwrap(), Var::None);
    assert_eq!(exec("let v = 5 v *= 2").unwrap(), Var::None);
    assert_eq!(exec("let v = 5 v **= 2").unwrap(), Var::None);

    assert_eq!(exec("5 + 2 * 3").unwrap(), Var::Int(5 + 2 * 3));
    assert_eq!(exec("5 * 2 + 3").unwrap(), Var::Int(5 * 2 + 3));

    assert_eq!(exec("5 == 6").unwrap(), Var::Bool(false));
    assert_eq!(exec("5 != 6").unwrap(), Var::Bool(true));

    assert_eq!(exec("5 < 6").unwrap(), Var::Bool(true));
    assert_eq!(exec("6 > 5").unwrap(), Var::Bool(true));
    assert_eq!(exec("5 < 5").unwrap(), Var::Bool(false));
    assert_eq!(exec("5 > 5").unwrap(), Var::Bool(false));
    assert_eq!(exec("5 <= 5").unwrap(), Var::Bool(true));
    assert_eq!(exec("5 >= 5").unwrap(), Var::Bool(true));

    assert_eq!(exec("let i = 2 let j = 3 let res = i + j res").unwrap(), Var::Int(5));

    assert_eq!(exec("5.2 > 5.0").unwrap(), Var::Bool(true));
    assert_eq!(exec("4.9 > 5.0").unwrap(), Var::Bool(false));

    // Floating point math is weird, so using literals here doesn't always work.
    assert_eq!(exec("3.2 + 4.5").unwrap(), Var::Float(3.2 + 4.5));
    assert_eq!(exec("4.2 - 2.5").unwrap(), Var::Float(4.2 - 2.5));
    assert_eq!(exec("4.2 * 2.5").unwrap(), Var::Float(4.2 * 2.5));
    assert_eq!(exec("4.2 - -2.5").unwrap(), Var::Float(4.2 - -2.5));
    assert!(exec("let v = 4.2 v--").is_err());
    assert!(exec("let v = 4.2 v++").is_err());

    assert_eq!(exec("4 * 3 + 2").unwrap(), Var::Int(14));
    assert_eq!(exec("(4 * 3) + 2").unwrap(), Var::Int(14));
    assert_eq!(exec("4 * (3 + 2)").unwrap(), Var::Int(20));

    assert_eq!(exec("2 ** 3").unwrap(), Var::Int(8));
    assert!(exec("2 ** 3.0").is_err());
    assert_eq!(exec("2.0 ** 3").unwrap(), Var::Float(8.0));
    assert_eq!(exec("2.0 ** 3.0").unwrap(), Var::Float(8.0));
  }

  #[test]
  fn arrays() {
    assert_eq!(
      exec("[5, 6, \"hello\"]").unwrap(),
      Var::Array(Rc::new(RefCell::new(vec![
        Var::Int(5),
        Var::Int(6),
        Var::String(Rc::new("hello".into()))
      ])))
    );
    assert_eq!(exec("[5]").unwrap(), Var::Array(Rc::new(RefCell::new(vec![Var::Int(5)]))));
    assert_eq!(exec("[]").unwrap(), Var::Array(Rc::new(RefCell::new(vec![]))));
  }
}
