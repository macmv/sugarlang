use super::Expr;
use crate::{Ident, LockedEnv, Result, RuntimeError, Span, Var, VarID};
use std::rc::Rc;

#[derive(Debug, Clone, PartialEq)]
pub struct AssignOp {
  pub(crate) lhs: LHS,
  pub(crate) op:  AssignOpKind,
}

#[derive(Debug, Clone, PartialEq)]
pub enum LHS {
  Var(VarID),
  Value(Expr, LHSKind),
}

#[derive(Debug, Clone, PartialEq)]
pub enum LHSKind {
  Name(Ident),
  Index(Expr),
}

impl LHS {
  pub fn pos(&self) -> Span {
    match self {
      LHS::Var(id) => id.pos(),
      LHS::Value(expr, kind) => match kind {
        LHSKind::Name(ident) => expr.pos().join(ident.pos()),
        LHSKind::Index(idx) => expr.pos().join(idx.pos()),
      },
    }
  }
}

#[derive(Debug, Clone, PartialEq)]
pub enum AssignOpKind {
  Set(Expr),
  Inc,
  Dec,
  Add(Expr),
  Sub(Expr),
  Mul(Expr),
  Div(Expr),
  Mod(Expr),
  Exp(Expr),
}

/// Stores a partially executed LHS of an assignment. This is so that for things
/// like +=, we can execute things in the right order:
///
/// ```ignore
/// foo().a += bar();
/// ```
///
/// Here, `bar` needs to be executed first. Then `foo` is executed. Once this
/// happens we need to load the field `a` out of the struct returned from `foo`.
/// Then, we add to that field. Finally, we need to set the new value for that
/// field in `a`. This last part is tricky, as we can't execute `foo` twice. So,
/// we store the result of executing it the first time in this struct.
enum ExecLHS {
  Var(VarID),
  Value(Var, LHSKind),
}

impl LHS {
  fn exec(&self, env: &mut LockedEnv) -> Result<ExecLHS> {
    Ok(match self {
      LHS::Var(id) => ExecLHS::Var(id.clone()),
      LHS::Value(expr, kind) => ExecLHS::Value(expr.exec(env)?, kind.clone()),
    })
  }
}
impl ExecLHS {
  pub fn get(&self, env: &mut LockedEnv) -> Result<Var> {
    match self {
      Self::Var(id) => Ok(env.lookup(id).clone()),
      Self::Value(initial, kind) => match kind {
        LHSKind::Name(name) => initial.field(name),
        LHSKind::Index(index) => {
          let idx = index.exec(env)?.int(index.pos())?;
          initial.get_index(idx, index.pos())
        }
      },
    }
  }
  pub fn set(&mut self, env: &mut LockedEnv, rhs: Var) -> Result<()> {
    match self {
      Self::Var(id) => {
        env.set_var(id, rhs);
      }
      Self::Value(initial, kind) => match kind {
        LHSKind::Name(name) => {
          initial.set_field(name, rhs)?;
        }
        LHSKind::Index(index) => {
          let idx = index.exec(env)?.int(index.pos())?;
          initial.set_index(idx, index.pos(), rhs)?;
        }
      },
    }
    Ok(())
  }
}

impl AssignOp {
  pub fn exec(&self, env: &mut LockedEnv) -> Result<Var> {
    let mut lhs = self.lhs.exec(env)?;

    macro_rules! math_op {
      ($rhs:expr, $op:tt) => {{
        let rhs = $rhs.exec(env)?;
        match lhs.get(env)? {
          Var::Int(c) => {
            let var = rhs.int($rhs.pos())?;
            lhs.set(env, Var::Int(c $op var))?;
          }
          Var::Float(c) => {
            let var = rhs.float($rhs.pos())?;
            lhs.set(env, Var::Float(c $op var))?;
          }
          v => {
            return Err(RuntimeError::custom(
              &format!("expected a number, found `{}`", v),
              self.lhs.pos(),
            ))
          }
        }
      }};
    }

    match self.op {
      AssignOpKind::Set(ref val) => {
        let rhs = val.exec(env)?;
        lhs.set(env, rhs)?;
      }
      AssignOpKind::Inc => {
        let mut v = lhs.get(env)?.int(self.lhs.pos())?;
        v += 1;
        lhs.set(env, Var::Int(v))?;
      }
      AssignOpKind::Dec => {
        let mut v = lhs.get(env)?.int(self.lhs.pos())?;
        v -= 1;
        lhs.set(env, Var::Int(v))?;
      }
      AssignOpKind::Add(ref val) => {
        let rhs = val.exec(env)?;
        match lhs.get(env)? {
          Var::Int(c) => {
            let var = rhs.int(val.pos())?;
            lhs.set(env, Var::Int(c + var))?;
          }
          Var::Float(c) => {
            let var = rhs.float(val.pos())?;
            lhs.set(env, Var::Float(c + var))?;
          }
          Var::String(s) => {
            lhs.set(env, Var::String(Rc::new(s.as_ref().clone() + rhs.string(val.pos())?)))?;
          }
          v => {
            return Err(RuntimeError::custom(
              &format!("expected a number or string, found `{}`", v),
              self.lhs.pos(),
            ))
          }
        }
      }
      AssignOpKind::Sub(ref val) => math_op!(val, -),
      AssignOpKind::Mul(ref val) => math_op!(val, *),
      AssignOpKind::Div(ref val) => {
        let rhs = val.exec(env)?;
        match lhs.get(env)? {
          Var::Int(c) => {
            let var = rhs.int(val.pos())?;
            if var == 0 {
              return Err(RuntimeError::custom("cannot divide int by zero", val.pos()));
            }
            lhs.set(env, Var::Int(c / var))?;
          }
          Var::Float(c) => {
            let var = rhs.float(val.pos())?;
            lhs.set(env, Var::Float(c / var))?;
          }
          v => {
            return Err(RuntimeError::custom(
              &format!("expected a number, found `{}`", v),
              self.lhs.pos(),
            ))
          }
        }
      }
      AssignOpKind::Mod(ref val) => math_op!(val, %),
      AssignOpKind::Exp(ref val) => {
        let rhs = val.exec(env)?;
        match lhs.get(env)? {
          Var::Int(c) => {
            let var = rhs.int(val.pos())?;
            if var < 0 {
              return Err(RuntimeError::custom(
                &format!("cannot raise int to a power less than 0 ({} < 0)", var),
                val.pos(),
              ));
            }
            lhs.set(env, Var::Int(c.pow(var as u32)))?;
          }
          Var::Float(c) => {
            let var = rhs.float(val.pos())?;
            lhs.set(env, Var::Float(c.powf(var)))?;
          }
          v => {
            return Err(RuntimeError::custom(
              &format!("expected a number, found `{}`", v),
              self.lhs.pos(),
            ))
          }
        }
      }
    }
    Ok(Var::None)
  }
}
