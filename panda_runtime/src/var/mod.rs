use super::{BuiltinImpl, Ident, Result, RuntimeError};
use crate::tree::Closure;
use panda_parse::{token::Span, Path, VarType};
use std::{
  cell::{Ref, RefCell, RefMut},
  cmp::Ordering,
  collections::HashMap,
  fmt,
  hash::{Hash, Hasher},
  rc::Rc,
  sync::Arc,
};

pub mod convert;

/// A local variable id. This is used when settings/getting local variables at
/// runtime. The span is the position where the variable is defined. The span
/// is not used in lookups (only the id).
///
/// The span is stores so that when setting a variable, we can get a better
/// stacktrace to where it was defined.
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct VarID {
  pos:  Span,
  kind: VarKind,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
enum VarKind {
  Local(usize),
  Global(String),
}

/// A Panda variable. This is a dynamically typed value, that can change at
/// any point. The `None` variant can be used if a struct field is declared as
/// `optional`, but otherwise is an invalid variable. It is mostly used as a
/// placeholder for functions like `println`, which don't have anything sensible
/// to return.
///
/// The main reason this might be confusing is how this behaves for cloning
/// values. Any struct, map, array, or builtin is reference counted. This makes
/// sense in most situations. This means that it will always be passed around by
/// reference, and any inner function that modifies the type will not be working
/// with a duplicate.
///
/// The same is not true for the base types (string, int, float, and bool).
/// These are stored by value, so any time they are passed to a function, they
/// are copied. This means that we cannot modify a string after it is passed
/// into a function. This is mostly so that map keys are not able to be modified
/// after being used.
#[derive(Debug)]
pub enum Var {
  String(Rc<String>),
  Int(i64),
  Float(f64),
  Bool(bool),
  None,
  // The name of the struct and the fields
  Struct(Struct),
  Map(Rc<RefCell<HashMap<MapKeyVar, Var>>>),
  Array(Rc<RefCell<Vec<Var>>>),
  Range(Rc<(Var, Var)>),

  // A function path.
  Callback(Path),
  Closure(Rc<Closure>),
}

#[derive(Clone, Debug)]
pub struct Struct {
  pub(crate) path: Path,
  pub(crate) imp:  StructImpl,
}

#[derive(Clone, Debug)]
pub enum StructImpl {
  Native(Rc<RefCell<HashMap<String, Var>>>),
  Builtin(Rc<RefCell<dyn BuiltinImpl + Send + Sync>>),
}

#[derive(Debug, Clone)]
pub enum VarSend {
  String(String),
  Int(i64),
  Float(f64),
  Bool(bool),
  None,

  Struct(StructSend),
  Map(HashMap<MapKeyVar, VarSend>),
  Array(Vec<VarSend>),
  Range(Box<(VarSend, VarSend)>),

  Callback(Path),
  Closure(Arc<Closure>),
}

#[derive(Debug, Clone)]
pub struct StructSend {
  path: Path,
  imp:  StructSendImpl,
}

#[derive(Debug, Clone)]
pub enum StructSendImpl {
  Native(HashMap<String, VarSend>),
  Builtin(Arc<dyn BuiltinImpl + Send + Sync>),
}

impl From<Var> for VarSend {
  fn from(v: Var) -> Self {
    match v {
      Var::String(v) => VarSend::String(v.to_string()),
      Var::Int(v) => VarSend::Int(v),
      Var::Float(v) => VarSend::Float(v),
      Var::Bool(v) => VarSend::Bool(v),
      Var::None => VarSend::None,

      Var::Struct(strct) => VarSend::Struct(StructSend {
        path: strct.path,
        imp:  match strct.imp {
          StructImpl::Native(data) => StructSendImpl::Native(
            data.borrow().iter().map(|(k, v)| (k.clone(), v.clone().into())).collect(),
          ),
          StructImpl::Builtin(data) => {
            StructSendImpl::Builtin(Arc::from(data.borrow().box_clone()))
          }
        },
      }),
      Var::Map(map) => {
        VarSend::Map(map.borrow().iter().map(|(k, v)| (k.clone(), v.clone().into())).collect())
      }
      Var::Array(items) => {
        VarSend::Array(items.borrow().iter().map(|v| v.clone().into()).collect())
      }
      Var::Range(r) => VarSend::Range(Box::new((r.0.clone().into(), r.1.clone().into()))),

      Var::Callback(path) => VarSend::Callback(path),
      Var::Closure(c) => VarSend::Closure(Arc::new((*c).clone())),
    }
  }
}
impl From<VarSend> for Var {
  fn from(v: VarSend) -> Self {
    match v {
      VarSend::String(v) => Var::String(Rc::new(v)),
      VarSend::Int(v) => Var::Int(v),
      VarSend::Float(v) => Var::Float(v),
      VarSend::Bool(v) => Var::Bool(v),
      VarSend::None => Var::None,

      VarSend::Struct(strct) => Var::Struct(Struct {
        path: strct.path,
        imp:  match strct.imp {
          StructSendImpl::Native(data) => StructImpl::Native(Rc::new(RefCell::new(
            data.into_iter().map(|(k, v)| (k, v.into())).collect(),
          ))),
          StructSendImpl::Builtin(data) => StructImpl::Builtin(data.rc_clone()),
        },
      }),
      VarSend::Map(map) => {
        Var::Map(Rc::new(RefCell::new(map.into_iter().map(|(k, v)| (k, v.into())).collect())))
      }
      VarSend::Array(items) => {
        Var::Array(Rc::new(RefCell::new(items.into_iter().map(Into::into).collect())))
      }
      VarSend::Range(r) => Var::Range(Rc::new((r.0.into(), r.1.into()))),

      VarSend::Callback(path) => Var::Callback(path),
      VarSend::Closure(c) => Var::Closure(Rc::new((*c).clone())),
    }
  }
}
impl From<MapKeyVar> for Var {
  fn from(v: MapKeyVar) -> Self {
    match v {
      MapKeyVar::String(v) => Var::String(Rc::new(v)),
      MapKeyVar::Int(v) => Var::Int(v),
      MapKeyVar::Bool(v) => Var::Bool(v),
      MapKeyVar::Struct(strct) => match strct.imp {
        MapKeyStructImpl::Native(data) => Var::Struct(Struct {
          path: strct.path,
          imp:  StructImpl::Native(Rc::new(RefCell::new(
            data.into_iter().map(|(k, v)| (k, v.into())).collect(),
          ))),
        }),
        MapKeyStructImpl::Builtin(data) => data.to_var(),
      },
      MapKeyVar::Array(items) => {
        Var::Array(Rc::new(RefCell::new(items.into_iter().map(Into::into).collect())))
      }
    }
  }
}

pub struct DynHasher<'a> {
  hasher: &'a mut dyn Hasher,
}

impl Hasher for DynHasher<'_> {
  fn finish(&self) -> u64 {
    self.hasher.finish()
  }
  fn write(&mut self, bytes: &[u8]) {
    self.hasher.write(bytes);
  }
  fn write_u8(&mut self, i: u8) {
    self.hasher.write_u8(i)
  }
  fn write_u16(&mut self, i: u16) {
    self.hasher.write_u16(i)
  }
  fn write_u32(&mut self, i: u32) {
    self.hasher.write_u32(i)
  }
  fn write_u64(&mut self, i: u64) {
    self.hasher.write_u64(i)
  }
  fn write_u128(&mut self, i: u128) {
    self.hasher.write_u128(i)
  }
  fn write_usize(&mut self, i: usize) {
    self.hasher.write_usize(i)
  }
  fn write_i8(&mut self, i: i8) {
    self.hasher.write_i8(i)
  }
  fn write_i16(&mut self, i: i16) {
    self.hasher.write_i16(i)
  }
  fn write_i32(&mut self, i: i32) {
    self.hasher.write_i32(i)
  }
  fn write_i64(&mut self, i: i64) {
    self.hasher.write_i64(i)
  }
  fn write_i128(&mut self, i: i128) {
    self.hasher.write_i128(i)
  }
  fn write_isize(&mut self, i: isize) {
    self.hasher.write_isize(i)
  }
}

pub trait MapKeyBuiltinImpl: std::fmt::Debug + std::any::Any {
  fn hash(&self, state: &mut DynHasher);
  fn eq(&self, other: &dyn MapKeyBuiltinImpl) -> bool;
  fn box_clone(&self) -> Box<dyn MapKeyBuiltinImpl + Send + Sync>;
  fn to_builtin(&self) -> Rc<RefCell<dyn BuiltinImpl + Send + Sync>>;
  fn as_any(&self) -> &dyn std::any::Any;
}

/// A wrapper for `Box<dyn MapKeyBuiltinImpl>`. This implements all the traits
/// needed by `MapKeyVar`, while allowing for dynamic calls to
/// `MapKeyBuiltinImpl`.
#[derive(Debug)]
pub struct MapKeyBuiltin {
  path:    Path,
  builtin: Box<dyn MapKeyBuiltinImpl + Send + Sync>,
}

impl PartialEq for MapKeyBuiltin {
  fn eq(&self, other: &Self) -> bool {
    self.builtin.eq(other.builtin.as_ref())
  }
}
impl Eq for MapKeyBuiltin {}

impl Hash for MapKeyBuiltin {
  fn hash<H: Hasher>(&self, state: &mut H) {
    self.builtin.hash(&mut DynHasher { hasher: state });
  }
}

impl Clone for MapKeyBuiltin {
  fn clone(&self) -> MapKeyBuiltin {
    MapKeyBuiltin { path: self.path.clone(), builtin: self.builtin.box_clone() }
  }
}

impl MapKeyBuiltin {
  pub fn new(path: Path, builtin: Box<dyn MapKeyBuiltinImpl + Send + Sync>) -> Self {
    MapKeyBuiltin { path, builtin }
  }
  pub fn to_var(self) -> Var {
    Var::Struct(Struct { path: self.path, imp: StructImpl::Builtin(self.builtin.to_builtin()) })
  }
}

/// A subset of [`Var`]. This type is valid as a HashMap key, and
/// variables are converted into this any time they are used as a map key.
///
/// The Struct variant is checked at runtime, and every field on the struct must
/// be valid to convert into a map key. The same goes for arrays, which are
/// still valid to have multiple types, so long as they are all map keys.
#[derive(Debug, Clone, Hash, PartialEq, Eq)]
pub enum MapKeyVar {
  String(String),
  Int(i64),
  Bool(bool),
  Array(Vec<MapKeyVar>),
  /* I have removed maps as keys. They were never implemented, and the only way
   * I can think of doing it is to sort the entire map any time you use it as
   * an index, which would add a bunch more complication. If anyone wants to
   * implement this, it would be a welcome feature. I just don't know how to
   * implement it right now.
   * Map(Vec<(MapKeyVar, MapKeyVar)>), */
  Struct(MapKeyStruct),
}

#[derive(Debug, Clone, Hash, PartialEq, Eq)]
pub struct MapKeyStruct {
  path: Path,
  imp:  MapKeyStructImpl,
}

#[derive(Debug, Clone, Hash, PartialEq, Eq)]
pub enum MapKeyStructImpl {
  Native(Vec<(String, MapKeyVar)>),
  Builtin(MapKeyBuiltin),
}

impl VarID {
  pub fn local(id: usize, pos: Span) -> VarID {
    VarID { pos, kind: VarKind::Local(id) }
  }
  pub fn global(name: String, pos: Span) -> VarID {
    VarID { pos, kind: VarKind::Global(name) }
  }
  pub fn local_value(&self) -> Option<usize> {
    match self.kind {
      VarKind::Local(id) => Some(id),
      VarKind::Global(_) => None,
    }
  }
  pub fn global_value(&self) -> Option<&String> {
    match &self.kind {
      VarKind::Local(_) => None,
      VarKind::Global(name) => Some(name),
    }
  }
  pub fn pos(&self) -> Span {
    self.pos
  }
}

impl PartialEq for Var {
  fn eq(&self, other: &Self) -> bool {
    match self {
      Var::String(v) => matches!(other, Var::String(o) if v == o),
      Var::Int(v) => matches!(other, Var::Int(o) if v == o),
      Var::Float(v) => matches!(other, Var::Float(o) if v == o),
      Var::Bool(v) => matches!(other, Var::Bool(o) if v == o),
      // The name of the struct, the fields, and the order of fields
      Var::Struct(strct) => {
        matches!(other, Var::Struct(o_strct) if strct.path == o_strct.path && strct.imp == o_strct.imp)
      }
      Var::Map(v) => matches!(other, Var::Map(o) if v == o),
      Var::Array(v) => matches!(other, Var::Array(o) if v == o),
      Var::Range(v) => matches!(other, Var::Range(o) if v == o),
      Var::None => matches!(other, Var::None),

      Var::Callback(_) => false,
      // Closures are never equal.
      Var::Closure(_) => false,
    }
  }
}
impl PartialEq for StructImpl {
  fn eq(&self, other: &Self) -> bool {
    match self {
      StructImpl::Native(fields) => matches!(other, StructImpl::Native(o) if fields == o),
      StructImpl::Builtin(_) => false,
    }
  }
}

impl Clone for Var {
  fn clone(&self) -> Self {
    // Useful for debugging
    // println!("CLONING {:?}", self);
    match self {
      Var::String(v) => Var::String(v.clone()),
      Var::Int(v) => Var::Int(*v),
      Var::Float(v) => Var::Float(*v),
      Var::Bool(v) => Var::Bool(*v),
      Var::Struct(strct) => Var::Struct(strct.clone()),
      Var::Map(v) => Var::Map(v.clone()),
      Var::Array(v) => Var::Array(v.clone()),
      Var::Range(v) => Var::Range(v.clone()),
      Var::None => Var::None,

      Var::Callback(path) => Var::Callback(path.clone()),
      Var::Closure(c) => Var::Closure(c.clone()),
    }
  }
}

impl PartialEq for VarSend {
  fn eq(&self, other: &Self) -> bool {
    match self {
      VarSend::String(v) => matches!(other, VarSend::String(o) if v == o),
      VarSend::Int(v) => matches!(other, VarSend::Int(o) if v == o),
      VarSend::Float(v) => matches!(other, VarSend::Float(o) if v == o),
      VarSend::Bool(v) => matches!(other, VarSend::Bool(o) if v == o),
      // The name of the struct, the fields, and the order of fields
      VarSend::Struct(strct) => {
        matches!(other, VarSend::Struct(o_strct) if strct.path == o_strct.path && strct.imp == o_strct.imp)
      }
      VarSend::Map(v) => matches!(other, VarSend::Map(o) if v == o),
      VarSend::Array(v) => matches!(other, VarSend::Array(o) if v == o),
      VarSend::Range(v) => matches!(other, VarSend::Range(o) if v == o),
      VarSend::None => matches!(other, VarSend::None),

      VarSend::Callback(_) => false,
      // Closures are never equal.
      VarSend::Closure(_) => false,
    }
  }
}
impl PartialEq for StructSendImpl {
  fn eq(&self, other: &Self) -> bool {
    match self {
      StructSendImpl::Native(fields) => matches!(other, StructSendImpl::Native(o) if fields == o),
      StructSendImpl::Builtin(_) => false,
    }
  }
}

impl Var {
  /// Checks if the variable matches the given type. This is used internally to
  /// match variables with struct field types.
  pub fn is(&self, ty: &VarType) -> bool {
    if matches!(self, Var::Closure(_)) {
      return false;
    }
    if matches!(ty, VarType::None) {
      return true;
    }
    match self {
      Var::String(_) => matches!(ty, VarType::String),
      Var::Int(_) => matches!(ty, VarType::Int),
      Var::Float(_) => matches!(ty, VarType::Float),
      Var::Bool(_) => matches!(ty, VarType::Bool),
      Var::Struct(strct) => matches!(ty, VarType::Struct(ty_path) if &strct.path == ty_path),
      Var::Array(_) => matches!(ty, VarType::Array),
      Var::Range(_) => matches!(ty, VarType::Range),
      Var::Map(_) => matches!(ty, VarType::Map),
      Var::None => false,

      Var::Callback(_) => matches!(ty, VarType::Callback),
      Var::Closure(_) => false,
    }
  }

  /// Gets the type of self.
  pub fn ty(&self) -> VarType {
    match self {
      Var::String(_) => VarType::String,
      Var::Int(_) => VarType::Int,
      Var::Float(_) => VarType::Float,
      Var::Bool(_) => VarType::Bool,
      Var::Struct(strct) => strct.ty(),
      Var::Array(_) => VarType::Array,
      Var::Range(_) => VarType::Range,
      Var::Map(_) => VarType::Map,
      Var::None => VarType::None,

      Var::Callback(_) => VarType::Callback,
      Var::Closure(_) => VarType::Closure,
    }
  }

  /// Tries to convert the variable to a map key. If this returns None, then
  /// this variable cannot be used as a key within a map. This typically
  /// results in a runtime error.
  pub fn as_map_key(&self) -> Option<MapKeyVar> {
    match self {
      Var::String(v) => Some(MapKeyVar::String(v.as_ref().clone())),
      Var::Int(v) => Some(MapKeyVar::Int(*v)),
      Var::Float(_) => None,
      Var::Bool(v) => Some(MapKeyVar::Bool(*v)),
      Var::Struct(strct) => match &strct.imp {
        StructImpl::Native(fields) => Some(MapKeyVar::Struct(MapKeyStruct {
          path: strct.path.clone(),
          imp:  MapKeyStructImpl::Native({
            let fields_borrow = fields.borrow();
            let mut fields: Vec<_> = fields_borrow.iter().collect();
            fields.sort_unstable_by(|a, b| a.0.cmp(b.0));
            fields
              .into_iter()
              .map(|(k, v)| Some((k.clone(), v.as_map_key()?)))
              .collect::<Option<Vec<(String, MapKeyVar)>>>()?
          }),
        })),
        StructImpl::Builtin(b) => b.borrow().as_map_key().map(|imp| {
          MapKeyVar::Struct(MapKeyStruct {
            path: strct.path.clone(),
            imp:  MapKeyStructImpl::Builtin(imp),
          })
        }),
      },
      Var::Array(v) => Some(MapKeyVar::Array(
        v.borrow().iter().map(|v| v.as_map_key()).collect::<Option<Vec<MapKeyVar>>>()?,
      )),
      Var::Map(_) => None,
      Var::Range(_) => None,
      Var::None => None,

      Var::Callback(_) => None,
      Var::Closure(_) => None,
    }
  }

  /// If this is a struct, then this will look for the field name in the struct.
  /// Otherwise, this will return an error saying that fields cannot be
  /// accessed for non-struct types.
  pub fn field(&self, name: &Ident) -> Result<Var> {
    if let Var::Struct(strct) = self {
      return strct.field(name);
    }
    Err(RuntimeError::Undefined {
      pos:   name.pos(),
      name:  "field",
      value: name.to_string(),
      ty:    Some(self.ty()),
    })
  }
  /// If this is a struct, then this will look for the field name in the struct.
  /// Otherwise, this will return an error saying that fields cannot be
  /// accessed for non-struct types.
  pub fn set_field(&mut self, name: &Ident, val: Var) -> Result<()> {
    if let Var::Struct(strct) = self {
      return strct.set_field(name, val);
    }
    Err(RuntimeError::Undefined {
      pos:   name.pos(),
      name:  "field",
      value: name.to_string(),
      ty:    Some(self.ty()),
    })
  }

  /// If this is an array, then this will look for the field name in the struct.
  /// Otherwise, this will return an error saying that non-array types cannot be
  /// indexed into.
  pub fn get_index(&self, index: i64, pos: Span) -> Result<Var> {
    if let Var::Array(items) = self {
      if index < 0 {
        return Err(RuntimeError::custom("index out of bounds", pos));
      }
      if let Some(it) = items.borrow().get(index as usize) {
        Ok(it.clone())
      } else {
        Err(RuntimeError::custom("index out of bounds", pos))
      }
    } else {
      Err(RuntimeError::custom(format!("cannot access index of non-array type {self:?}"), pos))
    }
  }
  /// If this is an array, then this will look for the field name in the struct.
  /// Otherwise, this will return an error saying that non-array types cannot be
  /// indexed into.
  pub fn set_index(&mut self, index: i64, pos: Span, val: Var) -> Result<()> {
    if let Var::Array(items) = self {
      if index < 0 {
        return Err(RuntimeError::custom("index out of bounds", pos));
      }
      if let Some(it) = items.borrow_mut().get_mut(index as usize) {
        *it = val;
        Ok(())
      } else {
        Err(RuntimeError::custom("index out of bounds", pos))
      }
    } else {
      Err(RuntimeError::custom(format!("cannot access index of non-array type {self:?}"), pos))
    }
  }

  pub fn new_string(v: impl Into<String>) -> Self {
    Var::String(Rc::new(v.into()))
  }
}

macro_rules! var_op {
  ( $name:ident, $name_mut:ident, $val:ident, $ty:ty, $msg:expr ) => {
    /// Unwraps the variable into the given type. If it is not that type,
    /// a RuntimeError will be produced, using the given Span.
    pub fn $name<'a>(&'a self, pos: Span) -> Result<Ref<'a, $ty>> {
      match self {
        Self::$val(ref v) => Ok(v.borrow()),
        v => Err(RuntimeError::Custom(format!($msg, v), pos)),
      }
    }
    /// Unwraps the variable a mutable reference of the given type. If it
    /// is not that type, a RuntimeError will be produced, using the given Span.
    pub fn $name_mut(&self, pos: Span) -> Result<RefMut<'_, $ty>> {
      match self {
        Self::$val(ref v) => Ok(v.borrow_mut()),
        v => Err(RuntimeError::Custom(format!($msg, v), pos)),
      }
    }
  };
}
macro_rules! var_op_copy {
  ( $name:ident, $name_mut:ident, $val:ident, $ty:ty, $msg:expr ) => {
    /// Unwraps the variable into the given type. If it is not that type,
    /// a RuntimeError will be produced, using the given Span.
    pub fn $name(&self, pos: Span) -> Result<$ty> {
      match self {
        Self::$val(v) => Ok(*v),
        v => Err(RuntimeError::Custom(format!($msg, v), pos)),
      }
    }
    /// Unwraps the variable a mutable reference of the given type. If it
    /// is not that type, a RuntimeError will be produced, using the given Span.
    pub fn $name_mut(&mut self, pos: Span) -> Result<&mut $ty> {
      match self {
        Self::$val(v) => Ok(v),
        v => Err(RuntimeError::Custom(format!($msg, v), pos)),
      }
    }
  };
}

impl Var {
  /// Unwraps the variable into a string. If it is not a string,
  /// a RuntimeError will be produced, using the given Span.
  pub fn string(&self, pos: Span) -> Result<&String> {
    match self {
      Var::String(v) => Ok(v),
      v => Err(RuntimeError::Custom(format!("expected a string, got {}", v), pos)),
    }
  }

  /// Unwraps the variable into a closure. If it is not a closure, a
  /// RuntimeError will be produced.
  pub fn closure(&self, pos: Span) -> Result<&Closure> {
    match self {
      Var::Closure(v) => Ok(v),
      v => Err(RuntimeError::Custom(format!("expected a string, got {}", v), pos)),
    }
  }

  var_op_copy!(int, int_mut, Int, i64, "expected an int, got {}");
  var_op_copy!(float, float_mut, Float, f64, "expected a float, got {}");
  var_op_copy!(bool, bool_mut, Bool, bool, "expected a bool, got {}");
  var_op!(map, map_mut, Map, HashMap<MapKeyVar, Var>, "expected a map, got {}");
  var_op!(arr, arr_mut, Array, Vec<Var>, "expected an array, got {}");
  // var_op!(strct, struct_mut, Struct, HashMap<String, Var>, "expected a struct,
  // got {}");

  /// Unwraps the variable into a Callback path. If it is not a callback,
  /// a RuntimeError will be produced, using the given Span.
  pub fn callback(&self, pos: Span) -> Result<&Path> {
    match self {
      Self::Callback(path) => Ok(path),
      v => Err(RuntimeError::Custom(format!("expected a callback, got {}", v), pos)),
    }
  }

  /// Unwraps the variable into a struct. If it is not a struct,
  /// a RuntimeError will be produced, using the given Span.
  pub fn strct(&self, pos: Span) -> Result<&Struct> {
    match self {
      Var::Struct(strct) => Ok(strct),
      v => Err(RuntimeError::Custom(format!("expected a struct, got {}", v), pos)),
    }
  }
}

impl MapKeyVar {
  /// Unwraps the variable into a string. If it is not a string,
  /// a RuntimeError will be produced, using the given Span.
  pub fn string(&self, pos: Span) -> Result<&String> {
    match self {
      Self::String(v) => Ok(v),
      v => Err(RuntimeError::Custom(format!("expected a string, got {}", v), pos)),
    }
  }

  var_op_copy!(int, int_mut, Int, i64, "expected an int, got {}");
  var_op_copy!(bool, bool_mut, Bool, bool, "expected a bool, got {}");

  /// Unwraps the variable into a struct. If it is not a struct,
  /// a RuntimeError will be produced, using the given Span.
  pub fn strct(&self, pos: Span) -> Result<&MapKeyStruct> {
    match self {
      Self::Struct(strct) => Ok(strct),
      v => Err(RuntimeError::Custom(format!("expected a struct, got {}", v), pos)),
    }
  }
}

impl Var {
  /// Should only be used by proc macros.
  #[doc(hidden)]
  pub fn new_builtin(path: Path, imp: Rc<RefCell<dyn BuiltinImpl + Send + Sync>>) -> Self {
    Var::Struct(Struct { path, imp: StructImpl::Builtin(imp) })
  }
}

impl Struct {
  pub fn path(&self) -> &Path {
    &self.path
  }
  pub fn ty(&self) -> VarType {
    VarType::Struct(self.path.clone())
  }

  pub fn field(&self, field: &Ident) -> Result<Var> {
    match &self.imp {
      StructImpl::Native(fields) => match fields.borrow().get(field.as_ref()) {
        Some(f) => Ok(f.clone()),
        None => Err(RuntimeError::Undefined {
          pos:   field.pos(),
          name:  "field",
          value: field.to_string(),
          ty:    Some(self.ty()),
        }),
      },
      StructImpl::Builtin(b) => match b.borrow().field(field) {
        Some(v) => Ok(v),
        _ => Err(RuntimeError::Undefined {
          pos:   field.pos(),
          name:  "field",
          value: field.to_string(),
          ty:    Some(self.ty()),
        }),
      },
    }
  }
  pub fn set_field(&self, field: &Ident, to: Var) -> Result<()> {
    match &self.imp {
      StructImpl::Native(fields) => {
        if let Some(f) = fields.borrow_mut().get_mut(field.as_ref()) {
          *f = to;
          Ok(())
        } else {
          Err(RuntimeError::Undefined {
            pos:   field.pos(),
            name:  "field",
            value: field.to_string(),
            ty:    Some(self.ty()),
          })
        }
      }
      StructImpl::Builtin(_) => todo!("set fields on builtin"),
    }
  }

  pub fn as_builtin<'a>(
    &'a self,
    pos: Span,
  ) -> Result<Ref<'a, (dyn BuiltinImpl + Send + Sync + 'static)>> {
    match &self.imp {
      StructImpl::Builtin(b) => Ok(b.borrow()),
      _ => Err(RuntimeError::Custom(format!("expected a builtin, got a struct instead"), pos)),
    }
  }
  pub fn as_builtin_mut<'a>(
    &'a self,
    pos: Span,
  ) -> Result<RefMut<'a, (dyn BuiltinImpl + Send + Sync + 'static)>> {
    match &self.imp {
      StructImpl::Builtin(b) => Ok(b.borrow_mut()),
      _ => Err(RuntimeError::Custom(format!("expected a builtin, got a struct instead"), pos)),
    }
  }
}

macro_rules! from_int {
  ( $( $ty:ty ),* ) => {
    $(
      impl From<$ty> for Var {
        fn from(v: $ty) -> Var {
          Var::Int(v.into())
        }
      }
    )*
  };
}

from_int!(i64, i32, i16, i8, u32, u16, u8);

impl From<bool> for Var {
  fn from(v: bool) -> Var {
    Var::Bool(v)
  }
}

impl From<String> for Var {
  fn from(val: String) -> Var {
    Var::String(Rc::new(val))
  }
}
impl From<Vec<Var>> for Var {
  fn from(val: Vec<Var>) -> Var {
    Var::Array(Rc::new(RefCell::new(val)))
  }
}
impl From<()> for Var {
  fn from(_: ()) -> Var {
    Var::None
  }
}
impl From<f64> for Var {
  fn from(num: f64) -> Var {
    Var::Float(num)
  }
}
impl From<f32> for Var {
  fn from(num: f32) -> Var {
    Var::Float(num.into())
  }
}

impl fmt::Display for VarID {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(
      f,
      "VarID({:?} at {}:{})",
      self.local_value(),
      self.pos.start().index(),
      self.pos.start().index()
    )
  }
}

impl fmt::Display for Var {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    match self {
      Var::String(v) => write!(f, "{}", v),
      Var::Int(v) => write!(f, "{}", v),
      Var::Float(v) => write!(f, "{}", v),
      Var::Bool(v) => write!(f, "{}", v),
      Var::Struct(strct) => {
        write!(f, "{}:{{", strct.path)?;
        // TODO: Display all fields on a struct
        /*
        let fields_borrow = strct.fields.borrow();
        let mut fields: Vec<_> = fields_borrow.iter().collect();
        fields.sort_unstable_by(|a, b| a.0.cmp(b.0));
        let len = fields.len();
        for (i, (k, v)) in fields.into_iter().enumerate() {
          if i == len - 1 {
            write!(f, "{}:{}", k, v)?;
          } else {
            write!(f, "{}:{}, ", k, v)?;
          }
        }
        */
        write!(f, "}}")
      }
      Var::Map(map) => {
        let map = map.borrow();
        write!(f, "map{{")?;
        for (i, (k, v)) in map.iter().enumerate() {
          if i == map.len() - 1 {
            write!(f, "{}:{}", k, v)?;
          } else {
            write!(f, "{}:{}, ", k, v)?;
          }
        }
        write!(f, "}}")
      }
      Var::Array(arr) => {
        let arr = arr.borrow();
        write!(f, "[")?;
        for (i, v) in arr.iter().enumerate() {
          if i == arr.len() - 1 {
            write!(f, "{}", v)?;
          } else {
            write!(f, "{}, ", v)?;
          }
        }
        write!(f, "]")
      }
      Var::Range(r) => write!(f, "{}..{}", r.0, r.1),
      Var::None => write!(f, "None"),

      Var::Callback(path) => write!(f, "Callback at {}", path),
      Var::Closure(c) => write!(f, "Closure defined at {:?}", c.pos()),
    }
  }
}

impl fmt::Display for MapKeyVar {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    match self {
      Self::String(v) => write!(f, "{}", v),
      Self::Int(v) => write!(f, "{}", v),
      Self::Bool(v) => write!(f, "{}", v),
      Self::Struct(strct) => {
        write!(f, "{}:{{", strct.path)?;
        // TODO: Get all fields on a struct
        /*
        for (i, (k, v)) in strct.fields().iter().enumerate() {
          if i == s.len() - 1 {
            write!(f, "{}:{}", k, v)?;
          } else {
            write!(f, "{}:{}, ", k, v)?;
          }
        }
        */
        write!(f, "}}")
      }
      Self::Array(arr) => {
        write!(f, "[")?;
        for (i, v) in arr.iter().enumerate() {
          if i == arr.len() - 1 {
            write!(f, "{}", v)?;
          } else {
            write!(f, "{}, ", v)?;
          }
        }
        write!(f, "]")
      }
    }
  }
}

impl Var {
  /// Shorthand for `self.ord(other)? == Ordering::Less`.
  pub fn less(&self, other: &Var, pos: Span) -> Result<bool> {
    Ok(self.ord(other, pos)? == Ordering::Less)
  }
  /// Shorthand for `self.ord(other)? == Ordering::Greater`.
  pub fn greater(&self, other: &Var, pos: Span) -> Result<bool> {
    Ok(self.ord(other, pos)? == Ordering::Greater)
  }
  /// Shorthand for `self.ord(other)? == Ordering::Less || self.ord(other)? ==
  /// Ordering::Equal`.
  pub fn lte(&self, other: &Var, pos: Span) -> Result<bool> {
    let o = self.ord(other, pos)?;
    Ok(o == Ordering::Less || o == Ordering::Equal)
  }
  /// Shorthand for `self.ord(other)? == Ordering::Greater || self.ord(other)?
  /// == Ordering::Equal`.
  pub fn gte(&self, other: &Var, pos: Span) -> Result<bool> {
    let o = self.ord(other, pos)?;
    Ok(o == Ordering::Greater || o == Ordering::Equal)
  }

  /// Checks the order between `self` and `other`. Examples:
  ///
  /// ```
  /// # use panda_parse::token::Span;
  /// # use panda_runtime::Var;
  /// # use std::cmp::Ordering;
  /// // Numbers are compared like numbers should be
  /// assert_eq!(Var::Int(5).ord(&Var::Int(6), Span::default()).unwrap(), Ordering::Less);
  /// assert_eq!(Var::Int(5).ord(&Var::Int(5), Span::default()).unwrap(), Ordering::Equal);
  /// assert_eq!(Var::Float(5.0).ord(&Var::Float(3.2), Span::default()).unwrap(), Ordering::Greater);
  ///
  /// // Strings are compared by their shorest segments alphabetically, then by length.
  /// assert_eq!(Var::new_string("b").ord(&Var::new_string("aaa"), Span::default()).unwrap(), Ordering::Greater);
  /// assert_eq!(Var::new_string("aaa").ord(&Var::new_string("ccc"), Span::default()).unwrap(), Ordering::Less);
  /// assert_eq!(Var::new_string("aaaz").ord(&Var::new_string("ccc"), Span::default()).unwrap(), Ordering::Less);
  /// assert_eq!(Var::new_string("ccca").ord(&Var::new_string("ccc"), Span::default()).unwrap(), Ordering::Greater);
  ///
  /// // The rest of the types cannot be ordered.
  /// assert!(Var::Bool(true).ord(&Var::Bool(false), Span::default()).is_err());
  /// assert!(Var::None.ord(&Var::None, Span::default()).is_err());
  ///
  /// // Any differing types cannot be ordered.
  /// assert!(Var::Int(5).ord(&Var::Float(6.0), Span::default()).is_err());
  /// ```
  pub fn ord(&self, other: &Var, pos: Span) -> Result<Ordering> {
    match self {
      Var::String(l) => match other {
        Var::String(r) => Ok(l.cmp(r)),
        _ => Err(RuntimeError::custom("Cannot order different types", pos)),
      },
      Var::Int(l) => match other {
        Var::Int(r) => Ok(l.cmp(r)),
        _ => Err(RuntimeError::custom("Cannot order different types", pos)),
      },
      Var::Float(l) => match other {
        Var::Float(r) => match l.partial_cmp(r) {
          Some(v) => Ok(v),
          None => Err(RuntimeError::custom(&format!("Cannot compare floats {} and {}", l, r), pos)),
        },
        _ => Err(RuntimeError::custom("Cannot order different types", pos)),
      },
      Var::Bool(_) => Err(RuntimeError::custom("Cannot order booleans", pos)),
      Var::Struct(_) => Err(RuntimeError::custom("Cannot order structs", pos)),
      Var::Map(_) => Err(RuntimeError::custom("Cannot order maps", pos)),
      Var::Array(_) => Err(RuntimeError::custom("Cannot order arrays", pos)),
      Var::Range(_) => Err(RuntimeError::custom("Cannot order ranges", pos)),
      Var::None => Err(RuntimeError::custom("Cannot order None", pos)),
      Var::Callback(_) => Err(RuntimeError::custom("Cannot order callbacks", pos)),
      Var::Closure(_) => Err(RuntimeError::custom("Cannot order closures", pos)),
    }
  }
}
