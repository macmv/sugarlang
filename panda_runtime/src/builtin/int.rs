use panda_docs::markdown;

use crate::{RtEnv, RuntimeError, Var};
use std::rc::Rc;

pub fn add_core_builtins(env: &mut RtEnv) {
  env.add_builtin_fn(path!(int::to_s), markdown!(), true, |_, slf, args, pos| {
    RuntimeError::check_arg_len(&args, 0, pos)?;
    let slf = slf.int(pos)?;
    Ok(Var::String(Rc::new(slf.to_string())))
  });
  env.add_builtin_fn(path!(int::to_f), markdown!(), true, |_, slf, args, pos| {
    RuntimeError::check_arg_len(&args, 0, pos)?;
    let slf = slf.int(pos)?;
    Ok(Var::Float(slf as f64))
  });
}
