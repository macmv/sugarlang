use crate::{RtEnv, RuntimeError, Var};
use panda_docs::markdown;

pub fn add_core_builtins(env: &mut RtEnv) {
  env.add_builtin_fn(
    path!(arr::push),
    markdown!(
      /// Pushes an element into the end of the array.
      ///
      /// ```
      /// let array = [1, 2, 3]
      /// array.push(10)
      /// // array is now [1, 2, 3, 10]
      /// ```
    ),
    true,
    |_, slf, mut args, pos| {
      RuntimeError::check_arg_len(&args, 1, pos)?;
      let val = args.pop().unwrap();
      let mut slf = slf.arr_mut(pos)?;
      slf.push(val);
      Ok(Var::None)
    },
  );
  env.add_builtin_fn(path!(arr::pop), markdown!(), true, |_, slf, args, pos| {
    RuntimeError::check_arg_len(&args, 0, pos)?;
    let mut slf = slf.arr_mut(pos)?;
    Ok(slf.pop().unwrap_or(Var::None))
  });
  env.add_builtin_fn(path!(arr::insert), markdown!(), true, |_, slf, mut args, pos| {
    RuntimeError::check_arg_len(&args, 2, pos)?;
    let val = args.pop().unwrap();
    let idx = args.pop().unwrap().int(pos)?;
    let mut slf = slf.arr_mut(pos)?;
    if idx < 0 {
      Err(RuntimeError::custom("cannot insert an item at a negative index", pos))
    } else if idx as usize > slf.len() {
      Err(RuntimeError::custom(
        &format!("cannot insert an item outside of the array ({} > {})", idx, slf.len()),
        pos,
      ))
    } else {
      slf.insert(idx as usize, val);
      Ok(Var::None)
    }
  });
  env.add_builtin_fn(path!(arr::len), markdown!(), true, |_, slf, args, pos| {
    RuntimeError::check_arg_len(&args, 0, pos)?;
    let slf = slf.arr(pos)?;
    Ok(Var::Int(slf.len() as i64))
  });
  env.add_builtin_fn(path!(arr::index), markdown!(), true, |_, slf, args, pos| {
    RuntimeError::check_arg_len(&args, 1, pos)?;
    let slf = slf.arr(pos)?;
    let idx = args[0].int(pos)?;
    if idx < 0 {
      Err(RuntimeError::custom(
        &format!("cannot use `{}` as an index (negative indices are not allowed)", idx),
        pos,
      ))
    } else if idx >= slf.len() as i64 {
      Err(RuntimeError::custom(
        &format!(
          "index `{}` is too large (array has {} element{})",
          idx,
          slf.len(),
          "s".repeat((slf.len() != 1) as usize)
        ),
        pos,
      ))
    } else {
      Ok(slf[idx as usize].clone())
    }
  });

  // ITERATOR FUNCTIONS

  env.add_builtin_fn(path!(arr::for_each), markdown!(), true, |env, slf, mut args, pos| {
    RuntimeError::check_arg_len(&args, 1, pos)?;
    let arg = args.pop().unwrap();
    let filter = arg.closure(pos)?;
    let slf = slf.arr_mut(pos)?;
    for it in slf.iter() {
      filter.call(env, vec![it.clone()])?;
    }
    Ok(Var::None)
  });
  env.add_builtin_fn(path!(arr::filter), markdown!(), true, |env, slf, mut args, pos| {
    RuntimeError::check_arg_len(&args, 1, pos)?;
    let arg = args.pop().unwrap();
    let filter = arg.closure(pos)?;
    let slf = slf.arr_mut(pos)?;
    let mut new = Vec::with_capacity(slf.len());
    for it in slf.iter() {
      let res = filter.call(env, vec![it.clone()])?.bool(pos)?;
      if res {
        new.push(it.clone());
      }
    }
    Ok(new.into())
  });
}
