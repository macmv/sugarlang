use crate::{parse::VarType, Var, VarSend};

pub trait PandaType {
  fn var_type() -> VarType;
}

macro_rules! impls {
  ( $value:expr; $( $ty:ty ),* ) => {
    $(
      impl PandaType for $ty {
        fn var_type() -> VarType { $value }
      }
    )*
  }
}

impls!(VarType::Bool; bool);
impls!(VarType::Int; i8, u16, i16, u32, i32, i64);
impls!(VarType::Float; f32, f64);
impls!(VarType::Array; Vec<Var>, Vec<VarSend>);

impl PandaType for String {
  fn var_type() -> VarType {
    VarType::String
  }
}
