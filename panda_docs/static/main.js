function copy_text(but) {
  var parent = but.parentElement;
  var path = [
  ];
  onEach(parent.childNodes, function (child) {
    if (child.tagName === 'A') {
      path.push(child.textContent)
    }
  });
  var el = document.createElement('textarea');
  el.value = path.join('::');
  el.setAttribute('readonly', '');
  el.style.position = 'absolute';
  el.style.left = '-9999px';
  document.body.appendChild(el);
  el.select();
  document.execCommand('copy');
  document.body.removeChild(el);
  but.children[0].style.display = 'none';
  var tmp;
  if (but.childNodes.length < 2) {
    tmp = document.createTextNode('✓');
    but.appendChild(tmp)
  } else {
    onEachLazy(but.childNodes, function (e) {
      if (e.nodeType === Node.TEXT_NODE) {
        tmp = e;
        return true
      }
    });
    tmp.textContent = '✓'
  }
  if (reset_button_timeout !== null) {
    window.clearTimeout(reset_button_timeout)
  }
  function reset_button() {
    tmp.textContent = '';
    reset_button_timeout = null;
    but.children[0].style.display = ''
  }
  reset_button_timeout = window.setTimeout(reset_button, 1000)
}

function collapse_code(button) {
  let node = button.parentNode.children[2];
  if (node.style.display === "none") {
    node.style.display = "";
    button.classList.remove("collapsed");
    button.innerText = "[-]";
  } else {
    node.style.display = "none";
    button.classList.add("collapsed");
    button.innerText = "[+]";
  }
}
function collapse(content) {
}
