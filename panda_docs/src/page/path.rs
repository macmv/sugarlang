use std::ops::Index;

#[derive(Debug, Clone)]
pub struct Path {
  segments: Vec<String>,
  kind:     PathKind,
}

#[derive(Debug, Clone)]
pub enum PathKind {
  Struct,
  Func,
}

impl Path {
  pub fn new(segments: Vec<String>, kind: PathKind) -> Self {
    Path { segments, kind }
  }
  pub fn iter(&self) -> impl Iterator<Item = &str> {
    self.segments.iter().map(|v| v.as_ref())
  }
  pub fn len(&self) -> usize {
    self.segments.len()
  }
  pub fn is_empty(&self) -> bool {
    self.segments.is_empty()
  }
  pub fn join(&self, sep: &str) -> String {
    self.segments.join(sep)
  }
  pub fn push(&mut self, segment: String) {
    if !segment.is_empty() {
      self.segments.push(segment);
    }
  }
  pub fn first(&self) -> &str {
    self.segments.first().unwrap()
  }
  pub fn last(&self) -> &str {
    self.segments.last().unwrap()
  }
  pub fn pop(&mut self) -> String {
    self.segments.pop().unwrap()
  }
  pub fn href_from(&self, other: &Path) -> String {
    // This is the index where the paths diverge. Everything before this index is
    // the same, so we don't include a url to that.
    let mut diff_idx = self.segments.len() - 1;
    for (i, (a, b)) in self.segments.iter().zip(other.iter()).enumerate() {
      if a != b {
        diff_idx = i;
        break;
      }
    }
    match self.kind {
      PathKind::Struct => {
        "../".repeat(other.segments.len() - diff_idx - 1)
          + &self.segments[diff_idx..].join("/")
          + ".html"
      }
      // Functions are inlined, so we want to go to the page of the second to last element.
      // Then we add an object tag to go to the right function.
      PathKind::Func => {
        "../".repeat(other.segments.len() - diff_idx - 1)
          + &self.segments[diff_idx..self.segments.len() - 1].join("/")
          + ".html#funcs."
          + &self.segments[self.segments.len() - 1]
      }
    }
  }
}

impl Index<usize> for Path {
  type Output = str;

  fn index(&self, idx: usize) -> &Self::Output {
    &self.segments[idx]
  }
}

#[macro_export]
macro_rules! path {
  ( $($item:ident)::*, $kind:ident ) => {
    Path::new(vec![$(stringify!($item).into()),*], PathKind::$kind)
  };
}
