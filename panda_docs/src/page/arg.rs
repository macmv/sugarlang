use crate::{
  page::{Path, PathKind},
  path,
};

#[derive(Debug, Clone)]
pub struct Arg {
  name: String,
  ty:   Option<Ty>,
}

#[derive(Debug, Clone)]
pub enum Ty {
  Bool,
  Str,
  Int,
  Float,
  Struct(Path),
  Map,
  Array,
  Range,
  Any,
  Callback,
  Builtin(Path),
  Closure,
}

impl Arg {
  pub fn new(name: String, ty: Option<Ty>) -> Arg {
    Arg { name, ty }
  }
  pub fn generate(&self, path: &Path) -> String {
    if let Some(ty) = self.ty.as_ref() {
      format!("{}: {}", self.name, ty.generate(path))
    } else {
      self.name.to_string()
    }
  }
}

impl Ty {
  pub fn generate(&self, path: &Path) -> String {
    let ty_path = match self {
      Self::Bool => path!(bool, Struct),
      Self::Str => path!(str, Struct),
      Self::Int => path!(int, Struct),
      Self::Float => path!(float, Struct),
      Self::Struct(ty_path) => ty_path.clone(),
      Self::Map => path!(map, Struct),
      Self::Array => path!(arr, Struct),
      Self::Range => path!(range, Struct),
      Self::Any => path!(any, Struct),
      Self::Callback => path!(callback, Struct),
      Self::Builtin(ty_path) => ty_path.clone(),
      Self::Closure => path!(closure, Struct),
    };
    let class = match self {
      Self::Bool => "primitive",
      Self::Str => "primitive",
      Self::Int => "primitive",
      Self::Float => "primitive",
      Self::Struct(_) => "struct",
      Self::Map => "primitive",
      Self::Array => "primitive",
      Self::Range => "primitive",
      Self::Any => "primitive",
      Self::Callback => "primitive",
      Self::Builtin(_) => "builtin",
      Self::Closure => "primitive",
    };
    format!("<a class='{}' href='{}'>{}</a>", class, ty_path.href_from(path), ty_path.last())
  }
}
