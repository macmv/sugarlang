use html_builder::Node;
use std::{fmt, fmt::Write};

/// A section of markdown text. This can be converted to html.
#[derive(Debug, Clone)]
pub struct MarkdownSection {
  text: String,
}

fn parse_raw_string(content: &str) -> String {
  content.into()
}
fn parse_string(content: &str) -> String {
  let mut out = String::with_capacity(content.len());
  let mut in_escape = false;
  for c in content.chars() {
    if in_escape {
      match c {
        'n' => out.push('\n'),
        '"' => out.push('\"'),
        '\'' => out.push('\''),
        '\\' => out.push('\\'),
        _ => panic!("invalid escape `{c}`"),
      }
      in_escape = false;
    } else {
      if c == '\\' {
        in_escape = true;
      } else {
        out.push(c);
      }
    }
  }
  out
}

impl MarkdownSection {
  pub fn empty() -> Self {
    MarkdownSection { text: String::new() }
  }
  /// Should only be used internally. Parses doc comment lines (possibly
  /// prefixed by spaces, which will be trimmed) into a markdown section.
  ///
  /// This is the `tokens` section of a `syn::Attribute`. Each like should
  /// look like this: ` = "text here escaped quotes: \" and more text"
  pub fn from_lines(lines: &[&str]) -> Self {
    let mut texts = vec![];
    let mut min_spaces = 100;
    for line in lines {
      // Remove the `= `
      let line = &line[2..];
      let line = if line.chars().next() == Some('r') {
        // Remove the starting `r"` and the ending `"`
        parse_raw_string(&line[2..line.len() - 1])
      } else {
        // Remove the starting `"` and the ending `"`
        parse_string(&line[1..line.len() - 1])
      };
      if !line.is_empty() {
        let mut spaces = 0;
        for c in line.chars() {
          if c == ' ' {
            spaces += 1;
          } else {
            break;
          }
        }
        if spaces < min_spaces {
          min_spaces = spaces;
        }
      }
      texts.push(line);
    }
    if min_spaces != 0 {
      for l in texts.iter_mut() {
        if !l.is_empty() {
          *l = l[min_spaces..].to_string();
        }
      }
    }
    MarkdownSection { text: texts.join("\n") }
  }
  /// Should only be used internally. Parses meta strings (rust doc comments)
  /// into Panda docs.
  pub fn from_meta(metas: &[&str]) -> Self {
    let mut lines = vec![];
    let mut min_spaces = 100;
    for line in metas {
      let doc_start = "doc =";
      if let Some(mut line) = line.strip_prefix(doc_start) {
        line = line.trim_start();
        if line.starts_with("r\"") {
          // The line will end in "
          line = &line[2..line.len() - 1];
        } else if line.starts_with("r#\"") {
          // The line will end in "#
          line = &line[3..line.len() - 2];
        } else {
          panic!("invalid meta line: {}", line);
        }
        if !line.is_empty() {
          let mut spaces = 0;
          for c in line.chars() {
            if c == ' ' {
              spaces += 1;
            } else {
              break;
            }
          }
          if spaces < min_spaces {
            min_spaces = spaces;
          }
        }
        lines.push(line);
      } else {
        panic!("invalid meta line: {}", line);
      }
    }
    if min_spaces != 0 {
      for l in lines.iter_mut() {
        if l != &"" {
          *l = &l[min_spaces..];
        }
      }
    }
    MarkdownSection { text: lines.join("\n") }
  }

  pub fn generate(&self, node: &mut Node, anchor_prefix: &str) -> fmt::Result {
    let mut inline_code = false;
    let mut code_block = false;
    let mut prev_end = 0;
    let mut in_heading = None;
    for (i, c) in self.text.char_indices() {
      if prev_end > i {
        continue;
      }
      match c {
        '#' => {
          if i == 0 || self.text.as_bytes().get(i - 1) != Some(&b'\n') {
            continue;
          }
          write!(node, "{}", &self.text[prev_end..i])?;
          let mut heading_level = 1;
          while self.text.as_bytes().get(i + heading_level) == Some(&b'#') {
            heading_level += 1;
            if heading_level >= 6 {
              break;
            }
          }
          prev_end = i + heading_level;
          in_heading = Some(heading_level);
        }
        '\n' => {
          if !code_block {
            // Double newline
            if self.text.as_bytes().get(i - 1) == Some(&b'\n') {
              writeln!(node, "{}<p></p>", &self.text[prev_end..i])?;
              prev_end = i + 1;
            }
            if let Some(level) = in_heading {
              let title = &self.text[prev_end..i];
              let anchor = format!("{}.{}", anchor_prefix, title.trim());
              write!(
                node,
                "<h{}><a id='{}' href='#{}'>{}</a></h{}>",
                level, anchor, anchor, title, level
              )?;
              in_heading = None;
              prev_end = i + 1;
            }
          }
        }
        '`' => {
          if self.text.as_bytes().get(i + 1) == Some(&b'`')
            && self.text.as_bytes().get(i + 2) == Some(&b'`')
          {
            if code_block {
              write!(node, "<pre>")?;
              self.generate_code(node, self.text[prev_end..i].trim_start_matches('\n'))?;
              write!(node, "</pre>")?;
            } else {
              write!(node, "{}", &self.text[prev_end..i])?;
            }
            prev_end = i + 3;
            code_block = !code_block;
          } else if !code_block {
            if inline_code {
              write!(node, "<code>")?;
              self.generate_code(node, &self.text[prev_end..i])?;
              write!(node, "</code>")?;
            } else {
              write!(node, "{}", &self.text[prev_end..i])?;
            }
            prev_end = i + 1;
            inline_code = !inline_code;
          }
        }
        _ => {}
      }
    }
    write!(node, "{}", &self.text[prev_end..])?;
    Ok(())
  }
  fn generate_code(&self, node: &mut Node, text: &str) -> fmt::Result {
    let mut prev_end = 0;
    let mut in_line_comment = false;
    let mut in_block_comment = false;
    let mut in_string = false;
    let mut in_number = false;
    let mut in_word = false;
    for (i, c) in text.char_indices() {
      if prev_end > i {
        continue;
      }
      match c {
        '/' => match text.as_bytes().get(i + 1) {
          Some(&b'/') => {
            write!(node, "{}", &text[prev_end..i])?;
            prev_end = i;
            in_line_comment = true;
          }
          Some(&b'*') if !in_line_comment => {
            write!(node, "{}", &text[prev_end..i])?;
            prev_end = i;
            in_block_comment = true;
          }
          _ => {}
        },
        '*' => {
          if text.as_bytes().get(i + 1) == Some(&b'/') && in_block_comment {
            write!(node, "<span class='comment'>{}</span>", &text[prev_end..i + 2])?;
            prev_end = i + 2;
            in_block_comment = false;
            continue;
          }
        }
        '"' => {
          if in_line_comment || in_block_comment {
            continue;
          }
          if in_string {
            write!(node, "<span class='string'>{}</span>", &text[prev_end..=i])?;
            prev_end = i + 1;
          } else {
            write!(node, "{}", &text[prev_end..i])?;
            prev_end = i;
          }
          in_string = !in_string;
        }
        '\n' => {
          if in_line_comment {
            write!(node, "<span class='comment'>{}</span>", &text[prev_end..i])?;
            in_line_comment = false;
            prev_end = i;
          }
        }
        _ => {}
      }
      if !in_line_comment && !in_block_comment && !in_string {
        if c.is_ascii_digit() && !in_number {
          in_number = true;
          write!(node, "{}", &text[prev_end..i])?;
          prev_end = i;
        }
        if !c.is_ascii_digit() && in_number {
          in_number = false;
          write!(node, "<span class='number'>{}</span>", &text[prev_end..i])?;
          prev_end = i;
        }

        if (c.is_ascii_alphabetic() || c == '_') && !in_word {
          in_word = true;
          write!(node, "{}", &text[prev_end..i])?;
          prev_end = i;
        }
        if !(c.is_ascii_alphabetic() || c == '_') && in_word {
          in_word = false;
          let word = &text[prev_end..i];
          let is_kw = matches!(
            word,
            "fn" | "struct" | "impl" | "let" | "while" | "if" | "else" | "for" | "in"
          );
          if is_kw {
            write!(node, "<span class='keyword'>{}</span>", word)?;
          } else if matches!(word, "true" | "false" | "map") {
            write!(node, "<span class='number'>{}</span>", word)?;
          } else {
            let is_primitive = matches!(word, "bool" | "int" | "str" | "float");
            if is_primitive {
              write!(node, "<span class='primitive'>{}</span>", word)?;
            } else if c == '(' {
              write!(node, "<span class='func'>{}</span>", word)?;
            } else {
              write!(node, "{}", word)?;
            }
          }
          prev_end = i;
        }

        match c {
          '=' | '>' | '<' | '+' | '-' | '*' => {
            write!(node, "{}", &text[prev_end..i])?;
            write!(node, "<span class='keyword'>{}</span>", c)?;
            prev_end = i + 1;
          }
          '/' if text.as_bytes().get(i + 1) != Some(&b'/') => {
            write!(node, "{}", &text[prev_end..i])?;
            write!(node, "<span class='keyword'>{}</span>", c)?;
            prev_end = i + 1;
          }
          _ => {}
        }
      }
    }
    if in_line_comment {
      write!(node, "<span class='comment'>{}</span>", &text[prev_end..])?;
    } else {
      write!(node, "{}", &text[prev_end..])?;
    }
    Ok(())
  }

  pub fn summary(&self) -> String {
    let mut out = String::new();
    let mut prev_end = 0;
    let mut inline_code = false;
    for (i, c) in self.text.char_indices() {
      if prev_end > i {
        continue;
      }
      match c {
        '#' => {
          if i == 0 || self.text.as_bytes().get(i - 1) != Some(&b'\n') {
            continue;
          }
          out += &self.text[prev_end..i];
          return out;
        }
        '\n' => {
          if self.text.as_bytes().get(i - 1) == Some(&b'\n') {
            return self.text[..i].to_string();
          }
        }
        '`' => {
          if self.text.as_bytes().get(i + 1) == Some(&b'`')
            && self.text.as_bytes().get(i + 2) == Some(&b'`')
          {
            return self.text[..i].to_string();
          } else {
            if inline_code {
              out += "<code>";
              out += &self.text[prev_end..i];
              out += "</code>";
            } else {
              out += &self.text[prev_end..i];
            }
            prev_end = i + 1;
            inline_code = !inline_code;
          }
        }
        _ => {}
      }
    }
    out += &self.text[prev_end..];
    out
  }
}

#[macro_export]
macro_rules! markdown {
  ( $(#[$outer:meta])* ) => {
    $crate::MarkdownSection::from_meta(&[$(stringify!($outer)),*])
  };
}
