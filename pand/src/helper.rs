use panda::{
  parse::token::{Keyword, Token, Tokenizer},
  Panda,
};
use rustyline::{
  completion::Completer,
  highlight::Highlighter,
  hint::{Hint, Hinter},
  validate::Validator,
  Context, Helper,
};
use std::{collections::HashSet, io};

pub struct SlHelper {
  _locals:   HashSet<String>,
  functions: Vec<String>,
  modules:   Vec<String>,
}
pub struct SlHint {}

impl Hint for SlHint {
  fn display(&self) -> &str {
    "hint display"
  }
  fn completion(&self) -> Option<&str> {
    Some("completion")
  }
}

impl Completer for SlHelper {
  type Candidate = String;

  fn complete(
    &self,
    line: &str,
    pos: usize,
    _ctx: &Context<'_>,
  ) -> rustyline::Result<(usize, Vec<String>)> {
    // We don't care about what's in front of the cursor
    let line = &line[..pos];
    let rev = line.chars().rev().collect::<String>();
    let mut tokens = Tokenizer::new(rev.as_bytes(), 0); // File 0
    Ok(self.complete_expr(rev.as_bytes().len(), &mut tokens, ""))
  }
}
impl SlHelper {
  fn complete_expr<T: io::Read>(
    &self,
    len: usize,
    tokens: &mut Tokenizer<T>,
    prefix: &str,
  ) -> (usize, Vec<String>) {
    match tokens.read() {
      Ok(Token::Ident(ident)) => {
        // The last element in the input text was an ident, so we are completing
        // something like a variable or function name. After this ident will be
        // a ( or :: or , or something that will determine what we are looking
        // for.
        self.complete_expr(len, tokens, &ident.chars().rev().collect::<String>())
      }
      Ok(Token::Keyword(key)) => {
        // This keyword will determine if we are looking for an expression or a
        // module.
        match key.value() {
          Keyword::PathSep => (len - tokens.pos().index() as usize, self.list_modules(prefix)),
          Keyword::Comma | Keyword::OpenParen => {
            (len - tokens.pos().index() as usize + 1, self.list_exprs(prefix))
          }
          _ => (0, vec![]),
        }
      }
      Ok(Token::EOF(_)) => (0, self.list_exprs(prefix)),
      // We can't really help complete anything else
      Ok(_) => (0, vec![]),
      Err(_e) => (len - tokens.pos().index() as usize, vec![]),
    }
  }

  fn list_exprs(&self, prefix: &str) -> Vec<String> {
    let mut candidates = vec![];
    for f in &self.functions {
      if f.starts_with(prefix) {
        candidates.push(f.clone() + "(");
      }
    }
    for m in &self.modules {
      if m.starts_with(prefix) {
        candidates.push(m.clone() + "::");
      }
    }
    candidates
  }
  fn list_modules(&self, prefix: &str) -> Vec<String> {
    let mut candidates = vec![];
    for m in &self.modules {
      if m.starts_with(prefix) {
        candidates.push(m.clone() + "::");
      }
    }
    candidates
  }
}

impl Hinter for SlHelper {
  type Hint = String;

  fn hint(&self, line: &str, pos: usize, _ctx: &Context<'_>) -> Option<String> {
    let line = &line[..pos];
    let rev = line.chars().rev().collect::<String>();
    let mut tokens = Tokenizer::new(rev.as_bytes(), 0); // File 0
    let (_idx, options) = self.complete_expr(rev.as_bytes().len(), &mut tokens, "");
    if options.len() == 1 {
      Some(options.first().unwrap()[pos..].to_string())
    } else {
      None
    }
  }
}
impl Highlighter for SlHelper {}
impl Validator for SlHelper {}

impl Helper for SlHelper {}

impl SlHelper {
  pub fn new(_sl: &mut Panda) -> Self {
    /*
    let names = sl.env().names();
    let mut functions: Vec<_> = match &names[path!()] {
      TypeDef::Builtin { ty } => ty.list_funcs().iter().map(|f| f.name.to_string()).collect(),
      _ => unreachable!(),
    };
    let mut modules = vec![];
    for (path, ty) in types {
      match path {
        TypeDefPath::Path(path) => {
          modules.push(path.first().to_string());
        }
        _ => {}
      }
    }
    functions.sort_unstable();
    modules.sort_unstable();
    */
    let functions = vec![];
    let modules = vec![];
    SlHelper { _locals: HashSet::new(), functions, modules }
  }
}
