use panda::{define_ty, docs::markdown, path, runtime::PandaType, Panda};

#[derive(Debug, Clone)]
struct MyType;

trait Foo {
  type Bar;
}

impl Foo for i32 {
  type Bar = String;
}

/// MyType. This does many things.
#[define_ty(path = "demo::MyType")]
impl MyType {
  /// Creates a new `MyType`.
  ///
  /// # Example
  ///
  /// ```
  /// MyType::new()
  /// ```
  pub fn new() -> MyType {
    MyType
  }
  /// Sets the name.
  pub fn set_name(&mut self, name: &str) {
    let _ = name;
  }

  /// The name of MyType.
  #[field]
  pub fn name(&self) -> String {
    "Hello".into()
  }
  /// The color of MyType.
  ///
  /// This is always `"green"`.
  #[doc = concat!("foo", "bar")]
  #[field]
  pub fn color(&self) -> String {
    "green".into()
  }
  /// Returns `bleg`.
  #[field]
  pub fn foo_bar(&self) -> <i32 as Foo>::Bar {
    "bleg".into()
  }
}

fn main() {
  let mut pd = Panda::new();

  pd.add_builtin_ty::<MyType>();

  pd.def_callback(
    "init",
    vec![MyType::var_type()],
    markdown!(
      /// Does things on init.
    ),
  );
  pd.predefine(
    "data",
    markdown!(
      /// My fancy data
    ),
    || 3.into(),
  );

  let docs = pd.generate_docs(
    &[(
      path!(demo),
      markdown!(
        /// A demo module, created to show the docs page.
      ),
    )],
    &[],
  );
  println!("saving to target/panda_doc/index.html");
  docs.save("target/panda_doc")
}
