#![allow(clippy::unused_unit)]

use panda::{path, runtime::Var, Panda, PdError};
use std::{
  io, panic,
  path::Path,
  str,
  sync::{Arc, Mutex},
};
use wasm_bindgen::prelude::*;

#[wasm_bindgen]
extern "C" {
  #[wasm_bindgen(js_namespace = document)]
  fn write_text(text: &str);
}

struct WasmWriter;

impl io::Write for WasmWriter {
  fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
    write_text(str::from_utf8(buf).unwrap_or(""));
    Ok(buf.len())
  }

  fn flush(&mut self) -> io::Result<()> {
    Ok(())
  }
}

fn output(_sl: Panda, var: Var) {
  write_text(&format!(">> {}", var))
}

#[wasm_bindgen]
pub fn compile(src: &str) {
  panic::set_hook(Box::new(console_error_panic_hook::hook));

  let mut sl = Panda::new();

  sl.env().out = Arc::new(Mutex::new(WasmWriter));
  sl.env().err = Arc::new(Mutex::new(WasmWriter));

  sl.env().add_std_builtins();

  let path = Path::new("example.pand");
  if src.trim().starts_with("fn") {
    match sl.parse_file(&path!(example), path, src.to_string()) {
      Ok(()) => match sl.call(&path!(example::main)) {
        Ok(v) => output(sl, v),
        Err(e) => write_text(&e.gen(src, path, false)),
      },
      Err(e) => write_text(&e.gen(src, path, false)),
    }
  } else {
    match sl.parse_statement(src.as_bytes()) {
      Ok(stat) => match sl.run(stat) {
        Ok(v) => output(sl, v),
        Err(e) => write_text(&e.gen(src, path, false)),
      },
      Err(e) => write_text(&e.gen(src, path, false)),
    }
  }
}
